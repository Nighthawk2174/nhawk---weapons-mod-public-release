-- for warhead functions and default warheads map (for warheads["AIM_65"])
dofile("Scripts/Database/Weapons/warheads.lua")



local function bru_42_3x_weapon(clsid,weapon_info,left,right,bottom)

    local bru_42_mass = 128
    local lvl2 = wsType_Missile
    if type(weapon_info.wsType)=="table" then
      lvl2 = weapon_info.wsType[2]
    end
	local ret = {
		category			=	weapon_info.category,
		CLSID				=	clsid,
		Picture				=	weapon_info.picture,
		attribute			=	{wsType_Weapon,	lvl2,	wsType_Container, WSTYPE_PLACEHOLDER},
		Cx_pil				=	0.0005,
		Elements 			= { }
	}
    if type(weapon_info.wsType)=="table" and weapon_info.wsType[4] ~= nil then
        ret.wsTypeOfWeapon = weapon_info.wsType
    end
    if type(weapon_info.wsType)=="string" then
        ret.wsTypeOfWeapon = weapon_info.wsType
    end
    ret.Elements[#ret.Elements + 1] = { ShapeName	= "HB_F14_EXT_BRU42",IsAdapter  =   true}
    local sz = 0
	if left then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, connector_name =	"BRU-42_LEFT"}
    end
	if right then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, connector_name =	"BRU-42_RIGHT"}
    end
	if bottom then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, connector_name =	"BRU-42_LOWER"}
    end

	ret.Count  = sz
	ret.Weight = bru_42_mass +  sz * weapon_info.mass

	ret.Cx_pil = ret.Cx_pil + sz * weapon_info.Cx

    if sz > 1 then
        ret.displayName =	sz.." "..weapon_info.name
    else
        ret.displayName =	weapon_info.name
    end
	declare_loadout(ret)
    return ret
end

local function bru_42_3x_bomb(clsid,weapon_info,left,right,bottom)

    local bru_42_mass = 128
    local lvl2 = wsType_Bomb
    if type(weapon_info.wsType)=="table" then
      lvl2 = weapon_info.wsType[2]
    end
	local ret = {
		category			=	weapon_info.category,
		CLSID				=	clsid,
		Picture				=	weapon_info.picture,
		attribute			=	{wsType_Weapon,	lvl2,	wsType_Container, WSTYPE_PLACEHOLDER},
		Cx_pil				=	0.0005,
		Elements 			= { }
	}
    if type(weapon_info.wsType)=="table" and weapon_info.wsType[4] ~= nil then
        ret.wsTypeOfWeapon = weapon_info.wsType
    end
    if type(weapon_info.wsType)=="string" then
        ret.wsTypeOfWeapon = weapon_info.wsType
    end
    ret.Elements[#ret.Elements + 1] = { ShapeName	= "HB_F14_EXT_BRU42",IsAdapter  =   true}
    local sz = 0
	if left then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {ShapeName = weapon_info.ShapeName, connector_name =	"BRU-42_LEFT"}
    end
	if right then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {ShapeName = weapon_info.ShapeName, connector_name =	"BRU-42_RIGHT"}
    end
	if bottom then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {ShapeName = weapon_info.ShapeName, connector_name =	"BRU-42_LOWER"}
    end

	ret.Count  = sz
	ret.Weight = bru_42_mass +  sz * weapon_info.mass

	ret.Cx_pil = ret.Cx_pil + sz * weapon_info.Cx

    if sz > 1 then
        ret.displayName =	sz.." "..weapon_info.name
    else
        ret.displayName =	weapon_info.name
    end
	declare_loadout(ret)
    return ret
end

local function mak79_4x_weapon(clsid,weapon_info,outerpairs,frontleft,frontright,rearleft,rearright)

    local mak_79_mass = 10 -- TODO: find good value for this adapter
	local ret = {
		category			=	weapon_info.category,
		CLSID				=	clsid,
		Picture				=	weapon_info.picture,
		attribute			=	{wsType_Weapon,	weapon_info.wsType[2],	wsType_Container, WSTYPE_PLACEHOLDER},
		Cx_pil				=	0.00001, -- TODO: what is reasonable?
		Elements 			= { }
	}
    local x_from_center
    if outerpairs then
        x_from_center = 1.55 --forward/backward   mk83
    else
        x_from_center = 1.18 --forward/backward   mk82,81
    end;
    local z_from_center = 0.18   -- left/right
    local y_from_center = -0.16
    if weapon_info.wsType[4] ~= nil then
        ret.wsTypeOfWeapon = weapon_info.wsType
    end
    --ret.Elements[#ret.Elements + 1] = { ShapeName	= "HB_F14_EXT_BRU42",IsAdapter  =   true}
    local sz = 0
	if frontleft then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, Position= {x_from_center,y_from_center,-z_from_center}}
    end
	if frontright then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, Position= {x_from_center,y_from_center,z_from_center}}
    end
	if rearleft then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, Position= {-x_from_center,y_from_center,-z_from_center}}
    end
	if rearright then
        sz = sz + 1
        ret.Elements[#ret.Elements + 1] = {payload_CLSID = weapon_info.payload_CLSID, Position= {-x_from_center,y_from_center,z_from_center}}
    end

	ret.Count  = sz
	ret.Weight = sz * mak_79_mass +  sz * weapon_info.mass

	ret.Cx_pil = ret.Cx_pil + sz * weapon_info.Cx

    if sz > 1 then
        ret.displayName =	"MAK79 "..sz.." "..weapon_info.name
    else
        ret.displayName =	"MAK79 "..weapon_info.name
    end
	declare_loadout(ret)
    return ret
end

local function bru_32_nested(clsid,nested_loadout)
    local adu_703_bru_32_mass = 57.38

    local lvl2 = wsType_Missile
    if type(nested_loadout.attribute)=="table" then
      lvl2 = nested_loadout.attribute[2]
    end
	local ret = {
		category			=	nested_loadout.category,
		CLSID				=	clsid,
		Picture				=	nested_loadout.Picture,
		attribute			=	{wsType_Weapon,	lvl2,	wsType_Container,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	0.00002, -- TODO: what is reasonable?
   		JettisonSubmunitionOnly = true,
		Elements 			= {
        }
	}
    if nested_loadout.wsTypeOfWeapon ~= nil then
        ret.wsTypeOfWeapon		=	nested_loadout.wsTypeOfWeapon
    end
    ret.Elements[#ret.Elements + 1] = { ShapeName	= "HB_F14_EXT_BRU34", IsAdapter  =   true  }
    ret.Elements[#ret.Elements + 1] = {payload_CLSID = nested_loadout.CLSID, connector_name = "WEP_BRU-34_BRU-42"}
	ret.Count  = nested_loadout.Count
	ret.Weight = adu_703_bru_32_mass + nested_loadout.Weight

	ret.Cx_pil = ret.Cx_pil + nested_loadout.Cx_pil

	--ret.displayName =	_("BRU-32 ")..nested_loadout.displayName
	ret.displayName =	nested_loadout.displayName
	declare_loadout(ret)
    return ret
end

local function phx_adapter_nested(clsid,nested_loadout)
    local phx_adapter_mass = 0 -- TODO
	local ret = {
		category			=	nested_loadout.category,
		CLSID				=	clsid,
		Picture				=	nested_loadout.Picture,
		attribute			=	{wsType_Weapon,	nested_loadout.attribute[2],	wsType_Container,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	0.0002, -- TODO: what is reasonable?
   		JettisonSubmunitionOnly = true,
		Elements 			= {
        }
	}
    if nested_loadout.wsTypeOfWeapon ~= nil then
        ret.wsTypeOfWeapon		=	nested_loadout.wsTypeOfWeapon
    end
    ret.Elements[#ret.Elements + 1] = { ShapeName	= "HB_F14_EXT_SHOULDER_PHX_L", IsAdapter  =   true  }
    ret.Elements[#ret.Elements + 1] = {payload_CLSID = nested_loadout.CLSID, connector_name = "WEP_Phoenix_Connector"}
	ret.Count  = nested_loadout.Count
	ret.Weight = phx_adapter_mass + nested_loadout.Weight


	ret.Cx_pil = ret.Cx_pil + nested_loadout.Cx_pil

	--ret.displayName =	_("PHX ")..nested_loadout.displayName
	ret.displayName =	nested_loadout.displayName
	declare_loadout(ret)
    return ret
end

--------- MISSILES ---------
----------------------------------------------

local AIM_54A_Mk47 = {
    category		= CAT_AIR_TO_AIR,
    name			= "AIM_54A_Mk47",
    user_name		= _("AIM-54A-Mk47"),
    wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
    --class_name	= "wAmmunitionPhoenix",
    shape_table_data =
    {
        {
            name	 = "AIM-54A_Mk47";
            file  = "HB_F14_EXT_AIM54";
            life  = 1;
            fire  = { 0, 1};
            username = "AIM-54A-Mk47";
            index = WSTYPE_PLACEHOLDER,
        },
        {
            name = "HB_F14_EXT_PHX_ALU";
            file = "HB_F14_EXT_PHX_ALU";
            life = 1;
            fire = { 0, 1};
            index = WSTYPE_PLACEHOLDER,
        },
    },


        Escort = 0,
        Head_Type = 2,
        sigma = {5, 5, 5},
        M = 444.0,
        H_max = 20000.0,
        H_min = 1.0,
        Diam = 380.0,
        Cx_pil = 5,
        D_max = 14000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 160.0,
        Nr_max = 18,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 5.0,
        Range_max = 180000.0,
        H_min_t = 3.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.52,
        warhead = warheads["AIM_54"],
        exhaust = { 1, 1, 1, 1 },  -- smoke "high"
        X_back = -1.61,
        Y_back = -0.089,
        Z_back = 0.0,
        Reflection = 0.0329,
        KillDistance = 15.0,
		ccm_k0 = 0.06,
        loft = 0,
        hoj = 0,
        go_active_by_default = 0,
        active_radar_lock_dist = 18520,
		
        PN_coeffs = {11,                 -- Number of Entries
                    0.0, 1.0,
                    4000.0, 0.995,
                    5000.0, 0.99,
                    6000.0, 0.97,
                    7000.0, 0.94,
                    10000.0, 0.80,
                    15000.0, 0.50,
                    20000.0, 0.35,
                    30000.0, 0.20,
                    40000.0, 0.14,
                    100000.0, 0.05,
                    };

        ModelData = {   58,  -- model params count
                        0.9,   -- characteristic square (ÑÐ°ÑÐ°ÐºÑÐµÑÐ¸ÑÑÐ¸ÑÐµÑÐºÐ°Ð¯ Ð¿Ð»Ð¾ÑÐ°Ð´Ñ)

                        -- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ âx
                        0.01, -- Cx_k0 Ð¿Ð»Ð°Ð½ÐºÐ° âx0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
                        0.031, -- Cx_k1 Ð²ÑÑÐ¾ÑÐ° Ð¿Ð¸ÐºÐ° Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð³Ð¾ ÐºÑÐ¸Ð·Ð¸ÑÐ°
                        0.03, -- Cx_k2 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÑÐ¾Ð½ÑÐ° Ð½Ð° Ð¿Ð¾Ð´ÑÐ¾Ð´Ðµ Ðº Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð¼Ñ ÐºÑÐ¸Ð·Ð¸ÑÑ
                        0.028, -- Cx_k3 Ð¿Ð»Ð°Ð½ÐºÐ° Cx0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
                        0.9, -- Cx_k4 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð° Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼
                        1.5, -- ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½Ñ Ð¾ÑÐ²Ð°Ð»Ð° Ð¿Ð¾Ð»Ð¯ÑÑ (Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½Ð¾ sqrt (M^2-1))

                        -- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ Cy
                        0.9, -- Cy_k0 Ð¿Ð»Ð°Ð½ÐºÐ° ây0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
                        0.8, -- Cy_k1 Ð¿Ð»Ð°Ð½ÐºÐ° Cy0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
                        1.2, -- Cy_k2 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð°(ÑÑÐ¾Ð½ÑÐ°) Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼

                        0.45, -- 7 Alfa_max  Ð¼Ð°ÐºÑÐ¸Ð¼Ð°Ð»ÑÐ½ÑÐ¹ Ð±Ð°Ð»Ð°Ð½ÑÐ¸ÑÐ¾Ð²Ð°ÑÐ½ÑÐ¹ ÑÐ³Ð¾Ð», ÑÐ°Ð´Ð¸Ð°Ð½Ñ
                        0.0, --ÑÐ³Ð»Ð¾Ð²Ð°Ð¯ ÑÐºÐ¾ÑÐ¾ÑÑÑ ÑÐ¾Ð·Ð´Ð°Ð²Ð°Ð¹Ð¼Ð°Ð¯ Ð¼Ð¾Ð¼ÐµÐ½ÑÐ¾Ð¼ Ð³Ð°Ð·Ð¾Ð²ÑÑ ÑÑÐ»ÐµÐ¹

                    -- Engine data. Time, fuel flow, thrust.
                    --  t_statr     t_b     t_accel     t_march     t_inertial      t_break     t_end           -- Stage
                         0.27,       -1.0,   27.0,       0.0,        0.0,            0.0,        1.0e9,         -- time of stage, sec
                         0.0,       0.0,    6.32,       0.0,        0.0,            0.0,        0.0,           -- fuel flow rate in second, kg/sec(ÑÐµÐºÑÐ½Ð´Ð½ÑÐ¹ ÑÐ°ÑÑÐ¾Ð´ Ð¼Ð°ÑÑÑ ÑÐ¾Ð¿Ð»Ð¸Ð²Ð° ÐºÐ³/ÑÐµÐº)
                         0.0,       0.0,    15982.0,    0.0,        0.0,            0.0,        0.0,           -- thrust, newtons

                         1.0e9, -- ÑÐ°Ð¹Ð¼ÐµÑ ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, ÑÐµÐº
                         160.0, -- Ð²ÑÐµÐ¼Ð¯ ÑÐ°Ð±Ð¾ÑÑ ÑÐ½ÐµÑÐ³Ð¾ÑÐ¸ÑÑÐµÐ¼Ñ, ÑÐµÐº
                         0, -- Ð°Ð±ÑÐ¾Ð»ÑÑÐ½Ð°Ð¯ Ð²ÑÑÐ¾ÑÐ° ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, Ð¼
                         1.5, -- Ð²ÑÐµÐ¼Ð¯ Ð·Ð°Ð´ÐµÑÐ¶ÐºÐ¸ Ð²ÐºÐ»ÑÑÐµÐ½Ð¸Ð¯ ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ð¯ (Ð¼Ð°Ð½ÐµÐ²Ñ Ð¾ÑÐ»ÐµÑÐ°, Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐ½Ð¾ÑÑÐ¸), ÑÐµÐº
                         30000, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸ Ð² Ð¼Ð¾Ð¼ÐµÐ½Ñ Ð¿ÑÑÐºÐ°, Ð¿ÑÐ¸ Ð¿ÑÐµÐ²ÑÑÐµÐ½Ð¸Ð¸ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ ÑÐ°ÐºÐµÑÐ° Ð²ÑÐ¿Ð¾Ð»Ð½Ð¯ÐµÑÑÐ¯ Ð¼Ð°Ð½ÐµÐ²Ñ "Ð³Ð¾ÑÐºÐ°", Ð¼
                         40000, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸, Ð¿ÑÐ¸ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ Ð¼Ð°Ð½ÐµÐ²Ñ "Ð³Ð¾ÑÐºÐ°" Ð·Ð°Ð²ÐµÑÑÐ°ÐµÑÑÐ¯ Ð¸ ÑÐ°ÐºÐµÑÐ° Ð¿ÐµÑÐµÑÐ¾Ð´Ð¸Ñ Ð½Ð° ÑÐ¸ÑÑÑÑ Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½ÑÑ Ð½Ð°Ð²Ð¸Ð³Ð°ÑÐ¸Ñ (Ð´Ð¾Ð»Ð¶ÐµÐ½ Ð±ÑÑÑ Ð±Ð¾Ð»ÑÑÐµ Ð¸Ð»Ð¸ ÑÐ°Ð²ÐµÐ½ Ð¿ÑÐµÐ´ÑÐ´ÑÑÐµÐ¼Ñ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ), Ð¼
                         0.20,  -- ÑÐ¸Ð½ÑÑ ÑÐ³Ð»Ð° Ð²Ð¾Ð·Ð²ÑÑÐµÐ½Ð¸Ð¯ ÑÑÐ°ÐµÐºÑÐ¾ÑÐ¸Ð¸ Ð½Ð°Ð±Ð¾ÑÐ° Ð³Ð¾ÑÐºÐ¸
                         50.0, -- Ð¿ÑÐ¾Ð´Ð¾Ð»ÑÐ½Ð¾Ðµ ÑÑÐºÐ¾ÑÐµÐ½Ð¸Ð¯ Ð²Ð·Ð²ÐµÐ´ÐµÐ½Ð¸Ð¯ Ð²Ð·ÑÑÐ²Ð°ÑÐµÐ»Ð¯
                         0.0, -- Ð¼Ð¾Ð´ÑÐ»Ñ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ ÑÐ¾Ð¾Ð±ÑÐ°Ð¹Ð¼ÑÐ¹ ÐºÐ°ÑÐ°Ð¿ÑÐ»ÑÑÐ½ÑÐ¼ ÑÑÑÑÐ¾Ð¹ÑÑÐ²Ð¾Ð¼, Ð²ÑÑÐ¸Ð±Ð½ÑÐ¼ Ð·Ð°ÑÐ¯Ð´Ð¾Ð¼ Ð¸ ÑÐ´
                         1.19, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÐ¯Ð´ÐºÐ° K0
                         1.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÐ¯Ð´ÐºÐ° K1
                         2.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  Ð¿Ð¾Ð»Ð¾ÑÐ° Ð¿ÑÐ¾Ð¿ÑÑÐºÐ°Ð½Ð¸Ð¯ ÐºÐ¾Ð½ÑÑÑÐ° ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ð¯
                         25200.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð½Ð° Ð²ÑÑÐ¾ÑÐµ H=2000
                         3.92, -- ÐºÑÑÑÐ¸Ð·Ð½Ð° Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸  Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð¾Ñ Ð²ÑÑÐ¾ÑÑ H
                         3.2,
                         0.75, -- Ð±ÐµÐ·ÑÐ°Ð·Ð¼ÐµÑÐ½ÑÐ¹ ÐºÐ¾ÑÑ. ÑÑÑÐµÐºÑÐ¸Ð²Ð½Ð¾ÑÑÐ¸ âÐâ ÑÐ°ÐºÐµÑÑ
                         70.0, -- ÑÐ°ÑÑÐµÑ Ð²ÑÐµÐ¼ÐµÐ½Ð¸ Ð¿Ð¾Ð»ÐµÑÐ°
                          -- DLZ. âÐ°Ð½Ð½ÑÐµ Ð´Ð»Ð¯ ÑÐ°ÑÑÑÐµÑÐ° Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐµÐ¹ Ð¿ÑÑÐºÐ° (Ð¸Ð½Ð´Ð¸ÐºÐ°ÑÐ¸Ð¯ Ð½Ð° Ð¿ÑÐ¸ÑÐµÐ»Ðµ)
                         63000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ   180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ, Ð¼
                         25000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ 0(Ð² Ð´Ð¾Ð³Ð¾Ð½) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ
                         22000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ    180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´, Ð=1000Ð¼, V=900ÐºÐ¼/Ñ
                         0.2,
                         0.6,
                         1.4,
                        -3.0,
                        0.5,
                    },
    }
declare_weapon(AIM_54A_Mk47)

declare_loadout({	-- AIM-54A Mk47
    category		=	CAT_AIR_TO_AIR,
    CLSID			= 	"{AIM_54A_Mk47}",
    Picture			=	"aim54.png",
    --wsTypeOfWeapon	=	AIM_54A_Mk47.wsTypeOfWeapon,
    displayName		=	AIM_54A_Mk47.user_name,
    --attribute		=	{wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
    attribute	=	AIM_54A_Mk47.wsTypeOfWeapon,
    Cx_pil			=	AIM_54A_Mk47.Cx_pil  / 4096.0,
    Count			=	1,
    Weight			=	AIM_54A_Mk47.M,
    Elements	=
    {
		--{	ShapeName	=	"HB_F14_EXT_PHX_ALU"  ,	IsAdapter  	   =   true  },
        {
            DrawArgs	=
            {
                [1]	=	{1,	1},
                [2]	=	{2,	1},
            }, -- end of DrawArgs
            --Position	=	{0,	0,	0},
            --connector_name = "WEP_Phoenix_FrontPallette_L_ALU",
            ShapeName	=	"AIM-54A_Mk47",
        },
    }, -- end of Elements
})

local AIM_54A_Mk60 = {
    category		= CAT_AIR_TO_AIR,
    name			= "AIM_54A_Mk60",
    user_name		= _("AIM-54A-Mk60"),
    wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},

    shape_table_data =
    {
        {
            name	 = "AIM-54A_Mk60";
            file  = "HB_F14_EXT_AIM54";
            life  = 1;
            fire  = { 0, 1};
            username = "AIM-54A-Mk60";
            index = WSTYPE_PLACEHOLDER,
        },
        {
            name = "HB_F14_EXT_PHX_ALU";
            file = "HB_F14_EXT_PHX_ALU";
            life = 1;
            fire = { 0, 1};
            index = WSTYPE_PLACEHOLDER,
        },
    },


        Escort = 0,
        Head_Type = 2,
        sigma = {5, 5, 5},
        M = 471.7,
        H_max = 20000.0,
        H_min = 1.0,
        Diam = 380.0,
        Cx_pil = 5,
        D_max = 14000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 160.0,
        Nr_max = 18,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 5.0,
        Range_max = 180000.0,
        H_min_t = 3.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.52,
        warhead = warheads["AIM_54"],
        exhaust = { 1, 1, 1, 0.6 },   -- "medium" smoke, compared to 54a-mk47's "high" smoke
        X_back = -1.61,
        Y_back = -0.089,
        Z_back = 0.0,
        Reflection = 0.0329,
        KillDistance = 15.0,
		ccm_k0 = 0.06,
        loft = 0,
        hoj = 0,
        go_active_by_default = 0,
        active_radar_lock_dist = 18520,
		
        PN_coeffs = {11,                 -- Number of Entries
                    0.0, 1.0,
                    4000.0, 0.995,
                    5000.0, 0.99,
                    6000.0, 0.97,
                    7000.0, 0.94,
                    10000.0, 0.80,
                    15000.0, 0.50,
                    20000.0, 0.35,
                    30000.0, 0.20,
                    40000.0, 0.14,
                    100000.0, 0.05,
                    };

        ModelData = {   58,  -- model params count
                        0.9,   -- characteristic square (ÑÐ°ÑÐ°ÐºÑÐµÑÐ¸ÑÑÐ¸ÑÐµÑÐºÐ°Ð¯ Ð¿Ð»Ð¾ÑÐ°Ð´Ñ)

                        -- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ âx
                        0.01, -- Cx_k0 Ð¿Ð»Ð°Ð½ÐºÐ° âx0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
                        0.031, -- Cx_k1 Ð²ÑÑÐ¾ÑÐ° Ð¿Ð¸ÐºÐ° Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð³Ð¾ ÐºÑÐ¸Ð·Ð¸ÑÐ°
                        0.03, -- Cx_k2 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÑÐ¾Ð½ÑÐ° Ð½Ð° Ð¿Ð¾Ð´ÑÐ¾Ð´Ðµ Ðº Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð¼Ñ ÐºÑÐ¸Ð·Ð¸ÑÑ
                        0.028, -- Cx_k3 Ð¿Ð»Ð°Ð½ÐºÐ° Cx0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
                        0.9, -- Cx_k4 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð° Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼
                        1.5, -- ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½Ñ Ð¾ÑÐ²Ð°Ð»Ð° Ð¿Ð¾Ð»Ð¯ÑÑ (Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½Ð¾ sqrt (M^2-1))

                        -- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ Cy
                        0.9, -- Cy_k0 Ð¿Ð»Ð°Ð½ÐºÐ° ây0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
                        0.8, -- Cy_k1 Ð¿Ð»Ð°Ð½ÐºÐ° Cy0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
                        1.2, -- Cy_k2 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð°(ÑÑÐ¾Ð½ÑÐ°) Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼

                        0.45, -- 7 Alfa_max  Ð¼Ð°ÐºÑÐ¸Ð¼Ð°Ð»ÑÐ½ÑÐ¹ Ð±Ð°Ð»Ð°Ð½ÑÐ¸ÑÐ¾Ð²Ð°ÑÐ½ÑÐ¹ ÑÐ³Ð¾Ð», ÑÐ°Ð´Ð¸Ð°Ð½Ñ
                        0.0, --ÑÐ³Ð»Ð¾Ð²Ð°Ð¯ ÑÐºÐ¾ÑÐ¾ÑÑÑ ÑÐ¾Ð·Ð´Ð°Ð²Ð°Ð¹Ð¼Ð°Ð¯ Ð¼Ð¾Ð¼ÐµÐ½ÑÐ¾Ð¼ Ð³Ð°Ð·Ð¾Ð²ÑÑ ÑÑÐ»ÐµÐ¹

                    -- Engine data. Time, fuel flow, thrust.
                    --  t_start     t_b     t_accel     t_march     t_inertial      t_break     t_end           -- Stage
                         0.27,       -1.0,   30.0,       0.0,        0.0,            0.0,        1.0e9,         -- time of stage, sec
                         0.0,       0.0,    6.933,      0.0,        0.0,            0.0,        0.0,           -- fuel flow rate in second, kg/sec(ÑÐµÐºÑÐ½Ð´Ð½ÑÐ¹ ÑÐ°ÑÑÐ¾Ð´ Ð¼Ð°ÑÑÑ ÑÐ¾Ð¿Ð»Ð¸Ð²Ð° ÐºÐ³/ÑÐµÐº)
                         0.0,       0.0,    17793.0,    0.0,    0.0,            0.0,        0.0,           -- thrust, newtons

                         1.0e9, -- ÑÐ°Ð¹Ð¼ÐµÑ ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, ÑÐµÐº
                         160.0, -- Ð²ÑÐµÐ¼Ð¯ ÑÐ°Ð±Ð¾ÑÑ ÑÐ½ÐµÑÐ³Ð¾ÑÐ¸ÑÑÐµÐ¼Ñ, ÑÐµÐº
                         0, -- Ð°Ð±ÑÐ¾Ð»ÑÑÐ½Ð°Ð¯ Ð²ÑÑÐ¾ÑÐ° ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, Ð¼
                         1.5, -- Ð²ÑÐµÐ¼Ð¯ Ð·Ð°Ð´ÐµÑÐ¶ÐºÐ¸ Ð²ÐºÐ»ÑÑÐµÐ½Ð¸Ð¯ ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ð¯ (Ð¼Ð°Ð½ÐµÐ²Ñ Ð¾ÑÐ»ÐµÑÐ°, Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐ½Ð¾ÑÑÐ¸), ÑÐµÐº
                         30000, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸ Ð² Ð¼Ð¾Ð¼ÐµÐ½Ñ Ð¿ÑÑÐºÐ°, Ð¿ÑÐ¸ Ð¿ÑÐµÐ²ÑÑÐµÐ½Ð¸Ð¸ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ ÑÐ°ÐºÐµÑÐ° Ð²ÑÐ¿Ð¾Ð»Ð½Ð¯ÐµÑÑÐ¯ Ð¼Ð°Ð½ÐµÐ²Ñ "Ð³Ð¾ÑÐºÐ°", Ð¼
                         40000, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸, Ð¿ÑÐ¸ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ Ð¼Ð°Ð½ÐµÐ²Ñ "Ð³Ð¾ÑÐºÐ°" Ð·Ð°Ð²ÐµÑÑÐ°ÐµÑÑÐ¯ Ð¸ ÑÐ°ÐºÐµÑÐ° Ð¿ÐµÑÐµÑÐ¾Ð´Ð¸Ñ Ð½Ð° ÑÐ¸ÑÑÑÑ Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½ÑÑ Ð½Ð°Ð²Ð¸Ð³Ð°ÑÐ¸Ñ (Ð´Ð¾Ð»Ð¶ÐµÐ½ Ð±ÑÑÑ Ð±Ð¾Ð»ÑÑÐµ Ð¸Ð»Ð¸ ÑÐ°Ð²ÐµÐ½ Ð¿ÑÐµÐ´ÑÐ´ÑÑÐµÐ¼Ñ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ), Ð¼
                         0.20,  -- ÑÐ¸Ð½ÑÑ ÑÐ³Ð»Ð° Ð²Ð¾Ð·Ð²ÑÑÐµÐ½Ð¸Ð¯ ÑÑÐ°ÐµÐºÑÐ¾ÑÐ¸Ð¸ Ð½Ð°Ð±Ð¾ÑÐ° Ð³Ð¾ÑÐºÐ¸
                         50.0, -- Ð¿ÑÐ¾Ð´Ð¾Ð»ÑÐ½Ð¾Ðµ ÑÑÐºÐ¾ÑÐµÐ½Ð¸Ð¯ Ð²Ð·Ð²ÐµÐ´ÐµÐ½Ð¸Ð¯ Ð²Ð·ÑÑÐ²Ð°ÑÐµÐ»Ð¯
                         0.0, -- Ð¼Ð¾Ð´ÑÐ»Ñ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ ÑÐ¾Ð¾Ð±ÑÐ°Ð¹Ð¼ÑÐ¹ ÐºÐ°ÑÐ°Ð¿ÑÐ»ÑÑÐ½ÑÐ¼ ÑÑÑÑÐ¾Ð¹ÑÑÐ²Ð¾Ð¼, Ð²ÑÑÐ¸Ð±Ð½ÑÐ¼ Ð·Ð°ÑÐ¯Ð´Ð¾Ð¼ Ð¸ ÑÐ´
                         1.19, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÐ¯Ð´ÐºÐ° K0
                         1.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÐ¯Ð´ÐºÐ° K1
                         2.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  Ð¿Ð¾Ð»Ð¾ÑÐ° Ð¿ÑÐ¾Ð¿ÑÑÐºÐ°Ð½Ð¸Ð¯ ÐºÐ¾Ð½ÑÑÑÐ° ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ð¯
                         25200.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð½Ð° Ð²ÑÑÐ¾ÑÐµ H=2000
                         3.92, -- ÐºÑÑÑÐ¸Ð·Ð½Ð° Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸  Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð¾Ñ Ð²ÑÑÐ¾ÑÑ H
                         3.2,
                         0.75, -- Ð±ÐµÐ·ÑÐ°Ð·Ð¼ÐµÑÐ½ÑÐ¹ ÐºÐ¾ÑÑ. ÑÑÑÐµÐºÑÐ¸Ð²Ð½Ð¾ÑÑÐ¸ âÐâ ÑÐ°ÐºÐµÑÑ
                         70.0, -- ÑÐ°ÑÑÐµÑ Ð²ÑÐµÐ¼ÐµÐ½Ð¸ Ð¿Ð¾Ð»ÐµÑÐ°
                          -- DLZ. âÐ°Ð½Ð½ÑÐµ Ð´Ð»Ð¯ ÑÐ°ÑÑÑÐµÑÐ° Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐµÐ¹ Ð¿ÑÑÐºÐ° (Ð¸Ð½Ð´Ð¸ÐºÐ°ÑÐ¸Ð¯ Ð½Ð° Ð¿ÑÐ¸ÑÐµÐ»Ðµ)
                         63000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ   180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ, Ð¼
                         25000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ 0(Ð² Ð´Ð¾Ð³Ð¾Ð½) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ
                         22000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ    180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´, Ð=1000Ð¼, V=900ÐºÐ¼/Ñ
                         0.2,
                         0.6,
                         1.4,
                        -3.0,
                        0.5,
                    },
}
declare_weapon(AIM_54A_Mk60)

declare_loadout({	-- AIM-54A Mk60
    category		=	CAT_AIR_TO_AIR,
    CLSID			= 	"{AIM_54A_Mk60}",
    Picture			=	"aim54.png",
    --wsTypeOfWeapon	=	AIM_54A_Mk60.wsTypeOfWeapon,
    displayName		=	AIM_54A_Mk60.user_name,
    --attribute		=	{wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
    attribute	=	AIM_54A_Mk60.wsTypeOfWeapon,
    Cx_pil			=	AIM_54A_Mk60.Cx_pil / 4096.0,
    Count			=	1,
    Weight			=	AIM_54A_Mk60.M,
    Elements	=
    {
		--{	ShapeName	=	"HB_F14_EXT_PHX_ALU"  ,	IsAdapter  	   =   true  },
        {
            DrawArgs	=
            {
                [1]	=	{1,	1},
                [2]	=	{2,	1},
            }, -- end of DrawArgs
            --Position	=	{0,	0,	0},
            --connector_name = "WEP_Phoenix_FrontPallette_L_ALU",
            ShapeName	=	"AIM-54A_M60",
        },
    }, -- end of Elements
})

local AIM_54C_Mk47 = {
    category		= CAT_AIR_TO_AIR,
    name			= "AIM_54C_Mk47",
    user_name		= _("AIM-54C-Mk47"),
    wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},

    shape_table_data =
    {
        {
            name	 = "AIM-54C";
            file  = "HB_F14_EXT_AIM54";
            life  = 1;
            fire  = { 0, 1};
            username = "AIM-54C";
            index = WSTYPE_PLACEHOLDER,
        },
        {
            name = "HB_F14_EXT_PHX_ALU";
            file = "HB_F14_EXT_PHX_ALU";
            life = 1;
            fire = { 0, 1};
            index = WSTYPE_PLACEHOLDER,
        },
    },


        Escort = 0,
        Head_Type = 2,
        sigma = {5, 5, 5},
        M = 465.6,
        H_max = 20000.0,
        H_min = 1.0,
        Diam = 380.0,
        Cx_pil = 5,
        D_max = 14000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 160.0,
        Nr_max = 21,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 5.0,
        Range_max = 180000.0,
        H_min_t = 3.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.52,
        warhead = warheads["AIM_54"],
        exhaust = {0.8, 0.8, 0.8, 0.05 }; -- smoke "none"
        X_back = -1.61,
        Y_back = -0.089,
        Z_back = 0.0,
        Reflection = 0.0329,
        KillDistance = 15.0,
        ccm_k0 = 0.05,
        loft = 0,
        hoj = 0,
        go_active_by_default = 0,
        active_radar_lock_dist = 18520,
        KillDistance = 15.0,
        loft = 1,
        hoj = 1,
			   

        PN_coeffs = {11,                 -- Number of Entries
                    0.0, 1.0,
                    4000.0, 0.995,
                    5000.0, 0.99,
                    6000.0, 0.97,
                    7000.0, 0.94,
                    10000.0, 0.80,
                    15000.0, 0.50,
                    20000.0, 0.35,
                    30000.0, 0.20,
                    40000.0, 0.14,
                    100000.0, 0.05,
                    };

        ModelData = {   58,  -- model params count
                        0.9,   -- characteristic square (ÑÐ°ÑÐ°ÐºÑÐµÑÐ¸ÑÑÐ¸ÑÐµÑÐºÐ°Ð¯ Ð¿Ð»Ð¾ÑÐ°Ð´Ñ)

                        -- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ âx
                        0.01, -- Cx_k0 Ð¿Ð»Ð°Ð½ÐºÐ° âx0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
                        0.031, -- Cx_k1 Ð²ÑÑÐ¾ÑÐ° Ð¿Ð¸ÐºÐ° Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð³Ð¾ ÐºÑÐ¸Ð·Ð¸ÑÐ°
                        0.03, -- Cx_k2 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÑÐ¾Ð½ÑÐ° Ð½Ð° Ð¿Ð¾Ð´ÑÐ¾Ð´Ðµ Ðº Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð¼Ñ ÐºÑÐ¸Ð·Ð¸ÑÑ
                        0.028, -- Cx_k3 Ð¿Ð»Ð°Ð½ÐºÐ° Cx0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
                        0.9, -- Cx_k4 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð° Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼
                        1.5, -- ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½Ñ Ð¾ÑÐ²Ð°Ð»Ð° Ð¿Ð¾Ð»Ð¯ÑÑ (Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½Ð¾ sqrt (M^2-1))

                        -- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ Cy
                        0.9, -- Cy_k0 Ð¿Ð»Ð°Ð½ÐºÐ° ây0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
                        0.8, -- Cy_k1 Ð¿Ð»Ð°Ð½ÐºÐ° Cy0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
                        1.2, -- Cy_k2 ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð°(ÑÑÐ¾Ð½ÑÐ°) Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼

                        0.45, -- 7 Alfa_max  Ð¼Ð°ÐºÑÐ¸Ð¼Ð°Ð»ÑÐ½ÑÐ¹ Ð±Ð°Ð»Ð°Ð½ÑÐ¸ÑÐ¾Ð²Ð°ÑÐ½ÑÐ¹ ÑÐ³Ð¾Ð», ÑÐ°Ð´Ð¸Ð°Ð½Ñ
                        0.0, --ÑÐ³Ð»Ð¾Ð²Ð°Ð¯ ÑÐºÐ¾ÑÐ¾ÑÑÑ ÑÐ¾Ð·Ð´Ð°Ð²Ð°Ð¹Ð¼Ð°Ð¯ Ð¼Ð¾Ð¼ÐµÐ½ÑÐ¾Ð¼ Ð³Ð°Ð·Ð¾Ð²ÑÑ ÑÑÐ»ÐµÐ¹

                    -- Engine data. Time, fuel flow, thrust.
                    --  t_start     t_b     t_accel     t_march     t_inertial      t_break     t_end           -- Stage
                         0.27,       -1.0,   27.0,       0.0,        0.0,            0.0,        1.0e9,         -- time of stage, sec
                         0.0,       0.0,    6.048,      0.0,        0.0,            0.0,        0.0,           -- fuel flow rate in second, kg/sec(ÑÐµÐºÑÐ½Ð´Ð½ÑÐ¹ ÑÐ°ÑÑÐ¾Ð´ Ð¼Ð°ÑÑÑ ÑÐ¾Ð¿Ð»Ð¸Ð²Ð° ÐºÐ³/ÑÐµÐº)
                         0.0,       0.0,    15723.1,    0.0,        0.0,            0.0,        0.0,           -- thrust, newtons

                         1.0e9, -- ÑÐ°Ð¹Ð¼ÐµÑ ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, ÑÐµÐº
                         160.0, -- Ð²ÑÐµÐ¼Ð¯ ÑÐ°Ð±Ð¾ÑÑ ÑÐ½ÐµÑÐ³Ð¾ÑÐ¸ÑÑÐµÐ¼Ñ, ÑÐµÐº
                         0, -- Ð°Ð±ÑÐ¾Ð»ÑÑÐ½Ð°Ð¯ Ð²ÑÑÐ¾ÑÐ° ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, Ð¼
                         1.5, -- Ð²ÑÐµÐ¼Ð¯ Ð·Ð°Ð´ÐµÑÐ¶ÐºÐ¸ Ð²ÐºÐ»ÑÑÐµÐ½Ð¸Ð¯ ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ð¯ (Ð¼Ð°Ð½ÐµÐ²Ñ Ð¾ÑÐ»ÐµÑÐ°, Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐ½Ð¾ÑÑÐ¸), ÑÐµÐº
                         30000, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸ Ð² Ð¼Ð¾Ð¼ÐµÐ½Ñ Ð¿ÑÑÐºÐ°, Ð¿ÑÐ¸ Ð¿ÑÐµÐ²ÑÑÐµÐ½Ð¸Ð¸ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ ÑÐ°ÐºÐµÑÐ° Ð²ÑÐ¿Ð¾Ð»Ð½Ð¯ÐµÑÑÐ¯ Ð¼Ð°Ð½ÐµÐ²Ñ "Ð³Ð¾ÑÐºÐ°", Ð¼
                         40000, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸, Ð¿ÑÐ¸ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ Ð¼Ð°Ð½ÐµÐ²Ñ "Ð³Ð¾ÑÐºÐ°" Ð·Ð°Ð²ÐµÑÑÐ°ÐµÑÑÐ¯ Ð¸ ÑÐ°ÐºÐµÑÐ° Ð¿ÐµÑÐµÑÐ¾Ð´Ð¸Ñ Ð½Ð° ÑÐ¸ÑÑÑÑ Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½ÑÑ Ð½Ð°Ð²Ð¸Ð³Ð°ÑÐ¸Ñ (Ð´Ð¾Ð»Ð¶ÐµÐ½ Ð±ÑÑÑ Ð±Ð¾Ð»ÑÑÐµ Ð¸Ð»Ð¸ ÑÐ°Ð²ÐµÐ½ Ð¿ÑÐµÐ´ÑÐ´ÑÑÐµÐ¼Ñ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ), Ð¼
                         0.20,  -- ÑÐ¸Ð½ÑÑ ÑÐ³Ð»Ð° Ð²Ð¾Ð·Ð²ÑÑÐµÐ½Ð¸Ð¯ ÑÑÐ°ÐµÐºÑÐ¾ÑÐ¸Ð¸ Ð½Ð°Ð±Ð¾ÑÐ° Ð³Ð¾ÑÐºÐ¸
                         50.0, -- Ð¿ÑÐ¾Ð´Ð¾Ð»ÑÐ½Ð¾Ðµ ÑÑÐºÐ¾ÑÐµÐ½Ð¸Ð¯ Ð²Ð·Ð²ÐµÐ´ÐµÐ½Ð¸Ð¯ Ð²Ð·ÑÑÐ²Ð°ÑÐµÐ»Ð¯
                         0.0, -- Ð¼Ð¾Ð´ÑÐ»Ñ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ ÑÐ¾Ð¾Ð±ÑÐ°Ð¹Ð¼ÑÐ¹ ÐºÐ°ÑÐ°Ð¿ÑÐ»ÑÑÐ½ÑÐ¼ ÑÑÑÑÐ¾Ð¹ÑÑÐ²Ð¾Ð¼, Ð²ÑÑÐ¸Ð±Ð½ÑÐ¼ Ð·Ð°ÑÐ¯Ð´Ð¾Ð¼ Ð¸ ÑÐ´
                         1.19, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÐ¯Ð´ÐºÐ° K0
                         1.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÐ¯Ð´ÐºÐ° K1
                         2.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ âÐâ-ÑÐÐâ¦âÐ,  Ð¿Ð¾Ð»Ð¾ÑÐ° Ð¿ÑÐ¾Ð¿ÑÑÐºÐ°Ð½Ð¸Ð¯ ÐºÐ¾Ð½ÑÑÑÐ° ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ð¯
                         25200.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð½Ð° Ð²ÑÑÐ¾ÑÐµ H=2000
                         3.92, -- ÐºÑÑÑÐ¸Ð·Ð½Ð° Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸  Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð¾Ñ Ð²ÑÑÐ¾ÑÑ H
                         3.2,
                         0.75, -- Ð±ÐµÐ·ÑÐ°Ð·Ð¼ÐµÑÐ½ÑÐ¹ ÐºÐ¾ÑÑ. ÑÑÑÐµÐºÑÐ¸Ð²Ð½Ð¾ÑÑÐ¸ âÐâ ÑÐ°ÐºÐµÑÑ
                         70.0, -- ÑÐ°ÑÑÐµÑ Ð²ÑÐµÐ¼ÐµÐ½Ð¸ Ð¿Ð¾Ð»ÐµÑÐ°
                          -- DLZ. âÐ°Ð½Ð½ÑÐµ Ð´Ð»Ð¯ ÑÐ°ÑÑÑÐµÑÐ° Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐµÐ¹ Ð¿ÑÑÐºÐ° (Ð¸Ð½Ð´Ð¸ÐºÐ°ÑÐ¸Ð¯ Ð½Ð° Ð¿ÑÐ¸ÑÐµÐ»Ðµ)
                         63000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ   180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ, Ð¼
                         25000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ 0(Ð² Ð´Ð¾Ð³Ð¾Ð½) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ
                         22000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ    180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´, Ð=1000Ð¼, V=900ÐºÐ¼/Ñ
                         0.2,
                         0.6,
                         1.4,
                        -3.0,
                        0.5,
                    },
}
declare_weapon(AIM_54C_Mk47)

declare_loadout({	-- AIM-54C Mk47
    category		=	CAT_AIR_TO_AIR,
    CLSID			= 	"{AIM_54C_Mk47}",
    Picture			=	"aim54.png",
    --wsTypeOfWeapon	=	AIM_54C_Mk47.wsTypeOfWeapon,
    displayName		=	AIM_54C_Mk47.user_name,
    --attribute		=	{wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
    attribute	=	AIM_54C_Mk47.wsTypeOfWeapon,
    Cx_pil			=	AIM_54C_Mk47.Cx_pil / 4096.0,
    Count			=	1,
    Weight			=	AIM_54C_Mk47.M,
    Elements	=
    {
		--{	ShapeName	=	"HB_F14_EXT_PHX_ALU"  ,	IsAdapter  	   =   true  },
        {
            DrawArgs	=
            {
                [1]	=	{1,	1},
                [2]	=	{2,	1},
            }, -- end of DrawArgs
            --Position	=	{0,	-0.5,	0},
            --connector_name = "WEP_Phoenix_FrontPallette_L_ALU",
            ShapeName	=	"AIM-54C",
        },
    }, -- end of Elements
})

local function shoulder_aim_54(clsid, element, elem_CLSID, side)  -- side L or R
	local ret = {
		category			=	CAT_AIR_TO_AIR,
		CLSID				=	clsid,
		Picture				=	"aim54.png",
		wsTypeOfWeapon		=	element.wsTypeOfWeapon,
		attribute			=	{4,	4,	32,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	1.25 * (element.Cx_pil/4096.0),   -- what is reasonable? make it a bit more than the missile itself for now
		Count				=	1,
		Weight				=	element.M + 45.36,  --100lbs for LAU-93
		JettisonSubmunitionOnly = true,
		Elements			=
		{
			{	ShapeName	=	"HB_F14_EXT_SHOULDER_PHX_"..side  ,	IsAdapter  	   =   true  },
			{	payload_CLSID = elem_CLSID , connector_name = "WEP_Phoenix_Connector"}
		}-- end of Elements
	}
    -- actually a LAU-93 adapter, the LAU-93 is internal to the adapter (and the rails also have LAU-93)
	--ret.displayName =	_("LAU-93 ").." "..element.name
	--ret.displayName =	element.name
    ret.displayName = element.user_name
	declare_loadout(ret)
end

-- shoulder phoenix stations
shoulder_aim_54("{SHOULDER AIM_54C_Mk47 L}", AIM_54C_Mk47, "{AIM_54C_Mk47}", "L")
shoulder_aim_54("{SHOULDER AIM_54C_Mk47 R}", AIM_54C_Mk47, "{AIM_54C_Mk47}", "R")
shoulder_aim_54("{SHOULDER AIM_54A_Mk60 L}", AIM_54A_Mk60, "{AIM_54A_Mk60}", "L")
shoulder_aim_54("{SHOULDER AIM_54A_Mk60 R}", AIM_54A_Mk60, "{AIM_54A_Mk60}", "R")
shoulder_aim_54("{SHOULDER AIM_54A_Mk47 L}", AIM_54A_Mk47, "{AIM_54A_Mk47}", "L")
shoulder_aim_54("{SHOULDER AIM_54A_Mk47 R}", AIM_54A_Mk47, "{AIM_54A_Mk47}", "R")

----- sidewinders
-- from aim9_family.lua


local aim9_variants =
{
	["AIM-9"]  		= {display_name = _("AIM-9M")	  	 			,wstype = {4,	4,	7	, AIM_9 },	category = CAT_AIR_TO_AIR, mass = 86.64	},
	["AIM-9P"] 		= {display_name = _("AIM-9P")	  	 			,wstype = {4,	4,	7	, AIM_9P},	category = CAT_AIR_TO_AIR, mass = 86.18	},
	["AIM-9L"]		= {display_name = _("AIM-9L"), wstype = "weapons.missiles.AIM-9L",	category = CAT_AIR_TO_AIR	},
}

local function aim_9_with_adapter(CLSID,aim_9_variant)
	local var 	   = aim9_variants[aim_9_variant] or aim9_variants["AIM-9"]
	local var_mass = var.mass or 85.5
    local name_prefix = "LAU-7 "
	declare_loadout({
		category			= var.category,
		CLSID 				= CLSID,
		Picture				=	"aim9p.png",
		displayName			=	name_prefix..var.display_name,
		wsTypeOfWeapon		=   var.wstype,
		attribute			=	{4,	4,	32,	111},
		Cx_pil				=	1.2*(2.58 / 4096.0),  -- 2.58 from AIM_9L.Cx_pil
		Count				=	1,
		Weight				=	15 + var_mass,
		JettisonSubmunitionOnly = true,
		Elements			=
		{
			{	ShapeName	=	"HB_F14_EXT_LAU-7"	   	  ,	IsAdapter  	   =   true},
			{	ShapeName	=	aim_9_variant	  ,	connector_name =  "WEP_Sidewinder_Extra"},
		}-- end of Elements
	})
end

local function aim_9_without_adapter(CLSID,aim_9_variant,name_prefix)
	local var = aim9_variants[aim_9_variant] or aim9_variants["AIM-9"]
	local var_mass = var.mass or 85.5
	declare_loadout({
		category			= 	var.category,
		CLSID 				= 	CLSID,
		Picture				=	"aim9p.png",
		displayName			=	name_prefix..var.display_name,
		attribute			=	var.wstype,
		Cx_pil				=	2.58 / 4096.0,  -- 2.58 from AIM_9L.Cx_pil
		Count				=	1,
		Weight				=	var_mass,
		Elements			=  {{ShapeName = aim_9_variant}}-- end of Elements
	})
end

--for i,v in ipairs({"L","R"}) do
--end
--aim_9_with_adapter("{LAU-7 - AIM-9P}","AIM-9P")
aim_9_with_adapter("{LAU-7 - AIM-9M}","AIM-9")
aim_9_with_adapter("{LAU-7 - AIM-9L}","AIM-9L")

-- wingtip sidewinders use "adapter" built into the external model
aim_9_without_adapter("{LAU-138 wtip - AIM-9M}","AIM-9","LAU-138 ")
aim_9_without_adapter("{LAU-138 wtip - AIM-9L}","AIM-9L","LAU-138 ")

----- sparrows

local copied_aim7m =
    {
    category		= CAT_AIR_TO_AIR,
    name			= "AIM_7M_HB_copy",
    user_name		= _("AIM-7M"),
	scheme			= "aa_missile_semi_active",
	class_name		= "wAmmunitionSelfHoming",
    wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
        shape_table_data =
        {
            {
                name	 = "HB_F14_EXT_AIM-7";
                file  = "HB_F14_EXT_AIM-7";
                life  = 1;
                fire  = { 0, 1};
                username = "AIM-7M";
                index = WSTYPE_PLACEHOLDER,
            },
        },

	model			= "HB_F14_EXT_AIM-7",
        --Name = AIM_7, --AIM-7M
		display_name = _('AIM-7M'),
		--name = "AIM_7",
	mass = 231,
	Escort = 1,
	Head_Type = 6,
	sigma = {5.6, 5, 5.6},
	M = 231,
	H_max = 24400.0,
	H_min = 1.0,
	Diam = 203.0,
	Cx_pil = 2.21,
	D_max = 20000.0,
	D_min = 700.0,
	Head_Form = 1,
	Life_Time = 90.0,
	Nr_max = 25,
	v_min = 140.0,
	v_mid = 500.0,
	Mach_max = 3.2,
	t_b = 0.0,
	t_acc = 3.3,
	t_marsh = 11.0,
	Range_max = 50000.0,
	H_min_t = 15.0,
	Fi_start = 0.4,
	Fi_rak = 3.14152,
	Fi_excort = 1.05,
	Fi_search = 0.1,
	OmViz_max = 0.35,
	exhaust = {0.78, 0.78, 0.78, 0.3};
	X_back = -2.0,
	Y_back = -0.0,
	Z_back = 0.0, -- -0.1,
	Reflection = 0.0366,
	KillDistance = 12.0,
	ccm_k0 = 0.05,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
	loft = 0,
	hoj = 1,

	warhead		= predefined_warhead("AIM_7"),
	warhead_air = predefined_warhead("AIM_7"),

	PN_coeffs = {2, 				-- Number of Entries
				5000.0 ,1.0,		-- Less 5 km to target Pn = 1
				15000.0, 0.4};		-- Between 15 and 5 km  to target, Pn smoothly changes from 0.4 to 1.0. Longer then 15 km Pn = 0.4.

	supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
	nozzle_exit_area =	0.007238, -- площадь выходного сечения сопла

	ModelData = {   58 ,  -- model params count
					0.9 ,   -- characteristic square (характеристическая площадь)

					-- параметры зависимости Сx
					0.0125 , -- планка Сx0 на дозвуке ( M << 1)
					0.052 , -- высота пика волнового кризиса
					0.010 , -- крутизна фронта на подходе к волновому кризису
					0.002 , -- планка Cx0 на сверхзвуке ( M >> 1)
					0.5  , -- крутизна спада за волновым кризисом
					1.2  , -- коэффициент отвала поляры

					-- параметры зависимости Cy
					2.20, -- планка Cya на дозвуке ( M << 1)
					1.05, -- планка Cya на сверхзвуке ( M >> 1)
					1.20, -- крутизна спада(фронта) за волновым кризисом

					0.18, -- ~10 degrees Alfa_max  максимальный балансировачный угол, радианы
					0.00, --угловая скорость создаваймая моментом газовых рулей

					--	t_statr   t_b      t_accel  t_march   t_inertial   t_break  t_end
					-1.0,    -1.0 ,  	3.7  ,  10.8,      0.0,		   0.0,      1.0e9,           -- time interval
					 0.0,     0.0 ,   	10.4 ,  2.02,     0.0,         0.0,      0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек)
					 0.0,     0.0 ,   25192.0,  4140.0,   0.0,         0.0,      0.0,           -- thrust

					 1.0e9, -- таймер самоликвидации, сек
					 75.0, -- время работы энергосистемы
					 0.0, -- абсалютеая высота самоликвидации, м
					 1.5, -- время задержки включения управленя, сек
					 5000, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты
					 15000, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр)
					 0.1,  -- синус угла возвышения траектории набора горки
					 50.0, -- продольное ускорения взведения взрывателя
					 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
					 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
					 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
					 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
					 6800.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
					 3.8, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
					 30.0, --  коэф поправки к дальности от скорости носителя
					 0.75, -- безразмерный коэф. эффективности САУ ракеты
					 43.0, -- Прогноз времени полета ракеты
					-- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
					 38000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
					 14500.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
					 24000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
					 0.2,     -- Коэффициент уменьшения дальности при увеличения угла между векторм скорости носителя и линией визирования цели
					 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
					 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
					-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
					0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
				},
	controller = {
		boost_start = 0.5,
		march_start = 4.2,
	},

	boost = {
		impulse								= 247,
		fuel_mass							= 38.48,
		work_time							= 3.7,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
	},

	march = {
		impulse								= 209,
		fuel_mass							= 21.82,
		work_time							= 10.8,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
	},

	fm = {
		mass				= 231,
		caliber				= 0.2,
		wind_sigma			= 0.0,
		wind_time			= 0.0,
		tail_first			= 1,
		fins_part_val		= 0,
		rotated_fins_inp	= 0,
		delta_max			= math.rad(20),
		draw_fins_conv		= {math.rad(90),1,1},
		L					= 0.2,
		S					= 0.0324,
		Ix					= 3.5,
		Iy					= 127.4,
		Iz					= 127.4,

		Mxd					= 0.3 * 57.3,
		Mxw					= -44.5,

		table_scale	= 0.2,
		table_degree_values = 1,
	--	Mach	  | 0.0		0.2		0.4		0.6		0.8		1.0		1.2		1.4		1.6		1.8		2.0		2.2		2.4		2.6		2.8		3.0		3.2		3.4		3.6		3.8		4.0	 |
		Cx0 	= {	0.34,	0.34,	0.34,	0.34,	0.35,	1.10,	1.27,	1.23,	1.19,	1.12,	1.05,	1.0,	0.95,	0.91,	0.87,	0.84,	0.81,	0.78,	0.76,	0.74,	0.72 },
		CxB 	= {	0.11,	0.11,	0.11,	0.11,	0.11,	0.40,	0.19,	0.17,	0.16,	0.14,	0.13,	0.12,	0.12,	0.11,	0.11,	0.10,	0.09,	0.09,	0.08,	0.08,	0.07 },
		K1		= { 0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0052,	0.0048,	0.0045,	0.0041,	0.0037,	0.0036,	0.0034,	0.0032,	0.0031,	0.0030,	0.0029,	0.0027,	0.0026 },
		K2		= { 0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0051,	0.0047,	0.0043,	0.0037,	0.0031,	0.0032,	0.0033,	0.0035,	0.0036,	0.0037,	0.0038,	0.0039,	0.0040 },
		Cya		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Cza		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Mya		= { -0.5,	-0.5},
		Mza		= { -0.5,	-0.5},
		Myw		= { -2.0,	-2.0},
		Mzw		= { -2.0,	-2.0},
		A1trim	= { 10.0,	10.0},
		A2trim	= { 10.0,	10.0},

		model_roll = math.rad(45),
		fins_stall = 1,
	},

	proximity_fuze = {
		radius		= 12,
		arm_delay	= 1.6,
	},

	seeker = {
		delay					= 1.5,
		op_time					= 75,
		FOV						= math.rad(120),
		max_w_LOS				= math.rad(20),
		sens_near_dist			= 100,
		sens_far_dist			= 30000,
		ccm_k0					= 0.05,
		aim_sigma				= 5.5,
		height_error_k			= 100;
		height_error_max_vel	= 138;
		height_error_max_h		= 300;
		hoj						= 1,
	},

	autopilot = {
		x_channel_delay		= 0.9,
		delay				= 1.5,
		op_time				= 75,
		Kconv				= 4.0,
		Knv					= 0.02,
		Kd					= 0.4,
		Ki					= 0.1,
		Kout				= 1.0,
		Kx					= 0.1,
		Krx					= 2.0,
		fins_limit			= math.rad(20),
		fins_limit_x		= math.rad(5),
		Areq_limit			= 25.0,
		bang_bang			= 0,
		max_side_N			= 10,
		max_signal_Fi		= math.rad(12),
		rotate_fins_output	= 0,
		alg					= 0,
		PN_dist_data 		= {	15000,	1,
								9000,	1},
		null_roll			= math.rad(45),

		loft_active_by_default	= 0,
		loft_add_pitch			= math.rad(30),
		loft_time				= 3.5,
		loft_min_dist			= 6500,
		loft_max_dist			= 20000,
	},

    }
declare_weapon(copied_aim7m)

-- from aim7_family.lua

local copied_aim7f =
{
	category		= CAT_AIR_TO_AIR,
	name			= "AIM-7F_HB_copy",
	user_name		= _("AIM-7F"),
	scheme			= "aa_missile_semi_active",
	class_name		= "wAmmunitionSelfHoming",
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
        shape_table_data =
        {
            {
                name	 = "HB_F14_AIM7F";
                file  = "HB_F14_EXT_AIM-7";
                life  = 1;
                fire  = { 0, 1};
                username = "HB_F14_AIM7F";
                index = WSTYPE_PLACEHOLDER,
            },
        },

	model			= "HB_F14_EXT_AIM-7",
	display_name = _('AIM-7F'),

	mass = 231,
	Escort = 1,
	Head_Type = 6,
	sigma = {5.6, 5, 5.6},
	M = 231,
	H_max = 24400.0,
	H_min = 1.0,
	Diam = 203.0,
	Cx_pil = 2.21,
	D_max = 20000.0,
	D_min = 700.0,
	Head_Form = 1,
	Life_Time = 90.0,
	Nr_max = 25,
	v_min = 140.0,
	v_mid = 500.0,
	Mach_max = 3.2,
	t_b = 0.0,
	t_acc = 3.3,
	t_marsh = 11.0,
	Range_max = 50000.0,
	H_min_t = 15.0,
	Fi_start = 0.4,
	Fi_rak = 3.14152,
	Fi_excort = 1.05,
	Fi_search = 0.1,
	OmViz_max = 0.35,
	exhaust = {0.78, 0.78, 0.78, 0.3};
	X_back = -2.0,
	Y_back = -0.0,
	Z_back = 0.0, -- -0.1,
	Reflection = 0.0366,
	KillDistance = 12.0,
	ccm_k0 = 0.05,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1. The curve is non linear!
	rad_correction = 0,

	-- The guidance error from the influence of the surface at low altitudes
	height_error_k = 110, -- ÐµÑÐ»Ð¸ Ð¿ÑÐ¾ÐµÐºÑÐ¸Ñ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ ÑÐµÐ»Ð¸ Ð½Ð° Ð»Ð¸Ð½Ð¸Ñ Ð²Ð¸Ð·Ð¸ÑÐ¾Ð²Ð°Ð½Ð¸Ñ Ð¼ÐµÐ½ÑÑÐµ ÑÑÐ¾Ð³Ð¾ Ð·Ð½Ð°ÑÐµÐ½Ð¸Ñ, Ð¿Ð¾ÑÐ²Ð»ÑÐµÑÑÑ Ð¾ÑÐ¸Ð±ÐºÐ°
	height_error_max_vel = 150, -- Ð¿ÑÐ¾Ð¿Ð¾ÑÑÐ¸Ð¾Ð½Ð°Ð»ÑÐ½ÑÐ¹ ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½Ñ
	height_error_max_h = 450, -- Ð¼Ð°ÐºÑÐ¸Ð¼Ð°Ð»ÑÐ½Ð°Ñ Ð²ÑÑÐ¾ÑÐ°, Ð³Ð´Ðµ Ð¿Ð¾ÑÐ²Ð»ÑÐµÑÑÑ Ð¾ÑÐ¸Ð±ÐºÐ°

	warhead		= predefined_warhead("AIM_7"),
	warhead_air = predefined_warhead("AIM_7"),

	hoj = 1,

	supersonic_A_coef_skew = 0.25, -- Ð½Ð°ÐºÐ»Ð¾Ð½ Ð¿ÑÑÐ¼Ð¾Ð¹ ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½ÑÐ° Ð¾ÑÐ²Ð°Ð»Ð° Ð¿Ð¾Ð»ÑÑÑ Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ
	nozzle_exit_area =	0.007238, -- Ð¿Ð»Ð¾ÑÐ°Ð´Ñ Ð²ÑÑÐ¾Ð´Ð½Ð¾Ð³Ð¾ ÑÐµÑÐµÐ½Ð¸Ñ ÑÐ¾Ð¿Ð»Ð°

	ModelData = {   58 ,  -- model params count
					0.9 ,   -- characteristic square (ÑÐ°ÑÐ°ÐºÑÐµÑÐ¸ÑÑÐ¸ÑÐµÑÐºÐ°Ñ Ð¿Ð»Ð¾ÑÐ°Ð´Ñ)

					-- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ Ð¡x
					0.0125 , -- Ð¿Ð»Ð°Ð½ÐºÐ° Ð¡x0 Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
					0.052 , -- Ð²ÑÑÐ¾ÑÐ° Ð¿Ð¸ÐºÐ° Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð³Ð¾ ÐºÑÐ¸Ð·Ð¸ÑÐ°
					0.010 , -- ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÑÐ¾Ð½ÑÐ° Ð½Ð° Ð¿Ð¾Ð´ÑÐ¾Ð´Ðµ Ðº Ð²Ð¾Ð»Ð½Ð¾Ð²Ð¾Ð¼Ñ ÐºÑÐ¸Ð·Ð¸ÑÑ
					0.002 , -- Ð¿Ð»Ð°Ð½ÐºÐ° Cx0 Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
					0.5  , -- ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð° Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼
					1.2  , -- ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½Ñ Ð¾ÑÐ²Ð°Ð»Ð° Ð¿Ð¾Ð»ÑÑÑ

					-- Ð¿Ð°ÑÐ°Ð¼ÐµÑÑÑ Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸ Cy
					2.20, -- Ð¿Ð»Ð°Ð½ÐºÐ° Cya Ð½Ð° Ð´Ð¾Ð·Ð²ÑÐºÐµ ( M << 1)
					1.05, -- Ð¿Ð»Ð°Ð½ÐºÐ° Cya Ð½Ð° ÑÐ²ÐµÑÑÐ·Ð²ÑÐºÐµ ( M >> 1)
					1.20, -- ÐºÑÑÑÐ¸Ð·Ð½Ð° ÑÐ¿Ð°Ð´Ð°(ÑÑÐ¾Ð½ÑÐ°) Ð·Ð° Ð²Ð¾Ð»Ð½Ð¾Ð²ÑÐ¼ ÐºÑÐ¸Ð·Ð¸ÑÐ¾Ð¼

					0.18, -- ~10 degrees Alfa_max  Ð¼Ð°ÐºÑÐ¸Ð¼Ð°Ð»ÑÐ½ÑÐ¹ Ð±Ð°Ð»Ð°Ð½ÑÐ¸ÑÐ¾Ð²Ð°ÑÐ½ÑÐ¹ ÑÐ³Ð¾Ð», ÑÐ°Ð´Ð¸Ð°Ð½Ñ
					0.00, --ÑÐ³Ð»Ð¾Ð²Ð°Ñ ÑÐºÐ¾ÑÐ¾ÑÑÑ ÑÐ¾Ð·Ð´Ð°Ð²Ð°Ð¹Ð¼Ð°Ñ Ð¼Ð¾Ð¼ÐµÐ½ÑÐ¾Ð¼ Ð³Ð°Ð·Ð¾Ð²ÑÑ ÑÑÐ»ÐµÐ¹

					--	t_statr   t_b      t_accel  t_march   t_inertial   t_break  t_end
					-1.0,    -1.0 ,  	3.7  ,  10.8,      0.0,		   0.0,      1.0e9,           -- time interval
					 0.0,     0.0 ,   	10.4 ,  2.02,     0.0,         0.0,      0.0,           -- fuel flow rate in second kg/sec(ÑÐµÐºÑÐ½Ð´Ð½ÑÐ¹ ÑÐ°ÑÑÐ¾Ð´ Ð¼Ð°ÑÑÑ ÑÐ¾Ð¿Ð»Ð¸Ð²Ð° ÐºÐ³/ÑÐµÐº)
					 0.0,     0.0 ,   25192.0,  4140.0,   0.0,         0.0,      0.0,           -- thrust

					 1.0e9, -- ÑÐ°Ð¹Ð¼ÐµÑ ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, ÑÐµÐº
					 75.0, -- Ð²ÑÐµÐ¼Ñ ÑÐ°Ð±Ð¾ÑÑ ÑÐ½ÐµÑÐ³Ð¾ÑÐ¸ÑÑÐµÐ¼Ñ
					 0.0, -- Ð°Ð±ÑÐ°Ð»ÑÑÐµÐ°Ñ Ð²ÑÑÐ¾ÑÐ° ÑÐ°Ð¼Ð¾Ð»Ð¸ÐºÐ²Ð¸Ð´Ð°ÑÐ¸Ð¸, Ð¼
					 1.5, -- Ð²ÑÐµÐ¼Ñ Ð·Ð°Ð´ÐµÑÐ¶ÐºÐ¸ Ð²ÐºÐ»ÑÑÐµÐ½Ð¸Ñ ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ñ, ÑÐµÐº
					 1.0e9, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸ Ð² Ð¼Ð¾Ð¼ÐµÐ½Ñ Ð¿ÑÑÐºÐ°, Ð²ÑÑÐµ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ Ð²ÑÐ¿Ð¾Ð»Ð½ÑÐµÑÑÑ Ð¼Ð°Ð½ÐµÐ²Ñ Ð½Ð°Ð±Ð¾ÑÐ° Ð²ÑÑÐ¾ÑÑ
					 1.0e9, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð´Ð¾ ÑÐµÐ»Ð¸ Ð½Ð° ÑÑÐ°ÑÑÐµ, Ð¼ÐµÐ½ÐµÐµ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ Ð½Ð°ÑÐ¸Ð½Ð°ÐµÑÑÑ Ð·Ð°Ð²ÐµÑÑÐµÐ½Ð¸Ðµ Ð¼Ð°Ð½ÐµÐ²ÑÐ° Ð½Ð°Ð±Ð¾ÑÐ° Ð²ÑÑÐ¾ÑÑ (Ð´Ð»Ð¶ÐµÐ½ Ð±ÑÑÑ Ð±Ð¾Ð»ÑÑÐµ ÑÐµÐ¼ Ð¿ÑÐµÐ´ÑÐ»ÑÑÐ¸Ð¹ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑ)
					 0.0,  -- ÑÐ¸Ð½ÑÑ ÑÐ³Ð»Ð° Ð²Ð¾Ð·Ð²ÑÑÐµÐ½Ð¸Ñ ÑÑÐ°ÐµÐºÑÐ¾ÑÐ¸Ð¸ Ð½Ð°Ð±Ð¾ÑÐ° Ð³Ð¾ÑÐºÐ¸
					 50.0, -- Ð¿ÑÐ¾Ð´Ð¾Ð»ÑÐ½Ð¾Ðµ ÑÑÐºÐ¾ÑÐµÐ½Ð¸Ñ Ð²Ð·Ð²ÐµÐ´ÐµÐ½Ð¸Ñ Ð²Ð·ÑÑÐ²Ð°ÑÐµÐ»Ñ
					 0.0, -- Ð¼Ð¾Ð´ÑÐ»Ñ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ ÑÐ¾Ð¾Ð±ÑÐ°Ð¹Ð¼ÑÐ¹ ÐºÐ°ÑÐ°Ð¿ÑÐ»ÑÑÐ½ÑÐ¼ ÑÑÑÑÐ¾Ð¹ÑÑÐ²Ð¾Ð¼, Ð²ÑÑÐ¸Ð±Ð½ÑÐ¼ Ð·Ð°ÑÑÐ´Ð¾Ð¼ Ð¸ ÑÐ´
					 1.19, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ Ð¡ÐÐ£-Ð ÐÐÐÐ¢Ð,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÑÐ´ÐºÐ° K0
					 1.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ Ð¡ÐÐ£-Ð ÐÐÐÐ¢Ð,  ÐºÐ¾ÑÑ ÑÐ¸Ð»ÑÑÑÐ° Ð²ÑÐ¾ÑÐ¾Ð³Ð¾ Ð¿Ð¾ÑÑÐ´ÐºÐ° K1
					 2.0, -- ÑÐ°ÑÐ°ÐºÑÑÐ¸ÑÑÐ¸ÐºÐ° ÑÐ¸ÑÑÐµÐ¼Ñ Ð¡ÐÐ£-Ð ÐÐÐÐ¢Ð,  Ð¿Ð¾Ð»Ð¾ÑÐ° Ð¿ÑÐ¾Ð¿ÑÑÐºÐ°Ð½Ð¸Ñ ÐºÐ¾Ð½ÑÑÑÐ° ÑÐ¿ÑÐ°Ð²Ð»ÐµÐ½Ð¸Ñ
					 6800.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð½Ð° Ð²ÑÑÐ¾ÑÐµ H=2000
					 3.8, -- ÐºÑÑÑÐ¸Ð·Ð½Ð° Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑÐ¸  Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ Ð¿Ð¾Ð»ÐµÑÐ° Ð² Ð³Ð¾ÑÐ¸Ð·Ð¾Ð½Ñ Ñ ÑÐ°ÑÐ¿Ð¾Ð»Ð°Ð³Ð°ÐµÐ¼Ð¾Ð¹ Ð¿ÐµÑÐµÐ³ÑÑÐ·ÐºÐ¾Ð¹ Navail >= 1.0 Ð¾Ñ Ð²ÑÑÐ¾ÑÑ H
					 30.0, --  ÐºÐ¾ÑÑ Ð¿Ð¾Ð¿ÑÐ°Ð²ÐºÐ¸ Ðº Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸ Ð¾Ñ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ Ð½Ð¾ÑÐ¸ÑÐµÐ»Ñ
					 0.75, -- Ð±ÐµÐ·ÑÐ°Ð·Ð¼ÐµÑÐ½ÑÐ¹ ÐºÐ¾ÑÑ. ÑÑÑÐµÐºÑÐ¸Ð²Ð½Ð¾ÑÑÐ¸ Ð¡ÐÐ£ ÑÐ°ÐºÐµÑÑ
					 43.0, -- ÐÑÐ¾Ð³Ð½Ð¾Ð· Ð²ÑÐµÐ¼ÐµÐ½Ð¸ Ð¿Ð¾Ð»ÐµÑÐ° ÑÐ°ÐºÐµÑÑ
					-- DLZ. ÐÐ°Ð½Ð½ÑÐµ Ð´Ð»Ñ ÑÐ°ÑÑÑÐµÑÐ° Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐµÐ¹ Ð¿ÑÑÐºÐ° (Ð¸Ð½Ð´Ð¸ÐºÐ°ÑÐ¸Ñ Ð½Ð° Ð¿ÑÐ¸ÑÐµÐ»Ðµ)
					 38000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ   180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ, Ð¼
					 14500.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ 0(Ð² Ð´Ð¾Ð³Ð¾Ð½) Ð³ÑÐ°Ð´,  Ð=10000Ð¼, V=900ÐºÐ¼/Ñ
					 24000.0, -- Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÑ ÑÐ°ÐºÑÑÑ 	180(Ð½Ð°Ð²ÑÑÑÐµÑÑ) Ð³ÑÐ°Ð´, Ð=1000Ð¼, V=900ÐºÐ¼/Ñ
					 0.2,     -- ÐÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½Ñ ÑÐ¼ÐµÐ½ÑÑÐµÐ½Ð¸Ñ Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸ Ð¿ÑÐ¸ ÑÐ²ÐµÐ»Ð¸ÑÐµÐ½Ð¸Ñ ÑÐ³Ð»Ð° Ð¼ÐµÐ¶Ð´Ñ Ð²ÐµÐºÑÐ¾ÑÐ¼ ÑÐºÐ¾ÑÐ¾ÑÑÐ¸ Ð½Ð¾ÑÐ¸ÑÐµÐ»Ñ Ð¸ Ð»Ð¸Ð½Ð¸ÐµÐ¹ Ð²Ð¸Ð·Ð¸ÑÐ¾Ð²Ð°Ð½Ð¸Ñ ÑÐµÐ»Ð¸
					 0.7, -- ÐÐµÑÑÐ¸ÐºÐ°Ð»ÑÐ½Ð°Ñ Ð¿Ð»Ð¾ÑÐºÐ¾ÑÑÑ. ÐÐ°ÐºÐ»Ð¾Ð½ ÐºÑÐ¸Ð²Ð¾Ð¹ ÑÐ°Ð·ÑÐµÑÐµÐ½Ð½Ð¾Ð¹ Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸ Ð¿ÑÑÐºÐ° Ð² Ð½Ð¸Ð¶Ð½ÑÑ Ð¿Ð¾Ð»ÑÑÑÐµÑÑ. Ð£Ð¼ÐµÐ½ÑÑÐµÐ½Ð¸Ðµ Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸ Ð¿ÑÐ¸ ÑÑÑÐµÐ»ÑÐ±Ðµ Ð²Ð½Ð¸Ð·.
					 2.0, -- ÐÐµÑÑÐ¸ÐºÐ°Ð»ÑÐ½Ð°Ñ Ð¿Ð»Ð¾ÑÐºÐ¾ÑÑÑ. ÐÐ°ÐºÐ»Ð¾Ð½ ÐºÑÐ¸Ð²Ð¾Ð¹ ÑÐ°Ð·ÑÐµÑÐµÐ½Ð½Ð¾Ð¹ Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸ Ð¿ÑÑÐºÐ° Ð² Ð²ÐµÑÑÐ½ÑÑ Ð¿Ð¾Ð»ÑÑÑÐµÑÑ. Ð£Ð²ÐµÐ»Ð¸ÑÐµÐ½Ð¸Ðµ Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸ Ð¿ÑÐ¸ ÑÑÑÐµÐ»ÑÐ±Ðµ Ð²Ð²ÐµÑÑ.
					-3.0, -- ÐÐµÑÑÐ¸ÐºÐ°Ð»ÑÐ½Ð°Ñ Ð¿Ð»Ð¾ÑÐºÐ¾ÑÑÑ. Ð£Ð³Ð¾Ð» Ð¿ÐµÑÐµÐ³Ð¸Ð±Ð° ÐºÑÐ¸Ð²Ð¾Ð¹ ÑÐ°Ð·ÑÐµÑÐµÐ½Ð½Ð¾Ð¹ Ð´Ð°Ð»ÑÐ½Ð¾ÑÑÐ¸, Ð²ÐµÑÑÐ½ÑÑ - Ð½Ð¸Ð¶Ð½ÑÑ Ð¿Ð¾Ð»ÑÑÑÐµÑÐ°.
					0.5, -- ÐÐ·Ð¼ÐµÐ½ÐµÐ½Ð¸Ðµ ÐºÐ¾ÑÑÑÐ¸ÑÐ¸ÐµÐ½ÑÐ¾Ð² Ð½Ð°ÐºÐ»Ð¾Ð½Ð° ÐºÑÐ¸Ð²Ð¾Ð¹ Ð² Ð²ÐµÑÑÐ½ÑÑ Ð¸ Ð½Ð¸Ð¶Ð½ÑÑ Ð¿Ð¾Ð»ÑÑÑÐµÑÑ Ð¾Ñ Ð²ÑÑÐ¾ÑÑ Ð½Ð¾ÑÐ¸ÑÐµÐ»Ñ.
				},

	controller = {
		boost_start = 0.5,
		march_start = 4.2,
	},

	boost = {
		impulse								= 247,
		fuel_mass							= 38.48,
		work_time							= 3.7,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
	},

	march = {
		impulse								= 209,
		fuel_mass							= 21.82,
		work_time							= 10.8,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
	},

	fm = {
		mass				= 231,
		caliber				= 0.2,
		wind_sigma			= 0.0,
		wind_time			= 0.0,
		tail_first			= 1,
		fins_part_val		= 0,
		rotated_fins_inp	= 0,
		delta_max			= math.rad(20),
		L					= 0.2,
		S					= 0.0324,
		Ix					= 3.5,
		Iy					= 127.4,
		Iz					= 127.4,

		Mxd					= 0.3 * 57.3,
		Mxw					= -44.5,

		table_scale	= 0.2,
		table_degree_values = 1,
	--	Mach	  | 0.0		0.2		0.4		0.6		0.8		1.0		1.2		1.4		1.6		1.8		2.0		2.2		2.4		2.6		2.8		3.0		3.2		3.4		3.6		3.8		4.0	 |
		Cx0 	= {	0.34,	0.34,	0.34,	0.34,	0.35,	1.10,	1.27,	1.23,	1.19,	1.12,	1.05,	1.0,	0.95,	0.91,	0.87,	0.84,	0.81,	0.78,	0.76,	0.74,	0.72 },
		CxB 	= {	0.11,	0.11,	0.11,	0.11,	0.11,	0.40,	0.19,	0.17,	0.16,	0.14,	0.13,	0.12,	0.12,	0.11,	0.11,	0.10,	0.09,	0.09,	0.08,	0.08,	0.07 },
		K1		= { 0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0052,	0.0048,	0.0045,	0.0041,	0.0037,	0.0036,	0.0034,	0.0032,	0.0031,	0.0030,	0.0029,	0.0027,	0.0026 },
		K2		= { 0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0051,	0.0047,	0.0043,	0.0037,	0.0031,	0.0032,	0.0033,	0.0035,	0.0036,	0.0037,	0.0038,	0.0039,	0.0040 },
		Cya		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Cza		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Mya		= { -0.5,	-0.5},
		Mza		= { -0.5,	-0.5},
		Myw		= { -2.0,	-2.0},
		Mzw		= { -2.0,	-2.0},
		A1trim	= { 10.0,	10.0},
		A2trim	= { 10.0,	10.0},

		model_roll = math.rad(45),
		fins_stall = 1,
	},

	proximity_fuze = {
		radius		= 12,
		arm_delay	= 1.6,
	},

	seeker = {
		delay					= 1.5,
		op_time					= 75,
		FOV						= math.rad(120),
		max_w_LOS				= math.rad(20),
		sens_near_dist			= 100,
		sens_far_dist			= 30000,
		ccm_k0					= 0.05,
		aim_sigma				= 5.5,
		height_error_k			= 110,
		height_error_max_vel	= 150,
		height_error_max_h		= 450,
		rad_correction			= 0,
		hoj						= 1,
	},

	autopilot = {
		x_channel_delay = 0.9,
		delay				= 1.5,
		op_time				= 75,
		Kconv				= 3.0,
		Knv					= 0.005,
		Kd					= 0.4,
		Ki					= 0.25,
		Kout				= 1.0,
		Kx					= 0.1,
		Krx					= 2.0,
		fins_limit			= math.rad(20),
		fins_limit_x		= math.rad(5),
		Areq_limit			= 25.0,
		bang_bang			= 0,
		max_side_N			= 10,
		max_signal_Fi		= math.rad(12),
		rotate_fins_output	= 0,
		alg					= 0,
		PN_dist_data 		= {	15000,	1,
								9000,	1	},
		draw_fins_conv		= {math.rad(90),1,1},
		null_roll			= math.rad(45),

		loft_active_by_default	= 0,
		loft_add_pitch			= math.rad(30),
		loft_time				= 3.5,
		loft_min_dist			= 6500,
		loft_max_dist			= 20000,
	},
}
declare_weapon(copied_aim7f)

local copied_aim7mh =
{
	category		= CAT_AIR_TO_AIR,
	name			= "AIM-7MH_HB_copy",
	user_name		= _("AIM-7MH"),
	scheme			= "aa_missile_semi_active",
	class_name		= "wAmmunitionSelfHoming",
	--model			= "aim-7",
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
        shape_table_data =
        {
            {
                name	 = "HB_F14_AIM7MH";
                file  = "HB_F14_EXT_AIM-7";
                life  = 1;
                fire  = { 0, 1};
                username = "HB_F14_AIM7MH";
                index = WSTYPE_PLACEHOLDER,
            },
        },

	model			= "HB_F14_EXT_AIM-7",
	display_name = _('AIM-7MH'),

	mass = 231,
	Escort = 1,
	Head_Type = 6,
	sigma = {5.6, 5, 5.6},
	M = 231,
	H_max = 24400.0,
	H_min = 1.0,
	Diam = 203.0,
	Cx_pil = 2.21,
	D_max = 20000.0,
	D_min = 700.0,
	Head_Form = 1,
	Life_Time = 90.0,
	Nr_max = 25,
	v_min = 140.0,
	v_mid = 500.0,
	Mach_max = 3.2,
	t_b = 0.0,
	t_acc = 3.3,
	t_marsh = 11.0,
	Range_max = 50000.0,
	H_min_t = 15.0,
	Fi_start = 0.4,
	Fi_rak = 3.14152,
	Fi_excort = 1.05,
	Fi_search = 0.1,
	OmViz_max = 0.35,
	exhaust = {0.78, 0.78, 0.78, 0.3};
	X_back = -2.0,
	Y_back = -0.0,
	Z_back = 0.0, -- -0.1,
	Reflection = 0.0366,
	KillDistance = 12.0,
	ccm_k0 = 0.05,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
	loft = 1,
	hoj = 1,

	warhead		= predefined_warhead("AIM_7"),
	warhead_air = predefined_warhead("AIM_7"),

	PN_coeffs = {2, 				-- Number of Entries
				5000.0 ,1.0,		-- Less 5 km to target Pn = 1
				15000.0, 0.4};		-- Between 15 and 5 km  to target, Pn smoothly changes from 0.4 to 1.0. Longer then 15 km Pn = 0.4.

	supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
	nozzle_exit_area =	0.007238, -- площадь выходного сечения сопла

	ModelData = {   58 ,  -- model params count
					0.9 ,   -- characteristic square (характеристическая площадь)

					-- параметры зависимости Сx
					0.0125 , -- планка Сx0 на дозвуке ( M << 1)
					0.052 , -- высота пика волнового кризиса
					0.010 , -- крутизна фронта на подходе к волновому кризису
					0.002 , -- планка Cx0 на сверхзвуке ( M >> 1)
					0.5  , -- крутизна спада за волновым кризисом
					1.2  , -- коэффициент отвала поляры

					-- параметры зависимости Cy
					2.20, -- планка Cya на дозвуке ( M << 1)
					1.05, -- планка Cya на сверхзвуке ( M >> 1)
					1.20, -- крутизна спада(фронта) за волновым кризисом

					0.18, -- ~10 degrees Alfa_max  максимальный балансировачный угол, радианы
					0.00, --угловая скорость создаваймая моментом газовых рулей

					--	t_statr   t_b      t_accel  t_march   t_inertial   t_break  t_end
					-1.0,    -1.0 ,  	3.7  ,  10.8,      0.0,		   0.0,      1.0e9,           -- time interval
					 0.0,     0.0 ,   	10.4 ,  2.02,     0.0,         0.0,      0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек)
					 0.0,     0.0 ,   25192.0,  4140.0,   0.0,         0.0,      0.0,           -- thrust

					 1.0e9, -- таймер самоликвидации, сек
					 75.0, -- время работы энергосистемы
					 0.0, -- абсалютеая высота самоликвидации, м
					 1.5, -- время задержки включения управленя, сек
					 5000, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты
					 15000, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр)
					 0.1,  -- синус угла возвышения траектории набора горки
					 50.0, -- продольное ускорения взведения взрывателя
					 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
					 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
					 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
					 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
					 6800.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
					 3.8, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
					 30.0, --  коэф поправки к дальности от скорости носителя
					 0.75, -- безразмерный коэф. эффективности САУ ракеты
					 43.0, -- Прогноз времени полета ракеты
					-- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
					 38000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
					 14500.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
					 24000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
					 0.2,     -- Коэффициент уменьшения дальности при увеличения угла между векторм скорости носителя и линией визирования цели
					 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
					 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
					-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
					0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
				},

	controller = {
		boost_start = 0.5,
		march_start = 4.2,
	},

	boost = {
		impulse								= 247,
		fuel_mass							= 38.48,
		work_time							= 3.7,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
	},

	march = {
		impulse								= 209,
		fuel_mass							= 21.82,
		work_time							= 10.8,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
	},

	fm = {
		mass				= 231,
		caliber				= 0.2,
		wind_sigma			= 0.0,
		wind_time			= 0.0,
		tail_first			= 1,
		fins_part_val		= 0,
		rotated_fins_inp	= 0,
		delta_max			= math.rad(20),
		L					= 0.2,
		S					= 0.0324,
		Ix					= 3.5,
		Iy					= 127.4,
		Iz					= 127.4,

		Mxd					= 0.3 * 57.3,
		Mxw					= -44.5,

		table_scale	= 0.2,
		table_degree_values = 1,
	--	Mach	  | 0.0		0.2		0.4		0.6		0.8		1.0		1.2		1.4		1.6		1.8		2.0		2.2		2.4		2.6		2.8		3.0		3.2		3.4		3.6		3.8		4.0	 |
		Cx0 	= {	0.34,	0.34,	0.34,	0.34,	0.35,	1.10,	1.27,	1.23,	1.19,	1.12,	1.05,	1.0,	0.95,	0.91,	0.87,	0.84,	0.81,	0.78,	0.76,	0.74,	0.72 },
		CxB 	= {	0.11,	0.11,	0.11,	0.11,	0.11,	0.40,	0.19,	0.17,	0.16,	0.14,	0.13,	0.12,	0.12,	0.11,	0.11,	0.10,	0.09,	0.09,	0.08,	0.08,	0.07 },
		K1		= { 0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0052,	0.0048,	0.0045,	0.0041,	0.0037,	0.0036,	0.0034,	0.0032,	0.0031,	0.0030,	0.0029,	0.0027,	0.0026 },
		K2		= { 0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0051,	0.0047,	0.0043,	0.0037,	0.0031,	0.0032,	0.0033,	0.0035,	0.0036,	0.0037,	0.0038,	0.0039,	0.0040 },
		Cya		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Cza		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Mya		= { -0.5,	-0.5},
		Mza		= { -0.5,	-0.5},
		Myw		= { -2.0,	-2.0},
		Mzw		= { -2.0,	-2.0},
		A1trim	= { 10.0,	10.0},
		A2trim	= { 10.0,	10.0},

		model_roll = math.rad(45),
		fins_stall = 1,
	},

	proximity_fuze = {
		radius		= 12,
		arm_delay	= 1.6,
	},

	seeker = {
		delay					= 1.5,
		op_time					= 75,
		FOV						= math.rad(120),
		max_w_LOS				= math.rad(20),
		sens_near_dist			= 100,
		sens_far_dist			= 30000,
		ccm_k0					= 0.5,
		aim_sigma				= 5.5,
		height_error_k			= 100;
		height_error_max_vel	= 138;
		height_error_max_h		= 300;
		hoj						= 1,
	},

	autopilot = {
		x_channel_delay		= 0.9,
		delay				= 1.6,
		op_time				= 75,
		Kconv				= 3.0,
		Knv					= 0.005,
		Kd					= 0.4,
		Ki					= 0.25,
		Kout				= 1.0,
		Kx					= 0.1,
		Krx					= 2.0,
		fins_limit			= math.rad(20),
		fins_limit_x		= math.rad(5),
		Areq_limit			= 25.0,
		bang_bang			= 0,
		max_side_N			= 10,
		max_signal_Fi		= math.rad(12),
		rotate_fins_output	= 0,
		alg					= 0,
		PN_dist_data 		= {	15000,	1,
								9000,	1	},
		draw_fins_conv		= {math.rad(90),1,1},
		null_roll			= math.rad(45),

		loft_active_by_default	= 1,
		loft_add_pitch			= math.rad(30),
		loft_time				= 3.5,
		loft_min_dist			= 6500,
		loft_max_dist			= 20000,
	},
}
declare_weapon(copied_aim7mh)


local function f14_shoulder_aim_7(clsid, missilename, wstype)
	local ret = {
		category			=	CAT_AIR_TO_AIR,
		CLSID				=	clsid,
		Picture				=	"aim7_r.png",
        wsTypeOfWeapon		=   wstype, -- {4,	4,	7,	21},
		attribute			=	{4,	4,	32,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	1.5 * (2.21 / 4096.0),  -- 2.21 from AIM-7 definition
		Count				=	1,
		Weight				=	54.4 + 230,
		JettisonSubmunitionOnly = true,
		Elements			=
		{
			{	ShapeName	=	"HB_F14_EXT_SPARROW_PYLON" ,IsAdapter  =   true  },
			{	ShapeName	=	"HB_F14_EXT_AIM-7" , connector_name =  "WEP_Sparrow_Connector", use_full_connector_position = true}
		}-- end of Elements
	}
    -- actually LAU-92 adapter, the LAU-92 is internal to the adapter and also internal in the F-14 belly sparrow slots
	--ret.displayName =	_("LAU-92").." "..missilename
    ret.displayName =	missilename
	declare_loadout(ret)
end

local function f14_belly_aim_7(clsid, missilename, wstype, mass)
	local ret = {
		category			=	CAT_AIR_TO_AIR,
		CLSID				=	clsid,
		Picture				=	"aim7_r.png",
    --wsTypeOfWeapon	=	copied_aim7m.wsTypeOfWeapon,
    --attribute		=	{wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
    Count			=	1,
    Weight			=	mass,
        attribute		=   wstype, --{4,	4,	7,	21},
		Cx_pil				=	0.001959765625,
		Count				=	1,
		Weight				=	230,
		Weight_Empty		=	0,
		Elements			=
		{
			{	ShapeName	=	"HB_F14_EXT_AIM-7"}
		}-- end of Elements
	}
    -- actually LAU-92 adapter, the LAU-92 is internal to the adapter and also internal in the F-14 belly sparrow slots
	--ret.displayName =	_("LAU-92").." "..missilename
    ret.displayName =	missilename

	declare_loadout(ret)
end

f14_shoulder_aim_7("{SHOULDER AIM-7M}", "AIM-7M", copied_aim7m.wsTypeOfWeapon)
f14_belly_aim_7("{BELLY AIM-7M}", "AIM-7M", copied_aim7m.wsTypeOfWeapon, copied_aim7m.M)
f14_shoulder_aim_7("{SHOULDER AIM-7F}", "AIM-7F", copied_aim7f.wsTypeOfWeapon)
f14_belly_aim_7("{BELLY AIM-7F}", "AIM-7F", copied_aim7f.wsTypeOfWeapon, copied_aim7f.M)
f14_shoulder_aim_7("{SHOULDER AIM-7MH}", "AIM-7MH", copied_aim7mh.wsTypeOfWeapon)
f14_belly_aim_7("{BELLY AIM-7MH}", "AIM-7MH", copied_aim7mh.wsTypeOfWeapon, copied_aim7mh.M)

----- fuel tanks
local GALLON_TO_KG = 3.785 * 0.8
declare_loadout(
{
    category		= CAT_FUEL_TANKS,
    CLSID			= "{F14-300gal-empty}",
    attribute		=  {wsType_Air,wsType_Free_Fall,wsType_FuelTank,WSTYPE_PLACEHOLDER},
    Picture	=	"PTB.png",
    Weight_Empty	= 50,
    Weight			= 70, --20 eunusable or something
    Capacity = 300*GALLON_TO_KG,
    --attribute	=	{1,	3,	43,	12},
    shape_table_data =
    {
        {
            name	= "HB_F14_EXT_DROPTANK_EMPTY";
            file	= "HB_F14_EXT_DROPTANK";
            life	= 1;
            fire	= { 0, 1};
            username	= "Fuel tank 300 gal";
            index	= WSTYPE_PLACEHOLDER;
        },
    },
    Elements	=
    {
        [1]	=
        {
            Position	=	{0,	0,	0},
            ShapeName	=	"HB_F14_EXT_DROPTANK_EMPTY",
        },
    }, -- end of Elements
    displayName	=	_("Fuel tank 300 gal (empty)"),
    Cx_pil = 0.002197266,
})

declare_loadout(
{
    category		= CAT_FUEL_TANKS,
    CLSID			= "{F14-300gal}",
    attribute		=  {wsType_Air,wsType_Free_Fall,wsType_FuelTank,WSTYPE_PLACEHOLDER},
    Picture	=	"PTB.png",
    Weight_Empty	= 50,
    Weight			= 50 + 300 * GALLON_TO_KG,
    Capacity = 300*GALLON_TO_KG,
    --attribute	=	{1,	3,	43,	12},
    shape_table_data =
    {
        {
            name	= "HB_F14_EXT_DROPTANK";
            file	= "HB_F14_EXT_DROPTANK";
            life	= 1;
            fire	= { 0, 1};
            username	= "Fuel tank 300 gal";
            index	= WSTYPE_PLACEHOLDER;
        },
    },
    Elements	=
    {
        [1]	=
        {
            Position	=	{0,	0,	0},
            ShapeName	=	"HB_F14_EXT_DROPTANK",
        },
    }, -- end of Elements
    displayName	=	_("Fuel tank 300 gal"),
    Cx_pil = 0.002197266,
})


-- LANTIRN
declare_loadout(
{
    category		= CAT_PODS,
    CLSID			= "{F14-LANTIRN-TP}",
    attribute		= {wsType_Weapon, wsType_GContainer, wsType_Control_Cont, WSTYPE_PLACEHOLDER},
    --attribute		= {wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
    Picture	=	"LantirnTP.png",
    Weight			= 342,
    --[[shape_table_data =
    {
        {
            name	= "HB_F14_EXT_LANTIRN_PYLON";
            file	= "HB_F14_EXT_LANTIRN_PYLON";
            life	= 1;
            fire	= { 0, 1};
            username	= "HB_F14_EXT_LANTIRN_PYLON";
            index	= WSTYPE_PLACEHOLDER;
        },
        {
            name	= "HB_F14_EXT_LANTIRN";
            file	= "HB_F14_EXT_LANTIRN";
            life	= 1;
            fire	= { 0, 1};
            username	= "HB_F14_EXT_LANTIRN";
            index	= WSTYPE_PLACEHOLDER;
        },
    },--]]
    Elements	=
    {
        --{ ShapeName	=	"HB_F14_EXT_LANTIRN_PYLON",	IsAdapter  = true},
        --{ ShapeName	=	"HB_F14_EXT_LANTIRN", connector_name = "Lantirn_Pod_Connector"},
    }, -- end of Elements
    displayName	=	_("LANTIRN Targeting Pod"),
    Cx_pil = 0.002,
})

-- SPECIAL
--[[
-- failed experiment
declare_loadout(
{
    category		= CAT_SERVICE,
    CLSID			= "{F14-CLEAN-RAIL}",
    Picture			=	"Weaponx.png",
    PictureBlendColor = { r = 112/255,g = 140/255,b = 170/255,a = 1},
    Weight_Empty	=	0,
    Weight			=	0,
    Cx_pil			=	0,
    attribute		=	{0,0,0,0},
    displayName	=	_("Clean Phoenix Pallet"),
    Elements = { {} },
})
--]]

-- F14AAA-75:  TALD vehicles are loaded on improved Triple Ejector Racks (BRU-42)

-- CNU-188/A External Baggage Container is a modified fuel tank, hangs on BRU-32 (F14AAA-75 Fig 3-10)

-- BOMBS
--[[
Mk_81       = 30;  -- Mk-81
Mk_82       = 31;  -- Mk-82
Mk_83       = 32;  -- Mk-83
Mk_84       = 33;  -- Mk-84
GBU_10      = 36;  -- GBU-10
GBU_11      = 37;  -- GBU-11
GBU_12      = 38;  -- GBU-12
GBU_16      = 39;  -- GBU-16

" The (BRU-42) TER provides
vertical separation from the fuselage at stations 3
and 6 for LAU-10/A rocket launchers and at stations
4 and 5 for SUU-44A flare dispensers"

           station 1  2  3  4  5  6  7  8
           ------------------------------
rockets LAU10/A:   2     2        1     2
mk-81 (lo&hi):     2     4  3  3  4     2
mk-82 (lo&hi):     2     4  3  3  4     2
mk-83 (lo):        1     3  1  1  3     1
mk-84 (lo):              1  1  1  1

MAK-79 clamps directly on the sides of the rails can be used instead of TER to mount
multiple bombs on each rail. 2 clamps can be connected to each side of the rail (4 total),
in one of two positions each (8 positions total)
--]]

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 MK-84}",
    Picture	=	"mk84.png",
    displayName	=	_("Mk-84"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 894, -- see db_weapons_data.lua
    --wsTypeOfWeapon	=	{4,	5,	9,	33},
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, Mk_84},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.00056, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"MK-84",
            connector_name =  "WEP_BRU-34_MK84",
            --use_full_connector_position = true,
            DrawArgs	=
            {
                {19, 0.0}, -- fusing wire visible
                {56, 0.0}, -- fusing scheme
                {57, 1.0} -- gator skin
            }, -- end of DrawArgs
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 MK-83}",
    Picture	=	"mk83.png",
    displayName	=	_("Mk-83"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 447,  -- see db_weapons_data.lua
    --wsTypeOfWeapon	=	{4,	5,	9,	32},
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, Mk_83},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.00035, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"MK-83",
            connector_name =  "WEP_BRU-34_MK84",
            --use_full_connector_position = true,
            DrawArgs	=
            {
                {19, 0.0}, -- fusing wire visible
                {56, 0.0}, -- fusing scheme
                {57, 0.12} -- gator skin
            }, -- end of DrawArgs
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 MK-82}",
    Picture	=	"mk82.png",
    displayName	=	_("Mk-82"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 241,  -- see db_weapons_data.lua
    --wsTypeOfWeapon	=	{4,	5,	9,	31},
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, Mk_82},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.00025, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"MK-82",
            connector_name =  "WEP_BRU-34_MK82",
            --use_full_connector_position = true,
            DrawArgs	=
            {
                {19, 0.0}, -- fusing wire visible
                {56, 0.0}, -- fusing scheme
                {57, 0.12} -- gator skin
            }, -- end of DrawArgs
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 MK-82AIR}",
    Picture	=	"mk82AIR.png",
    displayName	=	_("Mk-82AIR"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 241,  -- see db_weapons_data.lua
    --wsTypeOfWeapon	=	{4,	5,	9,	75},
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, MK_82AIR},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.00025, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"Mk-82AIR",
            connector_name =  "WEP_BRU-34_MK82",
            --use_full_connector_position = true,
            DrawArgs	=
            {
                {19, 0.0}, -- fusing wire visible
                {56, 0.0}, -- fusing scheme
                {57, 0.12} -- gator skin
            }, -- end of DrawArgs
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 MK-82SE}",
    Picture	=	"mk82AIR.png",
    displayName	=	_("Mk-82 SnakeEye"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 241,  -- see db_weapons_data.lua
    --wsTypeOfWeapon	=	{4,	5,	9,	75},
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, MK_82SNAKEYE},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.00025, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"MK-82_Snakeye",
            connector_name =  "WEP_BRU-34_MK82",
            --use_full_connector_position = true,
            DrawArgs	=
            {
                {19, 0.0}, -- fusing wire visible
                {56, 0.0}, -- fusing scheme
                {57, 0.12} -- gator skin
            }, -- end of DrawArgs
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 MK-20}",
    Picture	=	"Mk20.png",
    displayName	=	_("Mk-20"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 222,  -- see db_weapons_data.lua
    --wsTypeOfWeapon	=	{4,	5,	9,	75},
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_Cluster, ROCKEYE},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.000413, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"ROCKEYE",
            connector_name =  "WEP_BRU-34_MK82",
            --use_full_connector_position = true,
            DrawArgs	=
            {
                {19, 0.0}, -- fusing wire visible
                {56, 0.0}, -- fusing scheme
                {57, 0.12} -- gator skin
            }, -- end of DrawArgs
        },
    }, -- end of Elements
})

--{ CLSID = "{CBU_99}"},


declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 GBU-10}",
    Picture	=	"GBU10.png",
    displayName	=	_("GBU-10"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 940,  -- 940kg from wikipedia, db_weapons_data.lua says 1162 though
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_Guided, GBU_10},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.000793, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"GBU-10",
            connector_name =  "WEP_BRU-34_MK84",
            --use_full_connector_position = true,
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 GBU-12}",
    Picture	=	"GBU12.png",
    displayName	=	_("GBU-12"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 275,  -- see db_weapons_data.lua
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_Guided, GBU_12},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.000569, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"GBU-12",
            connector_name =  "WEP_BRU-34_MK82",
            --use_full_connector_position = true,
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 GBU-16}",
    Picture	=	"GBU16.png",
    displayName	=	_("GBU-16"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 564,  -- see db_weapons_data.lua
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_Guided, GBU_16},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.000640, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"GBU-16",
            connector_name =  "WEP_BRU-34_MK84",
            --use_full_connector_position = true,
        },
    }, -- end of Elements
})

declare_loadout(
{
    category = CAT_BOMBS,
    CLSID	=	"{BRU-32 GBU-24}",
    Picture	=	"GBU27.png", -- TODO: need GBU24.png ?
    displayName	=	_("GBU-24"),
    Weight_Empty = 57.38,   -- 100lbs+26.5lbs
    Weight	= 57.38 + 1050,  -- 1050kg from wikipedia, 900 from db_weapons_data.lua though
    wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_Guided, GBU_24},
    attribute      = {wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
    Count = 1,
    Cx_pil = 0.00002,
    Cx_item = 0.000793, -- see bombs_data.lua

    Elements	=
    {
        { ShapeName	= "HB_F14_EXT_BRU34" ,IsAdapter  =   true  },  -- combination ADU-703 & BRU-32
        {
            ShapeName	=	"GBU-24",
            connector_name =  "WEP_BRU-34_MK84",
            --use_full_connector_position = true,
        },
    }, -- end of Elements
})

local mk81_bomb = {
    category = CAT_BOMBS,
    name = "Mk-81",
    payload_CLSID = "{90321C8E-7ED1-47D4-A160-E074D5ABD902}",
    mass = 118, -- db_weapons_data.lua
    wsType = {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, Mk_81},
    Cx = 0.00018, -- bombs_data.lua
    ShapeName = "MK-81",
    picture = "FAB100.png" -- mk81.png?
}

local mk82_bomb = {
    category = CAT_BOMBS,
    name = "Mk-82",
    payload_CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}",
    mass = 241, -- db_weapons_data.lua
    wsType = {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, Mk_82},
    Cx = 0.00025, -- bombs_data.lua
    ShapeName = "MK-82",
    picture = "mk82.png"
}

local mk83_bomb = {
    category = CAT_BOMBS,
    name = "Mk-83",
    payload_CLSID = "{7A44FF09-527C-4B7E-B42B-3F111CFE50FB}",
    mass = 447, -- db_weapons_data.lua
    wsType = {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, Mk_83},
    Cx = 0.00035, -- bombs_data.lua
    ShapeName = "MK-83",
    picture = "mk83.png"
}

local bdu33_bomb = {
    category = CAT_BOMBS,
    name = "BDU-33",
    payload_CLSID	=	"{BDU-33}",
    mass	=	11, -- db_weapons_data
    wsType	=	{wsType_Weapon,	wsType_Bomb,	wsType_Bomb_A,	BDU_33},
    Cx = 0.00025, -- bombs_data.lua
    ShapeName = "BDU-33",
    picture	=	"bdu-33.png"
}

local mk82air_bomb = {
    category = CAT_BOMBS,
    name = "Mk-82AIR",
    payload_CLSID	=	"{Mk82AIR}",
    mass	=	241, -- bombs_data.lua  (db_weapons_data.lua has 232...)
    wsType	=	{wsType_Weapon,	wsType_Bomb,	wsType_Bomb_A,	MK_82AIR},
    Cx = 0.00025, -- bombs_data.lua
    ShapeName	=	"Mk-82AIR",
    picture	=	"mk82AIR.png"
}

local mk82se_bomb = {
    category = CAT_BOMBS,
    name = "Mk-82 SnakeEye",
    payload_CLSID	=	"{Mk82SNAKEYE}",
    mass	=	241, -- bombs_data.lua  (db_weapons_data.lua has 232...)
    wsType	=	{wsType_Weapon,	wsType_Bomb,	wsType_Bomb_A,	MK_82SNAKEYE},
    Cx = 0.00025, -- bombs_data.lua
    ShapeName	=	"MK-82_Snakeye",
    picture	=	"mk82AIR.png"
}

local mk20_bomb = {
    category = CAT_BOMBS,
    name = "MK-20",
    payload_CLSID	=	"{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}",
    mass	=	222, -- bombs_data.lua
    wsType	=	{wsType_Weapon,	wsType_Bomb, wsType_Bomb_Cluster, ROCKEYE},
    Cx = 0.00002, -- bombs_data.lua
    ShapeName	=	"ROCKEYE",
    picture	=	"Mk20.png"
}


local adm_141a_tald =
{
	category		= CAT_MISSILES,
	name			= "ADM_141A",
    payload_CLSID = "{ADM_141A}",
	mass			= 180,
	wsType	= "weapons.missiles.ADM_141A",
	Cx			= 8.0/4096.0, --8 in tactical_decoys.lua
	picture			= "agm154.png"
}

phx_adapter_nested("{PHXBRU3242_2*MK81 RS}", bru_32_nested("{BRU3242_2*MK81 RS}", bru_42_3x_bomb("{BRU42_2*MK81 RS}", mk81_bomb, false, true, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK81 LS}", bru_32_nested("{BRU3242_2*MK81 LS}", bru_42_3x_bomb("{BRU42_2*MK81 LS}", mk81_bomb, true, false, true) ) )

phx_adapter_nested("{PHXBRU3242_2*MK82 RS}", bru_32_nested("{BRU3242_2*MK82 RS}", bru_42_3x_bomb("{BRU42_2*MK82 RS}", mk82_bomb, false, true, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK82 LS}", bru_32_nested("{BRU3242_2*MK82 LS}", bru_42_3x_bomb("{BRU42_2*MK82 LS}", mk82_bomb, true, false, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK82AIR RS}", bru_32_nested("{BRU3242_2*MK82AIR RS}", bru_42_3x_bomb("{BRU42_2*MK82AIR RS}", mk82air_bomb, false, true, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK82AIR LS}", bru_32_nested("{BRU3242_2*MK82AIR LS}", bru_42_3x_bomb("{BRU42_2*MK82AIR LS}", mk82air_bomb, true, false, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK82SE RS}", bru_32_nested("{BRU3242_2*MK82SE RS}", bru_42_3x_bomb("{BRU42_2*MK82SE RS}", mk82se_bomb, false, true, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK82SE LS}", bru_32_nested("{BRU3242_2*MK82SE LS}", bru_42_3x_bomb("{BRU42_2*MK82SE LS}", mk82se_bomb, true, false, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK20 RS}", bru_32_nested("{BRU3242_2*MK20 RS}", bru_42_3x_bomb("{BRU42_2*MK20 RS}", mk20_bomb, false, true, true) ) )
phx_adapter_nested("{PHXBRU3242_2*MK20 LS}", bru_32_nested("{BRU3242_2*MK20 LS}", bru_42_3x_bomb("{BRU42_2*MK20 LS}", mk20_bomb, true, false, true) ) )
--phx_adapter_nested("{PHXBRU3242_2*MK82 RS}", bru_32_nested("{BRU3242_2*MK82 RS}", bru_42_3x_weapon("{BRU42_2*MK82 RS}", mk82_bomb, false, true, true) ) )
--phx_adapter_nested("{PHXBRU3242_2*MK82 LS}", bru_32_nested("{BRU3242_2*MK82 LS}", bru_42_3x_weapon("{BRU42_2*MK82 LS}", mk82_bomb, true, false, true) ) )

phx_adapter_nested("{PHXBRU3242_MK83 RS}", bru_32_nested("{BRU3242_MK83 RS}", bru_42_3x_bomb("{BRU42_MK83 RS}", mk83_bomb, false, false, true) ) )
phx_adapter_nested("{PHXBRU3242_MK83 LS}", bru_32_nested("{BRU3242_MK83 LS}", bru_42_3x_bomb("{BRU42_MK83 LS}", mk83_bomb, false, false, true) ) )


bru_32_nested("{BRU3242_3*BDU33}", bru_42_3x_bomb("{BRU42_3*BDU33}", bdu33_bomb, true, true, true) )

phx_adapter_nested("{PHXBRU3242_BDU33}", bru_32_nested("{BRU3242_3*BDU33_N}", bru_42_3x_bomb("{BRU42_3*BDU33_N}", bdu33_bomb, true, true, true) ) )

mak79_4x_weapon("{MAK79_MK81 4}",mk81_bomb,false,true,true,true,true)
mak79_4x_weapon("{MAK79_MK81 3L}",mk81_bomb,false,true,true,true,false)
mak79_4x_weapon("{MAK79_MK81 3R}",mk81_bomb,false,true,true,false,true)
mak79_4x_weapon("{MAK79_MK82 4}",mk82_bomb,false,true,true,true,true)
mak79_4x_weapon("{MAK79_MK82 3L}",mk82_bomb,false,true,true,true,false)
mak79_4x_weapon("{MAK79_MK82 3R}",mk82_bomb,false,true,true,false,true)
mak79_4x_weapon("{MAK79_BDU33 4}",bdu33_bomb,false,true,true,true,true)
mak79_4x_weapon("{MAK79_BDU33 3L}",bdu33_bomb,false,true,true,true,false)
mak79_4x_weapon("{MAK79_BDU33 3R}",bdu33_bomb,false,true,true,false,true)
mak79_4x_weapon("{MAK79_MK82AIR 4}",mk82air_bomb,false,true,true,true,true)
mak79_4x_weapon("{MAK79_MK82AIR 3L}",mk82air_bomb,false,true,true,true,false)
mak79_4x_weapon("{MAK79_MK82AIR 3R}",mk82air_bomb,false,true,true,false,true)
mak79_4x_weapon("{MAK79_MK82SE 4}",mk82se_bomb,false,true,true,true,true)
mak79_4x_weapon("{MAK79_MK82SE 3L}",mk82se_bomb,false,true,true,true,false)
mak79_4x_weapon("{MAK79_MK82SE 3R}",mk82se_bomb,false,true,true,false,true)

mak79_4x_weapon("{MAK79_MK83 3L}",mk83_bomb,true,false,true,true,true)
mak79_4x_weapon("{MAK79_MK83 3R}",mk83_bomb,true,true,false,true,true)
mak79_4x_weapon("{MAK79_MK83 1R}",mk83_bomb,true,false,false,false,true)
mak79_4x_weapon("{MAK79_MK83 1L}",mk83_bomb,true,false,false,true,false)

mak79_4x_weapon("{MAK79_MK20 2L}",mk20_bomb,true,true,false,true,false)
mak79_4x_weapon("{MAK79_MK20 2R}",mk20_bomb,true,false,true,false,true)
mak79_4x_weapon("{MAK79_MK20 1R}",mk20_bomb,false,false,false,false,true)
mak79_4x_weapon("{MAK79_MK20 1L}",mk20_bomb,false,false,false,true,false)
bru_32_nested("{BRU3242_ADM141}", bru_42_3x_weapon("{BRU42_ADM141}", adm_141a_tald, false, false, true) )

-- ROCKETS

local lau10_zuni = {
    category = CAT_ROCKETS,
    name = "LAU-10 - 4 ZUNI MK 71",
    payload_CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}",
    mass = 440,
    --wsType = {4, 7, 33, 37},
    wsType = { wsType_Weapon, wsType_NURS, wsType_Rocket, Zuni_127 },
    Cx = 0.001708984375,
    picture = "LAU10.png"
}

bru_32_nested("{BRU3242_LAU10}", bru_42_3x_weapon("{BRU42_LAU10}", lau10_zuni, false, true, false) )
bru_32_nested("{BRU3242_2*LAU10 L}", bru_42_3x_weapon("{BRU42_2*LAU10 L}", lau10_zuni, true, false, true) )
bru_32_nested("{BRU3242_2*LAU10 R}", bru_42_3x_weapon("{BRU42_2*LAU10 R}", lau10_zuni, true, true, false) )

phx_adapter_nested("{PHXBRU3242_2*LAU10 RS}", bru_32_nested("{BRU3242_2*LAU10 RS}", bru_42_3x_weapon("{BRU42_2*LAU10 RS}", lau10_zuni, false, true, true) ) )
phx_adapter_nested("{PHXBRU3242_2*LAU10 LS}", bru_32_nested("{BRU3242_2*LAU10 LS}", bru_42_3x_weapon("{BRU42_2*LAU10 LS}", lau10_zuni, true, false, true) ) )

-- TODO: SUU-44 instead of SUU-25?  (seems only older F-14 docs refer to SUU-44 though, newer ones refer to SUU-25)
-- Differences: SUU-25 can launch 8 LUU-2B/B individually, SUU-44 always launches an entire tube at a time (2 LUU-2B/B at a time)
--              SUU-25 looks a bit different (more aerodynamic nosecone for instance)
-- http://www.tpub.com/aviord321/105.htm
local suu25_flare = {
    category = CAT_BOMBS,
    name = "SUU-25 * 8 LUU-2",
    payload_CLSID = "{CAE48299-A294-4bad-8EE6-89EFC5DCDF00}",
    mass = 130,
    wsType = { wsType_Weapon, wsType_Bomb, wsType_Bomb_Lighter, LUU_2B },
    Cx = 0.001,
    picture = "L005.png"
}

bru_32_nested("{BRU3242_SUU25}", bru_42_3x_weapon("{BRU42_SUU25}", suu25_flare, false, true, false) )
bru_32_nested("{BRU3242_2*SUU25 L}", bru_42_3x_weapon("{BRU42_2*SUU25 L}", suu25_flare, true, false, true) )
bru_32_nested("{BRU3242_2*SUU25 R}", bru_42_3x_weapon("{BRU42_2*SUU25 R}", suu25_flare, true, true, false) )

local luu2_flare = {
    category = CAT_BOMBS,
    name = "LUU-2",
    mass = 13.6, -- db_weapons_data.lua
    wsType = { wsType_Weapon, wsType_Bomb, wsType_Bomb_Lighter, LUU_2B },
    Cx = 0.0001, -- bombs_data.lua
    ShapeName = "luu-2",
    picture = "mk82.png" ---XXX replace with icon of luu-2
}

-- XXX these appear mounted on aircraft already shining (firefly!), so don't use for now
bru_32_nested("{BRU3242_2*LUU2 R}", bru_42_3x_bomb("{BRU42_2*LUU2 R}", luu2_flare, true, false, true) )
bru_32_nested("{BRU3242_2*LUU2 L}", bru_42_3x_bomb("{BRU42_2*LUU2 L}", luu2_flare, false, true, true) )