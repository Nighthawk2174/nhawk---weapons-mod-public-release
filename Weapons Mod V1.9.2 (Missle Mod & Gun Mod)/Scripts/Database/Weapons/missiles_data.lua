-- // тип головки самонаведения ГСН:
-- const int InfraredSeeker = 1;    // тепловая IR (infrared seeker)
-- const int ActiveRadar        = 2;    // активная-радиолокационная (active radar (AR) (+ИНС)) 
-- const int AntiRadar          = 3;    // антирадарная (пассивный радар +ИНС)
-- const int LaserHoming        = 4;    // лазерный подсвет (+ИНС)
-- const int Autopilot          = 5;    // автономная (ИНС+карта, GPS,TV,IIR...)
-- const int SemiActiveRadar    = 6;    // полуактивная-радиолокационная semi-active radar (SAR) -радиоподсвет
-- const int SemiAutoAT	    = 7;	// полуавтоматическое управление с платформы для ПТРК, летят на woPoint, координаты woPoint меняются платформой.

-- struct WEAPONS_API Rocket_Const // Ракетные постоянные и настройки для законов управления. 
            -- // Характеристики ракеты
    -- unsigned char Name_;     //  имя ракеты
    -- int           Escort_;   //  сопровождение: 0 - нет, 1 - ЛА пуска, 2 - другой ЛА, 3 - c земли
    -- int           Head_Type_;// Тип головки самонаведения (ГСН)(cм выше) 
	--				sigma = {x, y, z}, максимальная ошибка прицеливания в метрах, в координатах цели. x - продольная ось цели, y - вертиальная ось цели, z - поперечная ось цели
    -- float         M_;        //  полная масса в кг
    -- float         H_max_;    //  максимальная высота полета. 
    -- float         H_min_;    //  минимальная высота полета.  
    -- float         Diam_;     //  Диаметр корпуса в мм 
    -- int           Cx_pil;    //  Cx как подвески
    -- float         D_max_;    //  Maximum range at low altitude
    -- float         D_min_;    //  Minimum range at low altitude
    -- bool          Head_Form_;// false - полусферическая форма головы, 
                   -- // true   - оживальная (~коническая)
    -- float         Life_Time_;//  время жизни (таймер самоликвидации), сек
    -- double        Nr_max_;   // Максимальная перегрузка при разворотах
    -- float         v_min_;    // Минимальная скорость. 
    -- float    v_mid_;  // Средняя скорость
    -- float         Mach_max_; // Максимальное число Маха.
    -- float         t_b_;      // время включения двигателя
    -- float         t_acc_;    // время работы ускорителя
    -- float         t_marsh_;  // время работы в маршевом режиме
    -- float         Range_max_;// максимальная дальность пуска на максимальной высоте
    -- float         H_min_t_;  // минимальная высота цели над рельефом, м.     
    -- float         Fi_start_; // угол сопровождения и визирования при пуске  
    -- float         Fi_rak_;   // допустимый угол ракурса цели (rad)
    -- float         Fi_excort_;// угол сопровождения (визир.) цели ракетой.
    -- float          Fi_search_;// предельный угол свободного поиска
    -- float          OmViz_max_;// предельная скорость линии визирования
    -- float         Damage_;// повреждение, наносимое при прямом попадании
	-- /*   int           Engine_Type_; // тип двигателя: 1 - твердотопливный;
                             -- // 2 - Жидкостный Ракетный Двигатель(РД)(ЖРД); 
                             -- // 3 - Прямоточный Воздушный РД ; 
                             -- // 4 - ускоритель-1+ЖРД.
                             -- // 5 - турбореактивный
                             -- // 6 - турбореактивный + ускоритель 
    -- int           Stage_;    // количество ступеней.*/
    -- float         X_back_;   // координаты центра сопла в осях ракеты
    -- float         Y_back_; 
    -- float         Z_back_;
	-- float		 X_back_acc_; // координаты центра сопла ускорителя в осях ракеты
	-- float		 Y_back_acc_;
	-- float		 Z_back_acc_;
    -- float          Reflection;   // эффективная поверхность радиоотражения, квадратные метры

    -- // Kill distances - this distance is used to fire a fuze
    -- double        KillDistance;

    -- // These are warheads used to simulate explosions
    -- // Due to the architecture of blocksim we have to use two schemes - 
    -- // one for server object (which actually makes damage), and other for
    -- // client object (which does not make any damage)
	
	-- Мгновенный угол обзора ракет:
	-- ИК ГСН +- 1 градус
	-- РС ГНС +- 5 градусов

    
--Tail smoke color format {R, G, B, alpha}
--All values from 0 to 1
tail_solid = { 1, 1, 1, 1 };
tail_liquid = {0.9, 0.9, 0.9, 0.05 };

-- Pn coefficient example
-- PN_coeffs = {3, 						-- Number of entries
			-- 10000.0 ,1.0, 			-- Less 10 km Pn = 1
			-- 20000.0, 0.5}; 			-- Between 20 and 10 km, Pn smoothly changes from 0.5 to 1.
			-- 30000.0, 0.3}; 			-- Between 30 and 20 km, Pn smoothly changes from 0.3 to 0.5. Longer then 30 km Pn = 0.3.

-- ATG Missiles' flight speed is too low. Need correction.
local ATGMMissiles_VelocityAdaptationCoeff = 340/295*340/295; -- woMissile.cpp: l. 906,907.

--IR ECCM logic for values, note gen is just generic term will try to find better term to use latter
--Testing still ongoing to see if these numbers make sense, for example is the system linear or exponential... I don't know yet....
--First gen,  Uncooled, AM logic, 5-6, aim-9b, AA2A
--Second gen, Uncooled, AM logic, 4-5, AA8A 
--Second gen, Cooled, AM logic, 3-4, AIM-9D through H, AA-2C, R-23T, R-24T, R-40T 
--Second gen, Cooled, AM logic, (improved solid state electronics), 2-2.5, AIM-9J/K/P/P3/P4, R27T, R27ET, AA-8B 
--Thrid gen,  Cooled, FM logic, InSb detector, 1.0-1.5 ,AIM-9P5, AIM-9L, Sa-14, StingerA, MIM72C 
--Thrid gen,  Cooled, FM logic, InSb detector, Improved ECCM, 0.5-1.0, Aim-9M, SA-16
--Fourth gen, Cooled, Crossed Linear Array, 0.3-0.5, Mistral,
--Fourth gen, Cooled, Dual Band, 0.3-0.5, Sa18, Sa24, R73M
--Fifth gen,  Cooled, CdS seeker, Dual Band, Rossette scan tracker, 0.1-0.3 Stinger C&D, MIM72G,
--Fifth gen,  Cooled, Imaging seeker, 0-0.1, AIM-9x, IRST, ASRAAM, MICA-IR


-- Others, SA9 EO device (0.1) works in clear daytime only, SA13B Dual mode optical(0.1) during daytime and IR(.5) with flare rejection by postion/trajectory at night/backup day,
rockets = 
{
    {
        Name = R_550, -- R.550 Magic 2
		display_name = _('R.550 Magic 2'),
		name = "R_550",
        Escort = 0,
        Head_Type = 1,
		sigma = {3, 3, 3}, 
        M = 89.0,
        H_max = 28000.0,
        H_min = 1.0,
        Diam = 157.0,
        Cx_pil = 2,
        D_max = 4000.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 40,
        v_min = 140.0,
        v_mid = 400.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 1.88,
        t_marsh = 0.0,
        Range_max = 10000.0,
        H_min_t = 10.0,
        Fi_start = 0.96,
        Fi_rak = 3.14152,
        Fi_excort = 0.96,
        Fi_search = 0.09,
        OmViz_max = 0.52,
        warhead = warheads["R_550"],
        exhaust = tail_solid,
        X_back = -1.15,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0157,
        KillDistance = 5.0,
		--seeker sensivity params
		SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters. 
		ccm_k0 = 0.85,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				
		ModelData = {   58 ,  -- model params count
						0.35 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.05 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.18 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.15, -- Cx_k3 Cd0 at high mach (M>>1)
						1.0 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.0 , -- “polar dump factor"
						
						-- Lift (Cy) 
						0.9 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.9	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.29 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain	t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	1.88,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	12.58,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	29750.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 20.0, 	-- Selfdestruction time in sec.
						 20.0, 	-- lifetime in seconds
						 0, 	-- Minimum height in M
						 0.3, 	-- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  	-- Angle in radians of loft 
						 30.0,	-- продольное ускорения взведения взрывателя
						 0.0,	-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, 	-- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, 	-- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, 	-- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 11000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 4000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 5000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
    },
    {
        Name = MICA_T, --MICA-IR
		display_name = _('MICA-IR'),
		name = "MICA_T",
        Escort = 0,
        Head_Type = 1,
		sigma = {4, 4, 4},
        M = 110.0,
        H_max = 28000.0,
        H_min = 1.0,
        Diam = 160.0,
        Cx_pil = 2,
        D_max = 8000.0,
        D_min = 700.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 24,
        v_min = 140.0,
        v_mid = 450.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 4.0,
        Range_max = 40000.0,
        H_min_t = 1.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 0.09,
        OmViz_max = 0.52,
        warhead = warheads["MICA_T"],
        exhaust = tail_solid,
        X_back = -1.072,
        Y_back = -0.1,
        Z_back = 0.0,
        Reflection = 0.0157,
        KillDistance = 7.0,
		--seeker sensivity params
		SeekerSensivityDistance = 25000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		ccm_k0 = 0.01,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0249, -- площадь выходного сечения сопла
		
			ModelData = {   58 ,  -- model params count
						0.5 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.018 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.050 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.018, -- Cx_k3 Cd0 at high mach (M>>1)
						1.50 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.20 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.65 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.75	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						
						0.29 , -- alpha max
						10.0, --Additional g’s due to thrust vectoring/rocket motors 		
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	4.1,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	8.85,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	22241.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 65.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin
						 1.0e9, -- Lofting logic Rmin 
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},
		
		
    },
    {
        Name = MICA_R, --MICA-RF
		display_name = _('MICA-RF'),
		name = "MICA_R",
        Escort = 0,
        Head_Type = 2,
		sigma = {5.6, 5, 5.6},
        M = 110.0,
        H_max = 28000.0,
        H_min = 1.0,
        Diam = 160.0,
        Cx_pil = 2,
        D_max = 8000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 60.0,
        Nr_max = 24,
        v_min = 140.0,
        v_mid = 500.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 4.0,
        Range_max = 45000.0,
        H_min_t = 10.0,
        Fi_start = 1.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.52,
        warhead = warheads["MICA_R"],
        exhaust = tail_solid,
        X_back = -1.072,
        Y_back = -0.1,
        Z_back = 0.0,
        Reflection = 0.0157,
        KillDistance = 7.0,
		hoj = 1, 
		ccm_k0 = 0,
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0249, -- площадь выходного сечения сопла
		
			ModelData = {   58 ,  -- model params count
						0.45 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  						
						0.016 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.045 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.016, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.5 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.75 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.80	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.5236 , -- alpha max
						10.0, --Additional g’s due to thrust vectoring/rocket motors 																						  
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	4.1,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	8.85,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	22241.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 65.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},
		
    },
    {
        Name = Super_530D, --Super-530D
		display_name = _('Super_530D'),
		name = "Super_530D",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 5, 5.6},
        M = 270.0,
        H_max = 24400.0,
        H_min = 1.0,
        Diam = 263.0,
        Cx_pil = 3,
        D_max = 12000.0,
        D_min = 600.0,
        Head_Form = 1,
        Life_Time = 55.0,
        Nr_max = 25,
        v_min = 140.0,
        v_mid = 600.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 8.0,
        t_marsh = 0.0,
        Range_max = 40000.0,
        H_min_t = 20.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 0.87,
        Fi_search = 0.1,
        OmViz_max = 0.35,
        warhead = warheads["Super_530D"],
        exhaust = tail_solid,
        X_back = -1.202,
        Y_back = -0.0,
        Z_back = 0.0,
        Reflection = 0.0257,
        KillDistance = 10.0,
		ccm_k0 = 0.25,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0201, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.04 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.08 , -- Cx_k1 Peak Cd0 value 
						0.05 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.04, -- Cx_k3 Cd0 at high mach (M>>1)
						2.0 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.0 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.4 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.5	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.26 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	2.0,  		8.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	6.85,		5.375,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	38000.0,	15250.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 45.0, -- Selfdestruction time in sec.
						 45.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin
						 1.0e9, -- Lofting logic Rmin 
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 4800.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 2.2, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 30.0, --  коэф поправки к дальности от скорости носителя
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 32.0, -- Прогноз времени полета ракеты 
						  -- DLZ settings. 
						 35000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 8000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 12000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
	
					proximity_fuze = {
						ignore_inp_armed  = 1,
						radius		= 12,
						arm_delay	= 1.0,
									},
	
    },
    {
        Name = P_40T, --R-40T
		display_name = _('R-40T (AA-6 Acrid)'),	
		name = "P_40T",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 475.0,
        H_max = 28000.0,
        H_min = 20.0,
        Diam = 355.0,
        Cx_pil = 4,
        D_max = 7000.0,
        D_min = 700.0,
        Head_Form = 0,
        Life_Time = 65.0,
        Nr_max = 12,
        v_min = 140.0,
        v_mid = 600.0,
        Mach_max = 3.8,
        t_b = 0.0,
        t_acc = 8.0,
        t_marsh = 0.0,
        Range_max = 32000.0,
        H_min_t = 20.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 0.87,
        Fi_search = 0.09,
        OmViz_max = 0.35,
        warhead = warheads["P_40T"],
        exhaust = tail_solid,
        X_back = -2.276,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0257,
        KillDistance = 7.0,
		--seeker sensivity params
		SeekerSensivityDistance = 15000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				
		
		ccm_k0 = 3.5,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		
		ModelData = {   58 ,  -- model params count
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.15 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.14 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.017, -- Cx_k3 Cd0 at high mach (M>>1)
						2.0 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.4 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.9 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.38  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.23 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	6.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	25.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	50000.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 120.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 1e9, -- Lofting logic Rmin 
						 1e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},
    },
    {
        Name = P_40R, --R-40R
		display_name = _('R-40R (AA-6 Acrid)'),	
		name = "P_40R",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 5, 5.6},
        M = 475.0,
        H_max = 28000.0,
        H_min = 20.0,
        Diam = 355.0,
        Cx_pil = 4,
        D_max = 15000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 75.0,
        Nr_max = 12,
        v_min = 140.0,
        v_mid = 600.0,
        Mach_max = 3.8,
        t_b = 0.0,
        t_acc = 8.0,
        t_marsh = 0.0,
        Range_max = 40000.0,
        H_min_t = 20.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 0.87,
        Fi_search = 0.1,
        OmViz_max = 0.35,
        warhead = warheads["P_40R"],
        exhaust = tail_solid,
        X_back = -2.276,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0257,
        KillDistance = 7.0,

		ccm_k0 = 0.5,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)

		ModelData = {   58 ,  -- model params count
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.12 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.14 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.012, -- Cx_k3 Cd0 at high mach (M>>1)
						2.0 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.4 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.9 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.38  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.23 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	6.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	25.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	50000.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 120.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 1e9, -- Lofting logic Rmin 
						 1e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					}, 
    },
    {
        Name = P_24R, --R-24R
		display_name = _('R-24R (AA-7 Apex)'),
		name = "P_24R",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 5, 5.6},
        M = 215.0,
        H_max = 28000.0,
        H_min = 20.0,
        Diam = 200.0,
        Cx_pil = 3,
        D_max = 12000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 100.0,
        Nr_max = 18,
        v_min = 140.0,
        v_mid = 500.0,
        Mach_max = 3.0,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 35000.0,
        H_min_t = 20.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 0.87,
        Fi_search = 0.1,
        OmViz_max = 0.35,
        warhead = warheads["P_24R"],
        exhaust = tail_solid,
        X_back = -1.4,
        Y_back = -0.0,
        Z_back = 0.0,
        Reflection = 0.036,
        KillDistance = 10.0,
		ccm_k0 = 0.5,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		
		PN_coeffs = {2, 	-- Number of Entries	
			5000.0 ,1.0,		-- Less 5 km to target Pn = 1
			15000.0, 0.4};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
		
		-- The guidance error from the influence of the surface at low altitudes
			height_error_k = 110, -- если проекция скорости цели на линию визирования меньше этого значения, появляется ошибка
			height_error_max_vel = 150, -- пропорциональный коэффициент
			height_error_max_h = 450, -- максимальная высота, где появляется ошибка
		
		ModelData = {   58 ,  -- model params count
						
						0.32 ,   -- characteristic square (характеристическая площадь) 
						
						-- Drag (Сx)  
						0.025 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.084 , -- Cx_k1 Peak Cd0 value 							
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis  
						0.031 , -- Cx_k3 Cd0 at high mach (M>>1)                 
						1.0  , -- Cx_k4 steepness of the drag curve after the transonic wave crisis   
						1.2  , -- коэффициент отвала поляры
						
						-- Lift (Cy) 
						2.0 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.5	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						
						0.2618, -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					--	t_statr   t_b    t_boost   t_sustain t_inertial t_break   t_end
						-1.0,    -1.0 ,   6.0,  	 0.0, 		0.0,	 0.0,      1.0e9,           -- time interval
						 0.0,     0.0 ,   11.333, 	 0.0, 	 	0.0,     0.0,      0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек)
						 0.0,     0.0 ,   22000, 	 0.0 , 		0.0,     0.0,      0.0,           
						
						 50.0, -- Selfdestruction time in sec.
						 45.0, -- время работы энергосистемы
						 0, -- растояние до поверхности срабатывания радиовзрывателя, м
						 0.4, -- время задержки включения управленя, сек
						 1.0e9, -- 5000.0, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты 
						 1.0e9, -- 10000.0, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр) 
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					}, 
					
		proximity_fuze = {
						ignore_inp_armed  = 1,
						radius		= 10,
						arm_delay	= 2.0,
						},
    },
    {
        Name = P_24T, --R-24T
		display_name = _('R-24T (AA-7 Apex)'),
		name = "P_24T",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 215.0,
        H_max = 28000.0,
        H_min = 20.0,
        Diam = 200.0,
        Cx_pil = 3,
        D_max = 10000.0,
        D_min = 700.0,
        Head_Form = 0,
        Life_Time = 100.0,
        Nr_max = 18,
        v_min = 140.0,
        v_mid = 500.0,
        Mach_max = 3.0,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 25000.0,
        H_min_t = 20.0,
        Fi_start = 0.3, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 1.0472, --Gimbal Angle Limit
        Fi_search = 0.1396, --Field of View
        OmViz_max = 0.3142, --Gimbl Ang Rate Lim 18°/s 
        warhead = warheads["P_24T"],
        exhaust = tail_solid,
        X_back = -1.328,
        Y_back = -0.101,
        Z_back = 0.0,
        Reflection = 0.0352,
        KillDistance = 6.0,
		--seeker sensivity params
		SeekerSensivityDistance = 6000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.
		ccm_k0 = 3.5,
		
			PN_coeffs = {2, 		-- Number of Entries	
					5000.0 ,1.0,		-- Less 5 km to target Pn = 1
					15000.0, 0.4};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
		
		
		ModelData = {   58,  -- model params count
						
						0.32 ,   -- characteristic square (характеристическая площадь) 1.1
						
						-- Drag (Сx)  
						0.029 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.128 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis  .
						0.056 , -- Cx_k3 Cd0 at high mach (M>>1) 
						1.0  , -- Cx_k4 steepness of the drag curve after the transonic wave crisis   
						1.2  , -- коэффициент отвала поляры
	  
						-- Lift (Cy) 
						2.0 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.0	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						
						0.239 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					--	t_statr   t_b      t_boost  t_sustain   t_inertial   t_break  t_end
						-1.0,    -1.0 ,   6.0   ,   0.0,  0.0,		   0.0,      1.0e9,        -- time interval
						 0.0,     0.0 ,   11.33   ,  0.0,  0.0,         0.0,      0.0,         -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек)
						 0.0,     0.0 ,   24408.0,  0.0 , 0.0,         0.0,      0.0,          -- thrust 6000 by doc
						
						 50.0, -- Selfdestruction time in sec.
						 45.0, -- время работы энергосистемы
						 0, -- растояние до поверхности срабатывания радиовзрывателя, м
						 1.80, -- время задержки включения управленя, сек
						 1.0e9, -- 5000.0, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты 
						 1.0e9, -- 10000.0, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр) 
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					}, 
		proximity_fuze = {
				ignore_inp_armed  = 1,
				radius		= 10,
				arm_delay	= 2.0,
						},
    },
    {
        Name = P_60, --R-60 
		display_name = _('R-60M (AA-8B Aphid)'),
		name = "P_60",
        Escort = 0,
        Head_Type = 1,
		sigma = {3, 3, 3},
        M = 44.0,
        H_max = 20000.0,
        H_min = -1,
        Diam = 130.0,
        Cx_pil = 2.18,
        D_max = 4000.0,
        D_min = 200.0,
        Head_Form = 0,
        Life_Time = 20.0,
        Nr_max = 42,
        v_min = 140.0,
        v_mid = 400.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 12000.0,
        H_min_t = 1.0,
        Fi_start = 0.29671, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 0.34906585, --Gimbal Angle Limit
        Fi_search = 0.05235988, --Field of View
        OmViz_max = 0.61086, --Gimbl Ang Rate Lim 35°/s 
        warhead = warheads["P_60"],
        exhaust = tail_solid,
        X_back = -1.23,
        Y_back = -0.069,
        Z_back = 0.0,
        Reflection = 0.0157,
        KillDistance = 3.0, 
		--seeker sensivity params
		SeekerSensivityDistance = 10000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		ccm_k0 = 3.0,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.		
		
		
		nozzle_exit_area =	0.007238, -- площадь выходного сечения сопла
		
		
		ModelData = {   58 ,  -- model params count
						0.2 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.029 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.083 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.040, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.9 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.2967 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	4.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	2.6,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	6250.0,		0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 30.0, -- Selfdestruction time in sec.
						 25.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.10, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 5450.0,
						 0.627,
						 0.527,
						 0.75,
						 23,
						  -- DLZ settings. 
						 13000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 6000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 7000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},
					
					proximity_fuze = {
							ignore_inp_armed	= 1,
							radius		= 2.5,
							arm_delay	= 0.5,
									},		
},
	
    {
        Name = P_33E, --R-33
		display_name = _('R-33 (AA-9 Amos)'),
		name = "P_33E",
        Escort = 1,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 520.0,
        H_max = 28000.0,
        H_min = 10.0,
        Diam = 380.0,
        Cx_pil = 5,
        D_max = 40000.0,
        D_min = 6000.0,
        Head_Form = 1,
        Life_Time = 360.0,
        Nr_max = 12,
        v_min = 140.0,
        v_mid = 800.0,
        Mach_max = 4.5,
        t_b = 0.1,
        t_acc = 6.0,
        t_marsh = 25.0,
        Range_max = 120000.0,
        H_min_t = 25.0,
        Fi_start = 0.3,--Angle off boresight
        Fi_rak = 3.14152,--Allowable attack angles
        Fi_excort = 0.87,
        Fi_search = 0.1,--Lock on cone size in rad
        OmViz_max = 0.35,--Tracking rate 20*/s
        warhead = warheads["P_33E"],
        exhaust = tail_solid,
        X_back = -1.948,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.079,
        KillDistance = 12.0,
		loft = 1,
		hoj = 1,
		ccm_k0 = 0.01,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.007238, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.022 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.0367 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.013, -- Cx_k3 Cd0 at high mach (M>>1)
						1.4 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
	  
						-- Lift (Cy) 
						2.6 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.7	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
		
						0.2 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	5.0,  		15.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	20.0,		3.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	70000.0,	12000.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 120.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 2.5, -- Autopilot delay, sec
						 80000.0, -- Lofting logic Rmin 
						 80000.0, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.15,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					}, 
		
    },
	{
        Name = P_27P, --R-27R
		display_name = _('AA-10A'),
		name = "P_27P",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 5, 5.6},
        M = 253.0,
        H_max = 25000.0,
        H_min = 1.0,
        Diam = 230.0,
        Cx_pil = 2.21,
        D_max = 14000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 55.0,
        Nr_max = 24,
        v_min = 140.0,
        v_mid = 600.0,
        Mach_max = 4.5,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 0.0,
        Range_max = 35000.0,
        H_min_t = 20.0,
        Fi_start = 0.87,--Angle off boresight
        Fi_rak = 3.14152,--Allowable Target engagment angles
        Fi_excort = 0.9599,--Gimbal Angle Limit
        Fi_search = 0.1396,--Field of View
        OmViz_max = 0.4363,--Gimbl Ang Rate Lim 25°/s
        warhead = warheads["P_27P"],
        exhaust = tail_solid,
        X_back = -1.34,
        Y_back = -0.128,
        Z_back = 0.0,
        Reflection = 0.0479,
        KillDistance = 11.0,
		hoj = 1,
		ccm_k0 = 0.05,
				
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0266, -- площадь выходного сечения сопла
		
		
		PN_coeffs = {2, 		-- Number of Entries	
				5000.0 ,1.0,		-- Less 5 km to target Pn = 1
				15000.0, 0.4};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
		
		ModelData = {   58 ,  -- model params count
						
						0.90 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.023 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.084 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0175 , -- Cx_k3 Cd0 at high mach (M>>1)
						2.4  , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2  , -- коэффициент отвала поляры
						
						-- Lift (Cy) 
						1.6 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.2	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						
						0.2967 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
						--	t_statr   t_b      t_boost  t_sustain   t_inertial   t_break  t_end
						-1.0,    -1.0 ,   6.0   ,   0.0,  0.0,		   0.0,      1.0e9,           -- time interval
						 0.0,     0.0 ,   12.05   ,  0.0,  0.0,         0.0,      0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек), масса топлива 68 кг.
						 0.0,     0.0 ,   29639.0,  0.0 , 0.0,         0.0,      0.0,           -- thrust 6000 by doc
						
						 1.0e9, -- Selfdestruction time in sec.
						 55.0, -- время работы энергосистемы
						 0, -- растояние до поверхности срабатывания радиовзрывателя, м
						 0.4, -- время задержки включения управленя, сек https://forums.eagle.ru/showthread.php?t=266055
						 1.0e9, -- 5000.0, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты 
						 1.0e9, -- 10000.0, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр) 
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0,
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 5000.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 2.2, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 30.0, --  коэф поправки к дальности от скорости носителя
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 55.0, -- Прогноз времени полета ракеты 
						  -- DLZ settings. 
						 35000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 8000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
						 15000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
						 0.5,     -- Коэффициент уменьшения дальности при увеличения угла между векторм скорости носителя и линией визирования цели
						 0.3, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					}, 
    },
    {
        Name = P_27PE, --R-27ER
		display_name = _('AA-10C'),
		name = "P_27PE",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 5, 5.6},
        M = 350.0,
        H_max = 27000.0,
        H_min = 1.0,
        Diam = 265.0,
        Cx_pil = 2.21,
        D_max = 14000.0, -- Max range at sea level. Use AI only.
        D_min = 500.0, -- Min range. Use AI only.
        Head_Form = 1,
        Life_Time = 1.0e9, -- Not used
        Nr_max = 24, -- Not used
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0, -- Not used
        t_acc = 4.0, -- Not used
        t_marsh = 6.0, -- Not used
        Range_max = 60000.0, -- Max range at max altitude. Use AI only.
        H_min_t = 20.0,
        Fi_start = 0.87, -- Угол уелеуказания
        Fi_rak = 3.14152,--Allowable attack angles
        Fi_excort = 0.97,--Missile max fov angle
        Fi_search = 0.1, --Lock on cone size in rad
        OmViz_max = 0.35,--Tracking rate 20*
        warhead = warheads["P_27PE"],
        exhaust = tail_solid,
        X_back = -2.0,
        Y_back = -0.128,
        Z_back = 0.0,
        Reflection = 0.062,
        KillDistance = 11.0,
		hoj = 1,
		ccm_k0 = 0.05,
		
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0314, -- площадь выходного сечения сопла
		
		PN_coeffs = {2, 		-- Number of Entries	
				5000.0 ,1.0,		-- Less 5 km to target Pn = 1
				15000.0, 0.4};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
		
		
		ModelData = {   58 ,  -- model params count
																							
						
						0.90 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.026 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.085 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.025, -- Cx_k3 Cd0 at high mach (M>>1)
						2.4 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.6 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.2	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.2967 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	3.0,  		7.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	31.9,		6.32,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек), масса топлива 140 кг
						 0.0,		0.0,	73500.0,	14560.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 55.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.4, -- Autopilot delay, sec https://forums.eagle.ru/showthread.php?t=266055
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 8500.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 5.5, -- ЗРП. Крутизна зависимости в ППС
						 30.0, --  коэф поправки к дальности от скорости носителя
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 54.0, -- Прогноз времени полета ракеты 
						  -- DLZ settings. 
						 60000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 28500.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 26000.0, -- дальность ракурс 	180 град, Н=1000м, V=900км/ч, м
						 0.5, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 0.3, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.7, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					}, 
    },
    {
        Name = P_27T, --R-27T
		display_name = _('AA-10B'),
		name = "P_27T",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 245.0,
        H_max = 25000.0,
        H_min = 1.0,
        Diam = 230.0,
        Cx_pil = 2.23,
        D_max = 11000.0,
        D_min = 700.0,
        Head_Form = 0,
        Life_Time = 90.0,
        Nr_max = 24,
        v_min = 140.0,
        v_mid = 600.0,
        Mach_max = 3.2,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 0.0,
        Range_max = 25000.0,
        H_min_t = 1.0,
        Fi_start = 0.87,--Angle off boresight
        Fi_rak = 3.14152,--Allowable attack angles
        Fi_excort = 0.97,--Missile max fov angle
        Fi_search = 0.09,--Lock on cone size in rad
        OmViz_max = 0.35,--Tracking rate 20*
        warhead = warheads["P_27T"],
        exhaust = tail_solid,
        X_back = -1.34,
        Y_back = -0.128,
        Z_back = 0.0,
        Reflection = 0.0445,
        KillDistance = 11.0,
		--seeker sensivity params
		SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		ccm_k0 = 0.85,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.		
		

		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0266, -- площадь выходного сечения сопла
		
		PN_coeffs = {2, 		-- Number of Entries	
				5000.0 ,1.0,		-- Less 5 km to target Pn = 1
				15000.0, 0.4};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
		
		ModelData = {   58,  -- model params count
																							
						
						0.95 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.038 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.092 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.042 , -- Cx_k3 Cd0 at high mach (M>>1)
						2.0  , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2  , -- коэффициент отвала поляры
						
						-- Lift (Cy) 
						2.40 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.60	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.2967 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
												
						--	t_statr   t_b      t_boost  t_sustain   t_inertial   t_break  t_end
						-1.0,   	 -1.0 ,  	 6.0,   	0.0,  		0.0,	0.0,   1.0e9,           -- time interval
						 0.0,    	  0.0 ,  	 12.05,  	0.0, 		0.0,    0.0,   0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек), масса топлива 68 кг.
						 0.0,   	  0.0 ,  	 29639.0, 	0.0 , 		0.0,    0.0,   0.0,           -- thrust 6000 by doc
						
						 1.0e9, -- Selfdestruction time in sec.
						 55.0, -- время работы энергосистемы
						 0, -- абсалютеая высота самоликвидации, м
						 0.4, -- время задержки включения управленя, сек https://forums.eagle.ru/showthread.php?t=266055
						 1.0e9, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты 
						 1.0e9, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр) 
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 4800.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 2.2, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 30.0, --  коэф поправки к дальности от скорости носителя
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 32.0, -- Прогноз времени полета ракеты 
						  -- DLZ settings. 
						 27000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 11000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 12000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
						 }, 
    },
    {
        Name = P_27TE, --R-27ET
		display_name = _('AA-10D'),
		name = "P_27TE",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 343.0,
        H_max = 25000.0,
        H_min = 1.0,
        Diam = 260.0,
        Cx_pil = 2.5,
        D_max = 20000.0,
        D_min = 700.0,
        Head_Form = 0,
        Life_Time = 90.0,
        Nr_max = 24,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 6.0,
        Range_max = 54000.0,
        H_min_t = 1.0,
        Fi_start = 0.87,--Angle off boresight
        Fi_rak = 3.14152,--Allowable attack angles
        Fi_excort = 0.97,--Missile max fov angle
        Fi_search = 0.09,--Lock on cone size in rad
        OmViz_max = 0.35,--Tracking rate 20°/s
        warhead = warheads["P_27TE"],
        exhaust = tail_solid,
        X_back = -2.0,
        Y_back = -0.128,
        Z_back = 0.0,
        Reflection = 0.062,
        KillDistance = 11.0,
		--seeker sensivity params
		SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters.
		ccm_k0 = 0.85,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.
		
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0314, -- площадь выходного сечения сопла
		
		PN_coeffs = {2, 		-- Number of Entries	
				5000.0 ,1.0,		-- Less 5 km to target Pn = 1
				15000.0, 0.4};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
		
		
		ModelData = {   58 ,  -- model params count
																							
						
						0.95 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.04 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.095 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.042, -- Cx_k3 Cd0 at high mach (M>>1)
						1.9 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.6 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.2	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.2967 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	3.0,  		7.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	31.9,		6.32,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	73500.0,	14560.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 55.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.4, -- Autopilot delay, sec https://forums.eagle.ru/showthread.php?t=266055
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 8000.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 5.5, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 30.0, --  коэф поправки к дальности от скорости носителя
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 54.0, -- Прогноз времени полета ракеты 
						  -- DLZ settings. 
						 58000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м.
						 20000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м.
						 24000.0, -- дальность ракурс 	180 град (навстречу), Н=1000м, V=900км/ч, м.
						 0.6, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					}, 
    },
    
    {
        Name = P_73, --R-73
		display_name = _('R-73E (AA-11 Archer)'),
		name = "P_73",
        Escort = 0,
        Head_Type = 1,
		sigma = {3, 3, 3},
        M = 105.0,
        H_max = 20000.0,
        H_min = -1,
        Diam = 170.0,
        Cx_pil = 1.9,
        D_max = 4000.0,
        D_min = 300.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 45,
        v_min = 140.0,
        v_mid = 400.0,
        Mach_max = 2.8,
        t_b = 0.0,
        t_acc = 2.0,
        t_marsh = 3.0,
        Range_max = 12000.0,
        H_min_t = 1.0,
        Fi_start = 0.79,--Angle off boresight 45°
        Fi_rak = 3.14152, --Allowable Target engagment angles 
        Fi_excort = 1.31, --Gimbal Angle Limit 75°
        Fi_search = 0.09, --Field of View 5.15°
        OmViz_max = 1.05, --Gimbl Ang Rate Lim 60°/s 
        warhead = warheads["P_73"],
        exhaust = tail_solid,
        X_back = -1.1,
        Y_back = -0.09,
        Z_back = 0.0,
        Reflection = 0.0262,
        KillDistance = 5.0,
		--seeker sensivity params
		SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters.
		ccm_k0 = 0.85,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.02199015, -- площадь выходного сечения сопла
		
		PN_coeffs = {3, 				-- Number of Entries	
				3000.0 ,1.0,		-- Less 3 km to target Pn = 1
				5000.0, 0.5,		-- Between 5 and 3 km  to target, Pn smoothly changes from 0.5 to 1.0. 
				10000.0, 0.2};		-- Between 10 and 5 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 10 km Pn = 0.2.
		
		ModelData = {   58 ,  -- model params count
						
						0.4 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.05 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.12 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.062, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.0 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.9 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.78 , -- alpha max
						5.0, -- Extra G by trust vector
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	5.5,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	6.18,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	13847.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 23.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.35, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 8000.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 1.077, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 0.9,
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 28.0, -- расчет времени полета
						  -- DLZ settings. 
						 19000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 6000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 10000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
			
			proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 3.0,
								arm_delay	= 1.6,
													},
    },
   {
        Name = P_77, --R-77
		display_name = _('R-77 (AA-12 Adder)'),
		name = "P_77",
        Escort = 0,
        Head_Type = 2,
		sigma = {5.6, 5, 5},
        M = 175.0,
        H_max = 20000.0,
        H_min = 1.0,
        Diam = 190.0,
        Cx_pil = 3.44,
        D_max = 15000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 90.0,
        Nr_max = 40,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 6.0,
        Range_max = 50000.0,
        H_min_t = 3.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.52,
        warhead = warheads["P_77"],
        exhaust = tail_solid,
        X_back = -1.338,
        Y_back = -0.100,
        Z_back = -0.005,
        Reflection = 0.062,
        KillDistance = 15.0,
		hoj = 1,
		
		ccm_k0 = 0.05,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		supersonic_A_coef_skew = 0.16, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.02835, -- площадь выходного сечения сопла		--0.02199015

		PN_coeffs = {3, 				-- Number of Entries	
					15000.0 ,1.0,		-- Less 5 km to target Pn = 1
					25000.0, 0.5,		-- Between 10 and 5 km  to target, Pn smoothly changes from 0.5 to 1.0. 
					40000.0, 0.2};		-- Between 15 and 10 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.
								
		ModelData = {   58 ,  -- model params count
						0.45 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.024 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.13 , -- Cx_k1 Peak Cd0 value 
						0.058 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.020, -- Cx_k3 Cd0 at high mach (M>>1)
						1.5 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.0 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.75 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.29 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	5.1,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	11.1,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	26900,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 70.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 5000, -- Lofting logic Rmin 
						 8000, -- Lofting logic Rmin
						 0.1,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 17000.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 2.9, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 2.5,
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 52.0, -- расчет времени полета
						  -- DLZ settings. 
						 45100.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 18500.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
						 17700.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
    },
  
    {
       Name = AIM_7, --AIM-7M
		display_name = _('AIM-7M'),
		name = "AIM_7",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 5, 5.6},
        M = 231.1,
        H_max = 24400.0,
        H_min = 1.0,
        Diam = 203.0,
        Cx_pil = 2.21,
        D_max = 20000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 90.0,
        Nr_max = 25,
        v_min = 140.0,
        v_mid = 500.0,
        Mach_max = 3.2,
        t_b = 0.0,
        t_acc = 3.3,
        t_marsh = 11.0,
        Range_max = 50000.0,
        H_min_t = 15.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 0.1,
        OmViz_max = 0.35,
        warhead = warheads["AIM_7"],
        exhaust = {0.78, 0.78, 0.78, 0.3};
		X_back = -2.0,
		Y_back = -0.0,
		Z_back = 0.0, -- -0.1,
        Reflection = 0.0366,
        KillDistance = 12.0,
        ccm_k0 = 0.5,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		rad_correction = 0,
		  
		
		loft = 1,
		hoj = 1,
																														
								
		PN_coeffs = {4, 				-- Number of Entries
					8334,	1,			-- Under 5NMi PN rapidly climbs to 1 for target interception
					9260, 	.5,			-- From 8NMi to 5 NMi PN goes from .2 to .5
					15000,	.2,			-- From 18NMi until 8NMi PN goes from .01 to .2
					33336,  .01};		-- Above 18NMi PN is .01
					
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.007238, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						0.9 ,   -- characteristic square (характеристическая площадь)
				
						-- Drag (Сx)  
						0.023 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.048 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.012, -- Cx_k3 Cd0 at high mach (M>>1)
						1.3 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.5 , --“polar dump factor")
						
						-- Lift (Cy) 
						2.20, -- планка Cya на дозвуке ( M << 1)
						1.05, -- планка Cya на сверхзвуке ( M >> 1)
						1.20, -- крутизна спада(фронта) за волновым кризисом  

						0.1745, -- ~10 degrees Alfa_max  максимальный балансировачный угол, радианы
						0.00, --Additional g’s due to thrust vectoring/rocket motors 
						
					--	t_statr   t_b      t_boost  t_sustain   t_inertial   t_break  t_end
						-1.0,    -1.0 ,  	4.5  ,  	11.0,     0.0,		   	0.0,      1.0e9,           -- time interval
						0.0,     0.0 ,   	5.242 , 	3.546,    0.0,         0.0,      0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек)
						0.0,     0.0 ,   	25577.0,  	4528.0,   0.0,         0.0,      0.0,           -- thrust
					
						 1.0e9, -- Selfdestruction time in sec.
						 75.0, -- время работы энергосистемы
						 0.0, -- абсалютеая высота самоликвидации, м
						 1.5, -- время задержки включения управленя, сек
						 5000, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты 
						 6000, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр) 
						 0.1,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 6800.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 3.8, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 30.0, --  коэф поправки к дальности от скорости носителя
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 75.0, -- Прогноз времени полета ракеты 
						  -- DLZ settings. 
						 38000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 14500.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
						 24000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
						 0.2,     -- Коэффициент уменьшения дальности при увеличения угла между векторм скорости носителя и линией визирования цели
						 0.7, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 2.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					}, 
	},
    {
        Name = SeaSparrow, --RIM-7
		display_name = _('RIM-7'),
		name = "SeaSparrow",
        Escort = 1,
        Head_Type = 6,
		sigma = {15, 15, 15},
        M = 230.0,
        H_max = 24400.0,
        H_min = 1.0,
        Diam = 203.0,
        Cx_pil = 4,
        D_max = 27000.0,
        D_min = 1500.0,
        Head_Form = 1,
        Life_Time = 160.0,
        Nr_max = 32,
        v_min = 70.0,
        v_mid = 500.0,
        Mach_max = 3.0,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 3.0,
        Range_max = 27500.0,
        H_min_t = 20.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.6,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SeaSparrow"],
        exhaust = tail_solid,
        X_back = -1.96,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0366,
        KillDistance = 10.0,
		
		loft = 1,
		hoj = 1,
								
		PN_coeffs = {2, 				-- Number of Entries	
					9000.0 ,1.0,		-- Less 5 km to target Pn = 1
					15000.0, 0.4};		-- Between 15 and 5 km  to target, Pn smoothly changes from 0.4 to 1.0. Longer then 15 km Pn = 0.4.
			
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		
		
		ModelData = {   58 ,  -- model params count
						0.87 ,   -- characteristic square (характеристическая площадь)
				
						-- Drag (Сx)  
						0.0125 , -- планка Сx0 на дозвуке ( M << 1)
						0.052 , -- высота пика волнового кризиса
						0.010 , -- крутизна фронта на подходе к волновому кризису
						0.002 , -- планка Cx0 на сверхзвуке ( M >> 1)
						1.6  , -- крутизна спада за волновым кризисом 
						1.2  , -- коэффициент отвала поляры
						
						-- Lift (Cy) 
						2.20, -- планка Cya на дозвуке ( M << 1)
						1.05, -- планка Cya на сверхзвуке ( M >> 1)
						1.20, -- крутизна спада(фронта) за волновым кризисом  

						0.4, -- ~10 degrees Alfa_max  максимальный балансировачный угол, радианы
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
			
					--	t_statr   t_b      t_boost  t_sustain   t_inertial   t_break  t_end
						-1.0,    -1.0 ,  	4.5  ,  	11.0,     0.0,		   	0.0,      1.0e9,           -- time interval
						0.0,     0.0 ,   	5.242 , 	3.546,    0.0,         0.0,      0.0,           -- fuel flow rate in second kg/sec(секундный расход массы топлива кг/сек)
						0.0,     0.0 ,   	25577.0,  	4528.0,   0.0,         0.0,      0.0,           -- thrust
					
						 1.0e9, -- Selfdestruction time in sec.
						 60.0, -- время работы энергосистемы
						 0, -- абсалютеая высота самоликвидации, м
						 1.0, -- время задержки включения управленя, сек
						 10000.0, -- дальность до цели в момент пуска, выше которой выполняется маневр набора высоты 
						 15000.0, -- дальность до цели на трассе, менее которой начинается завершение маневра набора высоты (длжен быть больше чем предылущий параметр) 
						 0.17,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},											
    },
    {
        Name = AIM_9, --AIM-9M
		display_name = _('AIM-9M'),
		name = "AIM_9",
        Escort = 0,
        Head_Type = 1,
		sigma = {3, 3, 3},
        M = 85.36,
        H_max = 18000.0,
        H_min = -1,
        Diam = 127.0,
        Cx_pil = 2.58,
        D_max = 7000.0,
        D_min = 300.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 40,
        v_min = 140.0,
        v_mid = 350.0,
        Mach_max = 2.7,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 14000.0,
        H_min_t = 1.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 0.79,
        Fi_search = 0.09,
        OmViz_max = 0.61,
        warhead = warheads["AIM_9"],
        exhaust = { 0.7, 0.7, 0.7, 0.08 };
        X_back = -1.66,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0182,
        KillDistance = 7.0,
		--seeker sensivity params
		SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		ccm_k0 = 0.4,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				

		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0068, -- площадь выходного сечения сопла
  
		ModelData = {   58,  -- model params count
						0.35,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.049, -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.082, -- Cx_k1 Peak Cd0 value 
						0.010, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.001, -- Cx_k3 Cd0 at high mach (M>>1)
						0.550, -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						0.8, --“polar dump factor"
						
						-- Lift (Cy) 
						2.5, -- Cy_k0 Clmax at low mach ( M << 1)
						0.8, -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2, -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						


						0.13, -- alpha max
						0.00, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_accel		t_march		t_inertial		t_break		t_end			-- Stage
						-1.0,	   -1.0,	4.0,  		2.2,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	4.4189,		4.422,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	14106.423,	6108.891,		0.0,			0.0,		0.0,           -- thrust, newtons
						
						1.0e9, -- Selfdestruction time in sec.
						60.0, -- lifetime in seconds
						0, -- Minimum height in M
						0.3, -- Autopilot delay, sec
						1.0e9, -- Lofting logic Rmin 
						1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						0.0,  -- Angle in radians of loft 
						30.0, -- продольное ускорения взведения взрывателя
						0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						0.0,
						0.0,
						0.0,
						0.0,
						0.0,
						-- DLZ settings. 
						26800.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						11400.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						13300.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
			proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 4,
								arm_delay	= 1.0,
													},
			
    },
    {
        Name = AIM_9P, --AIM-9P
		display_name = _('AIM-9P'),
		name = "AIM_9P",
        Escort = 0,
        Head_Type = 1,
		sigma = {3, 3, 3},
        M = 85.5,
        H_max = 18000.0,
        H_min = -1,
        Diam = 127.0,
        Cx_pil = 2.58,
        D_max = 4000.0,
        D_min = 300.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 16,
        v_min = 140.0,
        v_mid = 350.0,
        Mach_max = 2.2,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 0.0,
        Range_max = 11000.0,
        H_min_t = 1.0,
        Fi_start = 0.3, --Angle off boresight
        Fi_rak = 3.14152,
        Fi_excort = 0.79, --Gimbal Angle Limit
        Fi_search = 0.09, --Field of View
        OmViz_max = 0.35, --Gimbl Ang Rate Lim 20°/s
        warhead = warheads["AIM_9P"],
        exhaust = tail_solid,
        X_back = -1.55,
        Y_back = -0.0,
        Z_back = 0.0,
        Reflection = 0.0182,
        KillDistance = 5.0,
		--seeker sensivity params
		ccm_k0 = 3.0,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		--seeker sensivity params
		SeekerSensivityDistance = 10000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				
		
		ModelData = {   58 ,  -- model params count
						0.35 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.04 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.08 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.05, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.5 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.4	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis   
						
						0.29 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	3.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	8.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	18800.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 30.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 9000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 4000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 4000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
    },
    {
        Name = AIM_9X, --AIM-9X
		display_name = _('AIM-9X'),
		name = "AIM_9X",
        Escort = 0,
        Head_Type = 1,
		sigma = {2, 2, 2},
        M = 85.5,
        H_max = 18000.0,
        H_min = -1,
        Diam = 127.0,
        Cx_pil = 2.58,
        D_max = 11000.0,
        D_min = 200.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 55,
        v_min = 140.0,
        v_mid = 350.0,
        Mach_max = 2.7,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 14000.0,
        H_min_t = 1.0,
        Fi_start = 1.57, --Angle off boresight
        Fi_rak = 3.14152,
        Fi_excort = 1.57, --Gimbal Angle Limit
        Fi_search = 0.09, --Field of View
        OmViz_max = 1.10, --Gimbl Ang Rate Lim 63°/s 
        warhead = warheads["AIM_9X"],
        exhaust = { 0.7, 0.7, 0.7, 0.08 };
		X_back = -1.92,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0182,
        KillDistance = 6.0,
		--seeker sensivity params
		SeekerSensivityDistance = 25000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
		ccm_k0 = 0.025,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				

		PN_coeffs = {3, 				-- Number of Entries	
					3000.0 ,1.0,		-- Less 3 km to target Pn = 1
					5000.0, 0.5,		-- Between 5 and 3 km  to target, Pn smoothly changes from 0.5 to 1.0. 
					10000.0, 0.2};		-- Between 10 and 5 km  to target, Pn smoothly changes from 0.2 to 0.5. Longer then 10 km Pn = 0.2.

  
				ModelData = {   58 ,  -- model params count
						
						0.35 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.04 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.08 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.05, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.0 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.9 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.7 , -- alpha max
						10.0, -- Extra G by thrust vector
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	5.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	5.44,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	12802.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 60.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.3, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 -- DLZ settings. 
						 11000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 5000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 5000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
		fuze_proximity = {
			ignore_inp_armed	= 1,
			arm_delay			= 0.01,
			radius				= 4,
				},			
	},
    {
        Name = AIM_54,
		display_name = _('AIM-54C-47'),
		name = "AIM_54",
        Escort = 0,
        Head_Type = 2,
        sigma = {5, 5, 5},
        M = 465.6,
        H_max = 20000.0,
        H_min = 1.0,
        Diam = 380.0,
        Cx_pil = 5,
        D_max = 14000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 160.0,
        Nr_max = 25,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 5.0,
        Range_max = 180000.0,
        H_min_t = 3.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.52,
        warhead = warheads["AIM_54"],
        exhaust = {0.8, 0.8, 0.8, 0.05 }; -- smoke "none"
        X_back = -1.66,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0329,
        KillDistance = 15.0,
        loft = 1,
        hoj = 1,

		ccm_k0 = 0.001,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
		nozzle_exit_area =	0.0567, -- ??????? ????????? ??????? ?????
		supersonic_A_coef_skew = 0.25, -- ?????? ?????? ???????????? ?????? ?????? ?? ??????????

        PN_coeffs = {4,                 -- Number of Entries
                    5000.0, 1.0,        -- Below 5km to target Pn = 1
                    10000.0, 0.5,       -- From there up to 10km to target, Pn smoothly changes 0.5
                    15000.0, 0.1,       -- From there up to 15km to target, Pn smoothly changes 0.1
                    40000.0, 0.01       -- From there up to 20km to target, Pn smoothly changes 0.01 (and remains 0.01 beyond that range)
                    };														  
					
        ModelData = {   58,  -- model params count
                        0.9,   -- characteristic square (характеристическаЯ площадь)

                        -- параметры зависимости ‘x
                        0.01, -- Cx_k0 планка ‘x0 на дозвуке ( M << 1)
                        0.031, -- Cx_k1 Peak Cd0 value 
                        0.03, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
                        0.028, -- Cx_k3 Cd0 at high mach (M>>1)
                        0.9, -- Cx_k4 крутизна спада за волновым кризисом
                        1.5, --“polar dump factor"

                       -- Lift (Cy) 
						2.6 , -- Cy_k0 планка ‘y0 на дозвуке ( M << 1)
						1.7	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 ???????? ?????(??????) ?? ???????? ????????  

                        0.1745, -- alpha max
                        0.0, --Additional g’s due to thrust vectoring/rocket motors 

                    -- Engine data. Time, fuel flow, thrust.
                    --  t_start     t_b     t_accel     t_march     t_inertial      t_break     t_end           -- Stage
                         0.27,       -1.0,   27.0,       0.0,        0.0,            0.0,        1.0e9,         -- time of stage, sec
                         0.0,       0.0,    6.048,      0.0,        0.0,            0.0,        0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
                         0.0,       0.0,    15723.1,    0.0,        0.0,            0.0,        0.0,           -- thrust, newtons

                         1.0e9, -- таймер самоликвидации, сек
                         160.0, -- времЯ работы энергосистемы, сек
                         0, -- абсолютнаЯ высота самоликвидации, м
                         1.5, -- Autopilot delay, sec
                         30000, -- Lofting logic Rmin 
                         40000, -- дальность до цели, при которой маневр "горка" завершаетсЯ и ракета переходит на чистую пропорциональную навигацию (должен быть больше или равен предыдущему параметру), м
                         0.20,  -- Angle in radians of loft 
                         50.0, -- продольное ускорениЯ взведениЯ взрывателЯ
                         0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарЯдом и тд
                         1.19, -- характристика системы ‘Ђ“-ђЂЉ…’Ђ,  коэф фильтра второго порЯдка K0
                         1.0, -- характристика системы ‘Ђ“-ђЂЉ…’Ђ,  коэф фильтра второго порЯдка K1
                         2.0, -- характристика системы ‘Ђ“-ђЂЉ…’Ђ,  полоса пропусканиЯ контура управлениЯ
                         25200.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
		   
                         3.92, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
                         3.2,
                         0.75, -- безразмерный коэф. эффективности ‘Ђ“ ракеты
                         70.0, -- расчет времени полета
                          -- DLZ. „анные длЯ рассчета дальностей пуска (индикациЯ на прицеле)
                         63000.0, -- дальность ракурс   180(навстречу) град,  Ќ=10000м, V=900км/ч, м
                         25000.0, -- дальность ракурс 0(в догон) град,  Ќ=10000м, V=900км/ч
                         22000.0, -- дальность ракурс    180(навстречу) град, Ќ=1000м, V=900км/ч
                         0.2,
                         0.6,
                         1.4,
						-3.0,
                        0.5,
                    },

					proximity_fuze = {
							ignore_inp_armed	= 1,	
							radius		= 18,
							arm_delay	= 2.5,
													},	
   },
 {
        Name = AIM_120, -- AIM-120B
		display_name = _('AIM-120B'),
		name = "AIM_120",
        Escort = 0,
        Head_Type = 2,
		sigma = {5, 5, 5},
        M = 156.0,
        H_max = 20000.0,
        H_min = 1.0,
        Diam = 169.0,
        Cx_pil = 2.5,
        D_max = 14000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 90.0,
        Nr_max = 40,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 5.0,
        Range_max = 65000.0,
        H_min_t = 3.0,
        Fi_start = 0.6109, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 1.0472, --Gimbal Angle Limit
        Fi_search = 0.1745, --Field of View
        OmViz_max = 0.6981, --Gimbl Ang Rate Lim 40°/s
        warhead = warheads["AIM_120"],
        exhaust = {0.8, 0.8, 0.8, 0.05 };
        X_back = -1.99,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0329,
        KillDistance = 15.0,
		loft = 1,
		hoj = 1,
	    ccm_k0 = 0.001,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probabily
  		loft_factor = 4.5,																												  
		
		supersonic_A_coef_skew = 0.1, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.02322576, -- площадь выходного сечения сопла
	
		PN_coeffs = {6, 				-- Number of Entries	
					1852.0, 1.1,		-- Less than 1nm to target PM goes to 1.1
					4630.0, 0.95,		-- At 2.5nm to target PM is .95
					9260.0, 0.85,		-- At 5.0nm to target PM is .50
					18520.0, 0.45,		-- At 10.0nm to target PM is .45
					27780.0, 0.10,		-- At 15.0nm to target PM is .10
					37040.0, 0.05,		-- At 20.0nm to target or higher PM is .05
					};			

	ModelData = {   58 ,  -- model params count
						.33 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.017 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.048 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.017, -- Cx_k3 Cd0 at high mach (M>>1)
						1.25 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.5 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.75 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.5, -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	4.0,  		4.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	7.69,		3.94,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	19600.0,	10045.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 80.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 25000,--40000, -- Lofting logic Rmin 
						 15000,--40000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.52356,--0.17,-- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0,
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 19700.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 3.69, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 3.2,
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 67.0, -- расчет времени полета
						  -- DLZ settings. 
						  -- DLZ settings. 
						 43000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 20000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
						 20000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						 0.5,
					},

					proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 12,
								arm_delay	= 1.6,
													},

    },
    {
        Name = AIM_120C, -- AIM-120C
		display_name = _('AIM-120C-5'),
		name = "AIM_120C-5",
        Escort = 0,
        Head_Type = 2,
		sigma = {5, 5, 5},
        M = 161.5,
        H_max = 26000.0,
        H_min = 1.0,
        Diam = 160.0,
        Cx_pil = 2.5,
        D_max = 64000.0,
        D_min = 700.0,
        Head_Form = 1,
        Life_Time = 90.0,
        Nr_max = 40,
        v_min = 140.0,
        v_mid = 700.0,
        Mach_max = 4.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 6.0,
        Range_max = 85000.0,
        H_min_t = 1.0,
        Fi_start = 0.780, 		--Angle off boresight
        Fi_rak = 3.14152, 		--Allowable Target engagment angles
        Fi_excort = 1.0472, 	--Gimbal Angle Limit
        Fi_search = 1.05, 		--Field of View
        OmViz_max = 0.6981, 	--Gimbl Ang Rate Lim 40°/s
        warhead = warheads["AIM_120C"],
        exhaust = {0.8, 0.8, 0.8, 0.05 };
        X_back = -1.98,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0329,
        KillDistance = 15.0,
		loft = 1,
		hoj = 1,
		ccm_k0 = 0.001,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		loft_factor = 4.5,																													  
  
		nozzle_exit_area =	0.02322576, -- площадь выходного сечения сопла
		supersonic_A_coef_skew = 0.1, -- наклон прямой коэффициента отвала поляры на сверхзвуке
																														 
		
		PN_coeffs = {6, 				-- Number of Entries	
					1852.0, 1.0,		-- Less than 1nm to target PM goes to 1.0
					4630.0, 0.95,		-- At 2.5nm to target PM is .95
					9260.0, 0.85,		-- At 5.0nm to target PM is .85
					18520.0, 0.45,		-- At 10.0nm to target PM is .45
					27780.0, 0.10,		-- At 15.0nm to target PM is .10
					37040.0, 0.05,		-- At 20.0nm to target or higher PM is .05
					};		
			
		ModelData = {   58 ,  -- model params count
						0.4 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.016 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.045 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.016, -- Cx_k3 Cd0 at high mach (M>>1)
						1.25 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.40 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.60 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.5 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	8.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	6.43,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	16703.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 105.0, -- Selfdestruction time in sec.
						 100.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 15000, --40000 -- Lofting logic Rmin 
						 40000, --40000 -- Range to target at which loft is complete (must be larger than previous value)  
						 0.52356,--0.17, -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 25200.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 3.92, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 3.2,
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 50.0, -- расчет времени полета
						  -- DLZ settings. 
						 63000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 25000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч
						 22000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч
						 0.2, 
						 0.6,
						 1.4,
						 -3.0,
						 0.5,
					},
					
					proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 12,
								arm_delay	= 1.5,
													},
    },
    {
        Name = X_22, --Kh-22
		display_name = _('Kh-22 (AS-4 Kitchen)'),
		name = "X_22",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 6800.0,
        H_max = 28000.0,
        H_min = -1,
        Diam = 1000.0,
        Cx_pil = 8,
        D_max = 140000.0,
        D_min = 40000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 6,
        v_min = 170.0,
        v_mid = 1000.0,
        Mach_max = 3.5,
        t_b = 0.0,
        t_acc = 10.0,
        t_marsh = 50.0,
        Range_max = 300000.0,
        H_min_t = 0.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_22"],
        exhaust = tail_liquid,
        X_back = -5.7,
        Y_back = -0.487,
        Z_back = 0.0,
        Reflection = 0.82, -- RCS, square meters
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = X_23, --Kh-23
		display_name = _('Kh-23 (AS-7 Kerry)'),
		name = "X_23",
        Escort = 1,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 288.0,
        H_max = 28000.0,
        H_min = -1,
        Diam = 275.0,
        Cx_pil = 4,
        D_max = 5000.0,
        D_min = 1000.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 2,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 5000.0,
        H_min_t = 0.0,
        Fi_start = 0.2,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_23"],
        exhaust = tail_solid,
        X_back = -0.86,
        Y_back = -0.14,
        Z_back = 0.0,
        Reflection = 0.0795,
        KillDistance = 5.0,
    },
    {
        Name = X_23L, --Kh-23L
		display_name = _('Kh-23L (AS-7 Kerry)'),
		name = "X_23L",
        Escort = 1,
        Head_Type = 4,
		sigma = {10, 10, 10},
        M = 288.0,
        H_max = 28000.0,
        H_min = -1,
        Diam = 275.0,
        Cx_pil = 4,
        D_max = 5000.0,
        D_min = 1000.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 2,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 5000.0,
        H_min_t = 0.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_23L"],
        exhaust = tail_solid,
        X_back = -0.86,
        Y_back = -0.14,
        Z_back = 0.0,
        Reflection = 0.0791,
        KillDistance = 0.0,
    },
    {
        Name = X_28, --Kh-28
		display_name = _('Kh-28 (AS-9 Kyle)'),
		name = "X_28",
		Escort = 0,
        Head_Type = 3,
		sigma = {15, 15, 15},
        M = 715.0,
        H_max = 10000.0,
        H_min = 200,
        Diam = 430.0,
        Cx_pil = 5,
        D_max = 90000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 500.0,
        Nr_max = 2,
        v_min = 170.0,
        v_mid = 250.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 500.0,
        Range_max = 105000.0,
        H_min_t = 0.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_28"],
        exhaust = tail_liquid,
        X_back = -2.440,
        Y_back = -0.210,
        Z_back = 0.0,
        Reflection = 0.15,
        KillDistance = 0.0,

		LaunchDistData =
		{
			15,		7,

					150,	200,	250,	300,	350,	400,	450,		

			200,	27000,	31000,	35000,	38000,	42000,	46000,	49000,	
			300,	27000,	31000,	35000,	40000,	43000,	47000,	50000,	
			500,	27000,	32000,	36000,	40000,	46000,	50000,	53000,	
			700,	27000,	32000,	37000,	42000,	48000,	53000,	50000,	
			1000,	29000,	34000,	39000,	46000,	53000,	56000,	58000,
			2000,	37000,	43000,	47000,	50000,	53000,	56000,	59000,	
			3000,	38000,	43000,	48000,	53000,	58000,	62000,	66000,	
			4000,	43000,	47000,	52000,	57000,	64000,	68000,	80000,	
			5000,	44000,	51000,	58000,	63000,	70000,	80000,	89000,	
			6000,	47000,	55000,	61000,	68000,	81000,	88000,	95000,	
			7000,	50000,	57000,	65000,	75000,	87000,	93000,	101000,	
			8000,	53000,	61000,	69000,	81000,	92000,	98000,	105000,	
			9000,	55000,	64000,	73000,	85000,	95000,	102000,	107000,	
			10000,	57000,	66000,	76000,	87000,	97000,	105000,	110000,	
			11000,	58000,	68000,	78000,	89000,	99000,	106000,	110000,	
		},		
		
		MinLaunchDistData =
		{
			14,		7,
			
					100,	150,	200,	250,	300,	350,	400,		

			200,	12000,	13000,	14000,	15000,	17000,	18000,	19000,	
			400,	13000,	13000,	14000,	16000,	17000,	18000,	20000,	
			600,	13000,	13000,	14000,	16000,	17000,	19000,	20000,	
			1000,	14000,	15000,	16000,	18000,	19000,	21000,	22000,	
			2000,	15000,	16000,	18000,	19000,	21000,	23000,	24000,	
			3000,	16000,	17000,	19000,	21000,	23000,	25000,	27000,	
			4000,	17000,	19000,	21000,	23000,	25000,	27000,	30000,	
			5000,	18000,	20000,	23000,	25000,	28000,	30000,	33000,	
			6000,	20000,	22000,	25000,	28000,	31000,	33000,	37000,	
			7000,	22000,	24000,	27000,	30000,	34000,	37000,	40000,	
			8000,	24000,	27000,	30000,	33000,	37000,	40000,	45000,	
			9000,	26000,	29000,	33000,	37000,	41000,	44000,	49000,	
			10000,	28000,	32000,	36000,	40000,	44000,	49000,	51000,	
			11000,	31000,	35000,	39000,	43000,	48000,	52000,	55000,				
		},

		manualWeaponFlag = 1,
    },
    {
        Name = X_25ML, --Kh-25ML
		display_name = _('Kh-25ML'),
		name = "X_25ML",
        Escort = 1,
        Head_Type = 4,
		sigma = {8, 8, 8},
        M = 316.0,
        H_max = 5000.0,
        H_min = -1,
        Diam = 275.0,
        Cx_pil = 8.57,
        D_max = 7000.0,
        D_min = 2500.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 4.0,
        Range_max = 8000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 0.7,
        OmViz_max = 0.12,
        warhead = warheads["X_25ML"],
        exhaust = {0.63, 0.67, 0.75, 0.2};
        X_back = -1.092,
        Y_back = -0.140,
        Z_back = 0.0,
        Reflection = 0.09,
        KillDistance = 0.0,
		
		
		ModelData = {   58 ,  -- model params count
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.04 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.08 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.05, -- Cx_k3 Cd0 at high mach (M>>1)
						2.5 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.5 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.2	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.2 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	2.0,  		7.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	11.05,		8.62,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	22540.0,	17640.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 25.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
	}, 
	}, 
    {
        Name = X_25MP, --Kh-25MP
		display_name = _('Kh-25MPU'),
		name = "X_25MP",
        Escort = 0,
        Head_Type = 3,
		sigma = {10, 10, 10},
        M = 310.0,
        H_max = 15000.0,
        H_min = -1,
        Diam = 275.0,
        Cx_pil = 8.57,
        D_max = 27000.0,
        D_min = 3000.0,
        Head_Form = 1,
        
		
		Life_Time = 300.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.65,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 9.0,
        Range_max = 60000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_25MP"],
        exhaust = {0.63, 0.67, 0.75, 0.2};
        X_back = -1.092,
        Y_back = -0.140,
        Z_back = 0.0,
        Reflection = 0.09,
        KillDistance = 5.0,
		LaunchDistData =
		{
			19,		9,
			



					50,		100,	150,	200,	250,	300,	350,	400,	450,	
			100,	14000,	15000,	17000,	19000,	20000,	22000,	24000,	26000,	29000,	
			300,	14000,	16000,	17000,	19000,	21000,	23000,	25000,	27000,	30000,	
			500,	15000,	17000,	18000,	20000,	22000,	24000,	26000,	28000,	30000,	
			800,	16000,	17000,	19000,	21000,	23000,	25000,	27000,	29000,	31000,	
			1000,	16000,	18000,	20000,	21000,	23000,	25000,	27000,	30000,	32000,




			2000,	18000,	20000,	22000,	23000,	25000,	27000,	30000,	32000,	34000,
			3000,	22000,	24000,	25000,	27000,	29000,	31000,	33000,	35000,	38000,
			4000,	23000,	25000,	27000,	29000,	31000,	33000,	35000,	38000,	40000,
			5000,	25000,	27000,	29000,	31000,	33000,	35000,	38000,	40000,	43000,
			6000,	26000,	28000,	31000,	33000,	35000,	37000,	40000,	42000,	45000,	
			7000,	28000,	30000,	32000,	35000,	37000,	40000,	42000,	45000,	48000,	
			8000,	29000,	32000,	34000,	37000,	39000,	42000,	45000,	48000,	51000,	
			9000,	31000,	33000,	36000,	38000,	41000,	44000,	47000,	50000,	53000,	


			10000,	32000,	35000,	38000,	40000,	43000,	46000,	50000,	53000,	56000,	
			11000,	34000,	36000,	39000,	42000,	45000,	49000,	52000,	55000,	58000,	
			12000,	35000,	38000,	41000,	44000,	47000,	51000,	54000,	58000,	61000,	
			13000,	35000,	39000,	43000,	46000,	49000,	53000,	56000,	60000,	63000,	
			14000,	36000,	40000,	44000,	48000,	49000,	55000,	58000,	62000,	65000,	
			15000,	38000,	41000,	46000,	50000,	53000,	57000,	60000,	64000,	68000,





		},
	
    },
    {
        Name = X_25MR, --Kh-25MR
		display_name = _('Kh-25MR'),
		name = "X_25MR",
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 6, 5.6},
        M = 300.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 275.0,
        Cx_pil = 8.57,
        D_max = 8000.0,
        D_min = 3000.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 2,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 4.0,
        Range_max = 20000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_25MR"],
        exhaust = {0.63, 0.67, 0.75, 0.2};
        X_back = -1.092,
        Y_back = -0.140,
        Z_back = 0.0,
        Reflection = 0.09,
        KillDistance = 0.0,
    },
    {
        Name = X_58, --Kh-58
		display_name = _('Kh-58U'),
		name = "X_58",
        Escort = 0,
        Head_Type = 3,
		sigma = {10, 10, 10},
        M = 640.0,
        H_max = 22000.0,
        H_min = -1,
        Diam = 380.0,
        Cx_pil = 19.64,
        D_max = 250000.0,
        D_min = 5000.0,
        Head_Form = 1,
       
	   
		Life_Time = 3000.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 3.6,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 15.0,
        Range_max = 245000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_58"],
        exhaust = tail_solid,
        X_back = -1.44,
        Y_back = -0.225,
        Z_back = 0.0,
        Reflection = 0.15,
        KillDistance = 5.0,
		
		LaunchDistData =
		{
			19,		9,



					100,	150,	200,	250,	300,	350,	400,	450,	500,	
					
			100,	26000,	29000,	31000,	35000,	38000,	41000,	44000,	48000,	52000,
			200,	27000,	29000,	32000,	35000,	39000,	41000,	45000,	48000,	53000,
			400,	27000,	30000,	33000,	36000,	39000,	42000,	46000,	50000,	53000,


			700,	29000,	31000,	34000,	37000,	40000,	44000,	48000,	51000,	55000,
			1000,	30000,	33000,	36000,	39000,	42000,	46000,	49000,	53000,	57000,
			2000,	36000,	38000,	40000,	43000,	47000,	52000,	54000,	58000,	64000,
			3000,	41000,	43000,	46000,	49000,	53000,	57000,	61000,	65000,	140000,
			4000,	49000,	50000,	53000,	55000,	60000,	65000,	129000,	135000,	144000,
			5000,	57000,	58000,	60000,	64000,	67000,	128000,	135000,	143000,	152000,
			6000,	62000,	66000,	70000,	73000,	128000,	134000,	141000,	151000,	162000,
			7000,	86000,	98000,	111000,	126000,	132000,	139000,	149000,	160000,	172000,
			8000,	93000,	106000,	121000,	130000,	136000,	146000,	157000,	168000,	179000,
			9000,	101000,	115000,	130000,	138000,	143000,	153000,	164000,	175000,	186000,
			10000,	110000,	125000,	137000,	148000,	154000,	160000,	171000,	182000,	225000,
			11000,	117000,	130000,	143000,	154000,	169000,	173000,	177000,	215000,	232000,
			12000,	120000,	135000,	146000,	160000,	174000,	190000,	207000,	224000,	239000,
			14000,	129000,	147000,	157000,	171000,	186000,	202000,	218000,	235000,	250000,	
			16000,	133000,	149000,	164000,	179000,	194000,	210000,	228000,	242000,	258000,
			18000,	133000,	149000,	166000,	182000,	197000,	213000,	231000,	247000,	266000,	
		},

		MinLaunchDistData =
		{
			25,		9,

					100,	150,	200,	250,	300,	350,	400,	450,	500,	

			200,	7000,	7000,	8000,	9000,	10000,	10000,	11000,	12000,	13000,	
			300,	6000,	7000,	8000,	9000,	10000,	10000,	11000,	12000,	13000,	
			400,	6000,	7000,	8000,	9000,	9000,	10000,	11000,	12000,	13000,	
			1000,	6000,	7000,	8000,	8000,	9000,	10000,	11000,	12000,	13000,	
			2000,	5000,	5000,	5000,	5000,	6000,	6000,	11000,	12000,	13000,	
			3000,	5000,	6000,	6000,	7000,	7000,	8000,	8000,	8000,	9000,	
			4000,	6000,	7000,	7000,	8000,	8000,	9000,	9000,	10000,	10000,	
			5000,	7000,	8000,	8000,	9000,	10000,	10000,	11000,	11000,	12000,	
			6000,	8000,	9000,	9000,	10000,	11000,	12000,	12000,	13000,	13000,	
			7000,	9000,	10000,	11000,	11000,	12000,	13000,	14000,	14000,	15000,	
			8000,	10000,	11000,	12000,	13000,	13000,	14000,	15000,	16000,	17000,	
			9000,	11000,	12000,	13000,	14000,	15000,	16000,	17000,	17000,	18000,	
			10000,	12000,	13000,	14000,	15000,	16000,	17000,	18000,	19000,	20000,	
			11000,	13000,	14000,	16000,	17000,	18000,	19000,	20000,	21000,	22000,	
			12000,	14000,	16000,	17000,	18000,	19000,	20000,	22000,	23000,	24000,	
			13000,	16000,	17000,	18000,	20000,	21000,	22000,	23000,	25000,	26000,	
			14000,	17000,	19000,	20000,	21000,	23000,	24000,	25000,	27000,	28000,	
			15000,	18000,	20000,	22000,	23000,	24000,	26000,	27000,	29000,	30000,	
			16000,	20000,	22000,	23000,	25000,	26000,	28000,	29000,	31000,	32000,	
			17000,	21000,	23000,	25000,	27000,	28000,	30000,	31000,	33000,	35000,	
			18000,	23000,	25000,	27000,	28000,	30000,	32000,	33000,	35000,	37000,	
			19000,	24000,	26000,	28000,	30000,	32000,	34000,	36000,	37000,	39000,	
			20000,	26000,	28000,	30000,	32000,	34000,	36000,	38000,	40000,	42000,	
			21000,	27000,	30000,	32000,	34000,	36000,	38000,	40000,	42000,	44000,	
			22000,	29000,	31000,	34000,	36000,	38000,	40000,	42000,	44000,	46000,	
		},
    },
    {
        Name = X_59M, --Kh-59M
		display_name = _('Kh-59M (AS-13 Kingbolt)'),
		name = "X_59M",		
        Escort = 0,
        Head_Type = 5,
		sigma = {10, 10, 10},
        M = 850.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 380.0,
        Cx_pil = 19.64,
        D_max = 115000.0,
        D_min = 10000.0,
        Head_Form = 0,
        Life_Time = 1800.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 250.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 500.0,
        Range_max = 80000.0,
        H_min_t = 0.0,
        Fi_start = 0.37,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_59M"],
        -- No exhaust (turbo)
		exhaust1 = tail_solid,
        X_back = -1.6,
        Y_back = -0.7,
        Z_back = 0.0,
		-- accelerator exhaust coords
		X_back_acc = -3.0,
        Y_back_acc = -0.212,
        Z_back_acc = 0.0,
        Reflection = 0.1914,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = X_29L, --Kh-29L
		display_name = _('Kh-29L'),			
		name = "X_29L",
        Escort = 1,
        Head_Type = 4,
		sigma = {8, 8, 8},
        M = 657.0,
        H_max = 5000.0,
        H_min = 200.0,
        Diam = 380.0,
        Cx_pil = 16.37,
        D_max = 8000.0,
        D_min = 3000.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 10, -- из БП Су-24
        v_min = 170.0,
        v_mid = 220.0,
        Mach_max = 1.8,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 0.0,
        Range_max = 13000.0,
        H_min_t = 0.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 1.05,
        OmViz_max = 0.12,
        warhead = warheads["X_29L"],
        exhaust = {0.63, 0.71, 0.78, 0.5},
        X_back = -1.446,
        Y_back = -0.196,
        Z_back = 0.0,
        Reflection = 0.175,
        KillDistance = 0.0,
		
		LaunchDistData =
		{
			13,		13,
			
					50,		75,		100,	125,	150,	175,	200,	225,	250,	275,	300,	325,	500,
			200,	6500,	7000,	7750,	8250,	8750,	9250,	9750,	10250,	10750,	11000,	11500,	12000,	12000,	
			300,	6500,	7250,	7750,	8250,	9000,	9500,	9750,	10250,	10750,	11250,	11500,	12000,	12000,	
			400,	6500,	7250,	7750,	8500,	9000,	9500,	10000,	10500,	10750,	11250,	11750,	12000,	12000,
			500,	6750,	7250,	8000,	8500,	9000,	9500,	10000,	10500,	11000,	11250,	11750,	12000,	12000,
			1000,	7000,	7750,	8250,	8750,	9250,	9750,	10250,	10750,	11250,	11500,	12000,	12000,	12000,
			1500,	7500,	8000,	8500,	9000,	9750,	10000,	10500,	11000,	11500,	12000,	12000,	12000,	12000,
			2000,	7750,	8250,	8750,	9250,	9750,	10250,	10750,	11250,	11750,	12000,	12000,	12000,	12000,
			2500,	7750,	8500,	9000,	9500,	10000,	10500,	11000,	11500,	12000,	12000,	12000,	12000,	12000,
			3000,	8250,	8750,	9250,	9750,	10250,	10750,	11250,	11750,	12000,	12000,	12000,	12000,	12000,
			3500,	8500,	9000,	9500,	10000,	10500,	11000,	11250,	11750,	12000,	12000,	12000,	12000,	12000,
			4000,	8500,	9000,	9500,	10000,	10750,	11000,	11500,	12000,	12000,	12000,	12000,	12000,	12000,
			4500,	8500,	9000,	9500,	10000,	10750,	11250,	11750,	12000,	12000,	12000,	12000,	12000,	12000,
			5000,	8500,	9000,	9750,	10250,	10750,	11250,	11750,	12000,	12000,	12000,	12000,	12000,	12000,
		},
    },
    {
        Name = X_29T, --Kh-29T
		display_name = _('Kh-29T'),
		name = "X_29T",
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 670.0,
        H_max = 5000.0,
        H_min = 200.0,
        Diam = 380.0,
        Cx_pil = 16.37,
        D_max = 10000.0,
        D_min = 3000.0,
        Head_Form = 0,
        Life_Time = 120.0,
        Nr_max = 10, -- Из БП Су-24
        v_min = 170.0,
        v_mid = 250.0,
        Mach_max = 1.4,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 0.0,
        Range_max = 15000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_29T"],
        exhaust = {0.63, 0.71, 0.78, 0.5},
        X_back = -1.446,
        Y_back = -0.202,
        Z_back = 0.0,
        Reflection = 0.1729,
        KillDistance = 0.0,
		
		LaunchDistData =
		{
			13,		13,
			
					50,		75,		100,	125,	150,	175,	200,	225,	250,	275,	300,	325,	500,
			200,	6500,	7000,	7750,	8250,	8750,	9250,	9750,	10250,	10750,	11000,	11500,	12000,	12000,	
			300,	6500,	7250,	7750,	8250,	9000,	9500,	9750,	10250,	10750,	11250,	11500,	12000,	12000,	
			400,	6500,	7250,	7750,	8500,	9000,	9500,	10000,	10500,	10750,	11250,	11750,	12000,	12000,
			500,	6750,	7250,	8000,	8500,	9000,	9500,	10000,	10500,	11000,	11250,	11750,	12000,	12000,
			1000,	7000,	7750,	8250,	8750,	9250,	9750,	10250,	10750,	11250,	11500,	12000,	12000,	12000,
			1500,	7500,	8000,	8500,	9000,	9750,	10000,	10500,	11000,	11500,	12000,	12000,	12000,	12000,
			2000,	7750,	8250,	8750,	9250,	9750,	10250,	10750,	11250,	11750,	12000,	12000,	12000,	12000,
			2500,	7750,	8500,	9000,	9500,	10000,	10500,	11000,	11500,	12000,	12000,	12000,	12000,	12000,
			3000,	8250,	8750,	9250,	9750,	10250,	10750,	11250,	11750,	12000,	12000,	12000,	12000,	12000,
			3500,	8500,	9000,	9500,	10000,	10500,	11000,	11250,	11750,	12000,	12000,	12000,	12000,	12000,
			4000,	8500,	9000,	9500,	10000,	10750,	11000,	11500,	12000,	12000,	12000,	12000,	12000,	12000,
			4500,	8500,	9000,	9500,	10000,	10750,	11250,	11750,	12000,	12000,	12000,	12000,	12000,	12000,
			5000,	8500,	9000,	9750,	10250,	10750,	11250,	11750,	12000,	12000,	12000,	12000,	12000,	12000,
		},
    },
    {
        Name = X_29TE, --Kh-29TE
		display_name = _('Kh-29TE'),
		name = "X_29TE",
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 690.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 380.0,
        Cx_pil = 16.37,
        D_max = 20000.0,
        D_min = 1000.0,
        Head_Form = 0,
        Life_Time = 1800.0,
        Nr_max = 2,
        v_min = 170.0,
        v_mid = 250.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 16.0,
        t_marsh = 150.0,
        Range_max = 30000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_29TE"],
        exhaust = {0.63, 0.71, 0.78, 0.5},
        X_back = -1.446,
        Y_back = -0.202,
        Z_back = 0.0,
        Reflection = 0.1729,
        KillDistance = 0.0,
    },
    {
        Name = X_55, --Kh-55
		display_name = _('Kh-55 (AS-15 Kent)'),	
		name = "X_55",
        Escort = 0,
        Head_Type = 5,
		sigma = {20, 20, 20},
        M = 1250.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 514.0,
        Cx_pil = 19.9,
        D_max = 600000.0,
        D_min = 9000.0,
        Head_Form = 0,
        Life_Time = 9200.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 230.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 9200.0,
        Range_max = 600000.0,
        H_min_t = 0.0,
        Fi_start = 0.25,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_55"],
        -- No exhaust (turbo)
        X_back = -2.356,
        Y_back = -0.618,
        Z_back = 0.0,
        Reflection = 0.1656,
        KillDistance = 0.0,
    },
    {
        Name = X_65, --Kh-65
		display_name = _('Kh-65 (AS-15 Kent)'),	
		name = "X_65",
        Escort = 0,
        Head_Type = 5,
		sigma = {20, 20, 20},
        M = 1400.0,
        H_max = 28000.0,
        H_min = -1,
        Diam = 514.0,
        Cx_pil = 19.9,
        D_max = 2500000.0,
        D_min = 9000.0,
        Head_Form = 0,
        Life_Time = 12000.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 230.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 12000.0,
        Range_max = 2500000.0,
        H_min_t = 0.0,
        Fi_start = 0.25,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_65"],
        -- No exhaust (turbo)
        X_back = -2.356,
        Y_back = -0.618,
        Z_back = 0.0,
        Reflection = 0.1656,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    
    {
        Name = X_31P, --Kh-31P
		display_name = _('Kh-31P (AS-17 Krypton)'),	
		name = "X_31P",
        Escort = 0,
        Head_Type = 3,
		sigma = {10, 10, 10},
        M = 600.0,
        H_max = 15000.0,
        H_min = -1,
        Diam = 360.0,
        Cx_pil = 15.6,
        D_max = 80000.0,
        D_min = 15000.0,
        Head_Form = 1,
        Life_Time = 1000.0,
		Nr_max = 12,
        v_min = 180.0,
        v_mid = 400.0,
        Mach_max = 2.54,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 300.0,
        Range_max = 110000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_31P"],
        exhaust1 = tail_solid,
        X_back = -1.341,
        Y_back = -0.178,
        Z_back = 0.0,
        Reflection = 0.3,
        KillDistance = 5.0,
		
		LaunchDistData =
		{
			18,		6,
			
					180,	250,	300,	350,	400,	450,
				
			100,	27000,	31000,	35000,	39000,	42000,	46000,	
			400,	28000,	33000,	36000,	40000,	44000,	49000,	
			700,	30000,	34000,	38000,	42000,	46000,	51000,	
			1000,	31000,	36000,	39000,	44000,	49000,	53000,	
			2000,	37000,	41000,	46000,	52000,	56000,	66000,	
			3000,	43000,	51000,	56000,	61000,	66000,	73000,	
			4000,	48000,	55000,	62000,	67000,	74000,	80000,	
			5000,	52000,	61000,	67000,	73000,	80000,	88000,	
			6000,	56000,	66000,	73000,	79000,	87000,	95000,	
			7000,	60000,	70000,	78000,	85000,	93000,	102000,	
			8000,	65000,	75000,	83000,	91000,	99000,	108000,	
			9000,	68000,	79000,	88000,	98000,	105000,	114000,	
			10000,	72000,	83000,	92000,	101000,	112000,	119000,	
			11000,	76000,	87000,	98000,	105000,	115000,	124000,	
			12000,	79000,	91000,	99000,	108000,	119000,	129000,	
			13000,	81000,	93000,	103000,	112000,	121000,	134000,	
			14000,	84000,	96000,	106000,	117000,	125000,	134000,	
			15000,	86000,	98000,	107000,	117000,	127000,	135000,	
		},

		MinLaunchDistData =
		{
			14,		6,
			
					200,	250,	300,	350,	400,	450,	
					
			100,	15000,	15000,	15000,	15000,	16000,	17000,	
			500,	15000,	15000,	15000,	15000,	17000,	18000,	
			1000,	15000,	15000,	15000,	16000,	17000,	18000,	
			2000,	15000,	15000,	15000,	17000,	18000,	19000,	
			4000,	15000,	15000,	15000,	15000,	20000,	20000,	
			5000,	15000,	15000,	15000,	15000,	15000,	15000,	
			8000,	15000,	15000,	15000,	15000,	15000,	15000,	
			9000,	15000,	15000,	15000,	15000,	16000,	17000,	
			10000,	15000,	15000,	15000,	16000,	17000,	18000,	
			11000,	15000,	15000,	16000,	18000,	19000,	20000,	
			12000,	16000,	17000,	18000,	19000,	20000,	22000,	
			13000,	17000,	18000,	20000,	21000,	22000,	23000,	
			14000,	18000,	20000,	21000,	23000,	24000,	25000,	
			15000,	20000,	21000,	23000,	24000,	26000,	27000,	
		},
    },
    {
        Name = X_31A, --Kh-31A
		display_name = _('Kh-31A (AS-17 Krypton)'),	
		name = "X_31A",
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 600.0,
        H_max = 15000.0,
        H_min = -1,
        Diam = 360.0,
        Cx_pil = 15.6,
        D_max = 50000.0,
        D_min = 7000.0,
        Head_Form = 1,
        Life_Time = 150.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 2.54,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 150.0,
        Range_max = 70000.0,
        H_min_t = 0.0,
        Fi_start = 0.25,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_31A"],
        exhaust1 = tail_solid,
        X_back = -1.341,
        Y_back = -0.178,
        Z_back = 0.0,
        Reflection = 0.3,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = X_35, --Kh-35
		display_name = _('Kh-31A (AS-20 Kayak )'),
		name = "X_35",
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 480.0,
        H_max = 30000.0,
        H_min = -1,
        Diam = 420.0,
        Cx_pil = 17.2,
        D_max = 130000.0,
        D_min = 4000.0,
        Head_Form = 0,
        Life_Time = 1800.0,
        Nr_max = 20,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 0.92,
        t_b = 0.0,
        t_acc = 0.0,
        t_marsh = 600.0,
        Range_max = 130000.0,
        H_min_t = 0.0,
        Fi_start = 0.25,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_35"],
        exhaust1 = tail_solid,
        X_back = -1.636,
        Y_back = -0.196,
        Z_back = 0.0,
        Reflection = 0.191,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = X_41, --Kh-41
		display_name = _('Kh-41 Moskit'),
		name = "X_41",
        Escort = 0,
        Head_Type = 5,
		sigma = {10, 10, 10},
        M = 4500.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 760.0,
        Cx_pil = 7,
        D_max = 100000.0,
        D_min = 5000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 850.0,
        Mach_max = 2.8,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 250.0,
        Range_max = 250000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["X_41"],
        exhaust1 = tail_solid,
        X_back = -6.2,
        Y_back = -0.376,
        Z_back = 0.0,
        Reflection = 0.41,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = Vikhr, --9A4172 Vikhr
		display_name = _('9A4172 Vikhr'),
		name = "Vikhr",
        Escort = 1,
        Head_Type = 4,
		sigma = {3, 3, 3},
        M = 43.0,
        H_max = 5000.0,
        H_min = 5.0,
        Diam = 130.0,
        Cx_pil = 1,
        D_max = 7000.0,
        D_min = 800.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 461.0,
        Mach_max = 2.2,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 5.0,
        Range_max = 11000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 0.8,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["Vikhr"],
        exhaust = {0.63, 0.67, 0.75, 0.1};
        X_back = 0.382,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0121,
        KillDistance = 0.0,
    },
    {
        Name = Vikhr_M, --9A4172M Vikhr-M
		display_name = _('9A4172M Vikhr-M'),
		name = "Vikhr_M",
        Escort = 1,
        Head_Type = 4,
		sigma = {3, 3, 3},
        M = 45.0,
        H_max = 5000.0,
        H_min = 5.0,
        Diam = 130.0,
        Cx_pil = 1,
        D_max = 7000.0,
        D_min = 800.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 461.0,
        Mach_max = 2.2,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 5.0,
        Range_max = 11000.0,
        H_min_t = 0.0,
        Fi_start = 0.0175,
        Fi_rak = 3.14152,
        Fi_excort = 0.8,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["Vikhr_M"],
        exhaust = {0.63, 0.67, 0.75, 0.1};
        X_back = 0.208,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.015,
        KillDistance = 0.0,
    },
    {
        Name = AT_6, --9M114 Shturm
		display_name = _('9M114 (AT-6 Spiral)'),
		name = "AT_6",
        Escort = 1,
        Head_Type = 6,
		sigma = {4, 4, 4},
        M = 40.0,
        H_max = 8000.0,
        H_min = -1,
        Diam = 126.0,
        Cx_pil = 2,
        D_max = 5000.0,
        D_min = 800.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 1.2,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 1.8,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 5000.0,
        H_min_t = 0.0,
        Fi_start = 0.1,
        Fi_rak = 3.14152,
        Fi_excort = 0.4,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AT_6"],
        exhaust = {0.63, 0.67, 0.75, 0.1};
        X_back = -0.922,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0495,
        KillDistance = 0.0,
    },
	--[[{
        Name = Ataka_T, --9M120 Ataka-T
        Escort = 1,
        Head_Type = 6,
		sigma = {5.6, 0, 5.6},
        M = 49.5,
        H_max = 4000.0,
        H_min = -1,
        Diam = 130.0,
        Cx_pil = 2,
        D_max = 6000.0,
        D_min = 100.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 1.2,
        v_min = 170.0,
        v_mid = 375.0,
        Mach_max = 1.8,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 5000.0,
        H_min_t = 0.0,
        Fi_start = 0.1,
        Fi_rak = 3.14152,
        Fi_excort = 0.4,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AT_6"],
        exhaust = {0.63, 0.67, 0.75, 0.3 };
        X_back = -0.922,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0495,
        KillDistance = 0.0,
    },]]
    {
        Name = AGM_114, --AGM-114L
		display_name = _('AGM-114L Hellfire'),
		name = "AGM_114",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 48.25,
        H_max = 28000.0,
        H_min = -1,
        Diam = 178.0,
        Cx_pil = 2,
        D_max = 8000.0,
        D_min = 500.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 2,
        v_min = 170.0,
        v_mid = 200.0,
        Mach_max = 1.6,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 8000.0,
        H_min_t = 0.0,
        Fi_start = 1.0,
        Fi_rak = 3.14152,
        Fi_excort = 0.8,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_114"],
        exhaust = {0.9, 0.9, 0.85, 0.1};
        X_back = -0.813,
        Y_back = -0.089,
        Z_back = 0.0,
        Reflection = 0.015,
        KillDistance = 0.0,
    },
    {
        Name = AGM_114K, --AGM-114K
		display_name = _('AGM-114K Hellfire'),
		name = "AGM_114K",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 45.7,
        H_max = 28000.0,
        H_min = -1,
        Diam = 178.0,
        Cx_pil = 2,
        D_max = 9000.0,
        D_min = 500.0,
        Head_Form = 1,
        Life_Time = 35.0,
        Nr_max = 2,
        v_min = 150.0,
        v_mid = 200.0,
        Mach_max = 1.6,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 9000.0,
        H_min_t = 0.0,
        Fi_start = 1.0,
        Fi_rak = 3.14152,
        Fi_excort = 0.8,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_114K"],
        exhaust = {0.9, 0.9, 0.85, 0.1};
        X_back = -0.813,
        Y_back = -0.089,
        Z_back = 0.0,
        Reflection = 0.015,
        KillDistance = 0.0,
    },
    {
        Name = AGM_45, --AGM-45
		display_name = _('AGM-45 Shrike'),
		name = "AGM_45",
        Escort = 0,
        Head_Type = 3,
		sigma = {10, 10, 10},
        M = 177.0,
        H_max = 24400.0,
        H_min = -1,
        Diam = 203.0,
        Cx_pil = 4,
        D_max = 12000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 300.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 15.0,
        Range_max = 40000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_45"],
        exhaust = tail_solid,
        X_back = -1.065,
        Y_back = -0.101,
        Z_back = 0.0,
        Reflection = 0.05,
        KillDistance = 5.0,

		LaunchDistData =
		{
			16,		6,

					100,	150,	200,	250,	300,	350,	
			100,	20000,	22000,	24000,	26000,	28000,	31000,	
			200,	21000,	22000,	24000,	26000,	28000,	31000,	
			400,	21000,	23000,	25000,	27000,	29000,	31000,	
			700,	21000,	23000,	25000,	27000,	29000,	32000,	
			1000,	22000,	23000,	26000,	28000,	30000,	32000,	
			2000,	23000,	25000,	27000,	29000,	32000,	34000,	
			3000,	24000,	26000,	28000,	31000,	33000,	36000,	
			4000,	26000,	28000,	30000,	32000,	35000,	38000,	
			5000,	27000,	29000,	31000,	34000,	37000,	40000,	
			6000,	28000,	30000,	33000,	36000,	39000,	42000,	
			7000,	29000,	31000,	34000,	37000,	40000,	44000,	
			8000,	30000,	33000,	36000,	39000,	42000,	46000,	
			9000,	32000,	34000,	37000,	40000,	44000,	47000,				
			10000,	33000,	36000,	39000,	42000,	46000,	49000,	
			11000,	34000,	37000,	40000,	44000,	47000,	51000,	
			12000,	36000,	38000,	42000,	45000,	49000,	52000,
		},


   },
    {
        Name = AGM_65K, --AGM-65K
		display_name = _('AGM-65K'),
		name = "AGM_65K",
        Escort = 0,
        Head_Type = 5,
		sigma = {2, 2, 2},
        M = 360.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 1.05, --Gimbal Angle Limit
        Fi_search = 99.9, --Field of View
        OmViz_max = 0.2973, --Gimbl Ang Rate Lim 60°/s 
        warhead = warheads["AGM_65K"],
        exhaust = {0.75, 0.75, 0.75, 0.1},
        X_back = -0.9,
        Y_back = -0.15,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 5.0,

		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.01904512, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						0.8 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.1046 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.176 , -- Cx_k1 Peak Cd0 value 
						0.109, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0105, -- Cx_k3 Cd0 at high mach (M>>1)
						0.09 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.868 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.868	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						0.5  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.209 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	0.6,  		4.07,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	3.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	44558.0,	9675.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 105.0, -- Selfdestruction time in sec.
						 105.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.6, -- Autopilot delay, sec
						 4630, -- Lofting logic Rmin 
						 12000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.05,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
		}, 
			PN_autopilot = {
		K			= 0.02,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.28,
	},
   },
    {
        Name = AGM_65D, --AGM-65D
		display_name = _('AGM-65D'),
		name = "AGM_65D",
        Escort = 0,
        Head_Type = 5,
		sigma = {2, 2, 2},
        M = 218.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9, --Gimbl Ang Rate Lim °/s 
        warhead = warheads["AGM_65D"],
        exhaust = {0.75, 0.75, 0.75, 0.1},
        X_back = -0.9,
        Y_back = -0.15,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.01904512, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						0.8 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.1046 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.176 , -- Cx_k1 Peak Cd0 value 
						0.109, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0105, -- Cx_k3 Cd0 at high mach (M>>1)
						0.09 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.868 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.868	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						0.5  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.209 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	0.6,  		4.07,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	3.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	44558.0,	9675.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 105.0, -- Selfdestruction time in sec.
						 105.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.6, -- Autopilot delay, sec
						 1e9, -- Lofting logic Rmin 
						 1e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
		},  
			PN_autopilot = {
		K			= 0.02,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.0,
							},
    },
    {
        Name = AGM_65H, --AGM-65H
		display_name = _('AGM-65H'),
		name = "AGM_65H",
        Escort = 0,
        Head_Type = 5,
		sigma = {2, 2, 2},
        M = 208.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_65H"],
        exhaust = {0.75, 0.75, 0.75, 0.1},
        X_back = -0.9,
        Y_back = -0.15,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
			
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.01904512, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						0.8 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.1046 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.176 , -- Cx_k1 Peak Cd0 value 
						0.109, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0105, -- Cx_k3 Cd0 at high mach (M>>1)
						0.09 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.868 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.868	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						0.5  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.209 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	0.6,  		4.07,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	3.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	44558.0,	9675.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 105.0, -- Selfdestruction time in sec.
						 105.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.6, -- Autopilot delay, sec
						 4630, -- Lofting logic Rmin 
						 12000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.05,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
		},  
			PN_autopilot = {
		K			= 0.02,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.28,
							},
    },
    {
        Name = AGM_65G, --AGM-65G
		display_name = _('AGM-65G'),
		name = "AGM_65G",
        Escort = 0,
        Head_Type = 5,
		sigma = {2, 2, 2},
        M = 301.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_65G"],
        exhaust = {0.75, 0.75, 0.75, 0.1},
        X_back = -0.9,
        Y_back = -0.15,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 5.0,
 	
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.01904512, -- площадь выходного сечения сопла
		
		ModelData = {   58 ,  -- model params count
						0.8 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.1046 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.176 , -- Cx_k1 Peak Cd0 value 
						0.109, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0105, -- Cx_k3 Cd0 at high mach (M>>1)
						0.09 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.868 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.868	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						0.5  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.209 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	0.6,  		4.07,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	3.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	44558.0,	9675.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 105.0, -- Selfdestruction time in sec.
						 105.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.6, -- Autopilot delay, sec
						 4630, -- Lofting logic Rmin 
						 12000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.05,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0,
						 1.0,
						 1.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					}, 
	},
    {
        Name = TGM_65G, --TGM-65G
		display_name = _('TGM-65G Maverick'),
		name = "TGM_65G",
        Escort = 0,
        Head_Type = 5,
		sigma = {5, 5, 5},
        M = 301.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["TGM_65G"],
        exhaust = tail_solid,
        X_back = -0.879,
        Y_back = -0.155,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
    },
    {
        Name = TGM_65D,  --TGM-65D
		display_name = _('TGM-65D Maverick'),
		name = "TGM_65D",
        Escort = 0,
        Head_Type = 5,
		sigma = {5, 5, 5},
        M = 218.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["TGM_65D"],
        exhaust = tail_solid,
        X_back = -0.879,
        Y_back = -0.155,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
    },
    {
        Name = CATM_65K,  --CATM-65K
		display_name = _('CATM-65K Maverick'),
		name = "CATM_65K",
        Escort = 0,
        Head_Type = 5,
		sigma = {5, 5, 5},
        M = 360.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["CATM_65K"],
        exhaust = tail_solid,
        X_back = -0.879,
        Y_back = -0.155,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
    },
    {
        Name = TGM_65H, --TGM-65H
		display_name = _('TGM-65H Maverick'),
		name = "TGM_65H",
        Escort = 0,
        Head_Type = 5,
		sigma = {5, 5, 5},
        M = 208.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 12964.0,
        D_min = 500.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 24076.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["TGM_65H"],
        exhaust = tail_solid,
        X_back = -0.879,
        Y_back = -0.155,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
    },
    {
        Name = AGM_65E, --AGM-65E
		display_name = _('AGM-65E'),
		name = "AGM_65E",
        Escort = 1,
        Head_Type = 4,
		sigma = {2, 2, 2},
        M = 286.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 305.0,
        Cx_pil = 4,
        D_max = 18520.0,
        D_min = 1000.0,
        Head_Form = 0,
        Life_Time = 110,
        Nr_max = 16,
        v_min = 50.0,
        v_mid = 290.0,
        Mach_max = 1.5,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 0.0,
        Range_max = 11112.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_65E"],
        exhaust = {0.75, 0.75, 0.75, 0.1},
        X_back = -0.879,
        Y_back = -0.155,
        Z_back = 0.0,
        Reflection = 0.063,
        KillDistance = 0.0,
		
		LaunchDistData =
		{
			4,		5,
		--	H	--M 0.3		0.5		0.7		0.9		1.2		for a = 330m/s
					100,	165,	230,	300,	400,
			50,		8400,	11000,	12000,	13000,	13000,
			2000,	14000,	15500,	17000,	18000,	18000,
			5000,	22000,	23000,	23500,	24500,	26800,
			10000,	25600,	26800,	28000,	28700,	29800,
		},
		
		MinLaunchDistData = 
		{
			4,		5,
		--	H	--M 0.3		0.5		0.7		0.9		1.2		for a = 330m/s
					100,	165,	230,	300,	400,
			50,		2000,	2300,	2500,	2700,	3000,
			2000,	1000,	1200,	1400,	1600,	3000,
			5000,	3000,	3000,	3000,	3000,	4000,
			10000,	6000,	6000,	6000,	6000,	6000,
	
    },	
	},
    {
        Name = AGM_84A, --AGM-84A
		display_name = _('AGM-84A Harpoon'),
		name = "AGM_84A",
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 555.0,
        H_max = 6000.0,
        H_min = 50,
        Diam = 343.0,
        Cx_pil = 6,
        D_max = 120000.0,
        D_min = 5000.0,
        Head_Form = 1,
        Life_Time = 1850.0,
        Nr_max = 18,
        v_min = 170.0,
        v_mid = 272.0,
        Mach_max = 1.0,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 1800.0,
        Range_max = 120000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_84A"],
        -- No exhaust (turbo)
        X_back = -1.581,
        Y_back = -0.173,
        Z_back = 0.0,
        Reflection = 0.121,
        KillDistance = 0.0,
		
		LaunchDistData =
		{
			12,		7,
				
					100,	125,	150,	175,	200,	250,	300,
			100,	0,		90000,	96000,	99000,	102000,	106000,	110000,	
			200,	0,		92000,	97000,	99000,	102000,	106000,	110000,	
			300,	85000,	93000,	97000,	100000,	103000,	107000,	110000,	
			400,	88000,	95000,	99000,	100000,	103000,	107000,	110000,	
			600,	92000,	96000,	99000,	101000,	104000,	108000,	111000,	
			800,	94000,	97000,	100000,	102000,	105000,	108000,	111000,	
			1000,	95000,	98000,	100000,	102000,	105000,	109000,	112000,	
			2000,	98000,	102000,	103000,	105000,	108000,	112000,	114000,	
			3000,	101000,	103000,	106000,	108000,	111000,	114000,	116000,	
			4000,	103000,	106000,	109000,	111000,	114000,	116000,	118000,	
			5000,	105000,	108000,	111000,	113000,	116000,	119000,	120000,	
			6000,	108000,	110000,	113000,	116000,	119000,	121000,	123000,	
		},
		
		
		MinLaunchDistData = 
		{
			4,		6,
					50,		100,	150,	200,	250,	300,
					
			3000,	5000,	5000,	6000,	6000,	7000,	7000,	
			4000,	5000,	6000,	7000,	8000,	8000,	9000,	
			5000,	5000,	7000,	18000,	19000,	20000,	21000,	
			6000,	5000,	26000,	29000,	31000,	32000,	33000,		
		},
		
		Damage_correction_coeff = 0.7,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = AGM_84E, --AGM-84E
		display_name = _('AGM-84E Harpoon'),
		name = "AGM_84E",
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 628.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 343.0,
        Cx_pil = 6,
        D_max = 85000.0,
        D_min = 2000.0,
        Head_Form = 0,
        Life_Time = 1850.0,
        Nr_max = 18,
        v_min = 170.0,
        v_mid = 272.0,
        Mach_max = 0.9,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 1800.0,
        Range_max = 139000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_84E"],
        -- No exhaust (turbo)
        X_back = -1.581,
        Y_back = -0.173,
        Z_back = 0.0,
        Reflection = 0.121,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = AGM_86, --AGM-86
		display_name = _('AGM-86'),
		name = "AGM_86",
        Escort = 0,
        Head_Type = 5,
		sigma = {20, 20, 20},
        M = 1500.0,
        H_max = 13000.0,
        H_min = -1,
        Diam = 697.0,
        Cx_pil = 8,
        D_max = 2500000.0,
        D_min = 10000.0,
        Head_Form = 0,
        Life_Time = 10000.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 272.0,
        Mach_max = 0.95,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 10000.0,
        Range_max = 2414000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_86"],
        -- No exhaust (turbo)
        X_back = -3.392,
        Y_back = 0.064,
        Z_back = 0.0,
        Reflection = 0.1691,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
		
    },
    {
        Name = AGM_88, --AGM-88
		display_name = _('AGM-88C'),
		name = "AGM_88",
        Escort = 0,
        Head_Type = 3,
		sigma = {10, 10, 10},
        M = 361.0,
        H_max = 24400.0,
        H_min = -1,
        Diam = 254.0,
        Cx_pil = 4,
        D_max = 134000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 180.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 3.2,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 36.0,
        Range_max = 134000.0,
        H_min_t = 0.0,
        Fi_start = 1.5707,--Angle off boresight 90°
        Fi_rak = 3.14152, --Allowable Target engagment angles 
        Fi_excort = 1.5707, --Gimbal Angle Limit 90°
        Fi_search = 0.1745, --Field of View 10°
        OmViz_max = 0.5236, --Gimbl Ang Rate Lim 30°/s 
        warhead = warheads["AGM_88"],
		exhaust1 = {.9, .9, .9, .2 }, 
		X_back = -1.506,
		Y_back = 0.0,
		Z_back = 0.0,
		exhaust2 = { .9, .9, .9, .2 }, 
		X_back_acc = -1.506,
		Y_back_acc = 0.0,
		Z_back_acc = 0.0,
        Reflection = 0.05, -- Данные от Тульского ОАО "КБП"
        KillDistance = 7.0,
		LaunchDistData =
		{
			15,		8,

					50,		100,	150,	200,	250,	300,	350,	415,
					
			100,	19000,	19000,	22000,	24000,	27000,	29000,	31000,	35000,
			400,	19000,	21000,	23000,	25000,	28000,	30000,	33000,	36000,
			700,	20000,	22000,	24000,	26000,	29000,	31000,	34000,	37000,
			1000,	21000,	23000,	25000,	28000,	30000,	33000,	35000,	38000,
			2000,	25000,	27000,	29000,	32000,	34000,	37000,	38000,	42000,
			3000,	30000,	32000,	34000,	36000,	38000,	40000,	43000,	47000,
			4000,	35000,	36000,	38000,	40000,	43000,	46000,	49000,	53000,
			5000,	41000,	41000,	42000,	45000,	48000,	51000,	55000,	58000,
			6000,	45000,	47000,	48000,	51000,	54000,	57000,	60000,	65000,
			7000,	52000,	53000,	62000,	57000,	60000,	63000,	67000,	72000,
			8000,	57000,	61000,	63000,	64000,	66000,	70000,	75000,	78000,
			9000,	63000,	66000,	69000,	71000,	74000,	77000,	82000,	88000,
			10000,	68000,	72000,	74000,	76000,	81000,	85000,	90000,	97000,
			11000,	75000,	80000,	82000,	84000,	89000,	94000,	100000,	124000,
			12000,	80000,	83000,	86000,	93000,	98000,	105000,	123000,	135000,
																  
																  
																  
		},
		
		MinLaunchDistData =
		{
			15,		8,

					50,		100,	150,	200,	250,	300,	350,	415,
			100,	8000,	8000,	9000,	10000,	11000,	11000,	12000,	13000,
			800,	8000,	8000,	9000,	10000,	11000,	12000,	12000,	13000,
			900,	3000,	3000,	3000,	10000,	11000,	12000,	12000,	13000,
			1000,	3000,	3000,	3000,	10000,	11000,	12000,	12000,	13000,
			2000,	4000,	4000,	5000,	5000,	6000,	6000,	6000,	7000,
			3000,	5000,	6000,	6000,	6000,	7000,	7000,	8000,	8000,
			4000,	6000,	7000,	7000,	8000,	8000,	9000,	9000,	10000,
			5000,	7000,	8000,	8000,	9000,	10000,	10000,	11000,	11000,
			6000,	8000,	9000,	9000,	10000,	11000,	11000,	12000,	13000,
			7000,	9000,	10000,	10000,	11000,	12000,	13000,	14000,	14000,
			8000,	10000,	11000,	12000,	12000,	13000,	14000,	15000,	16000,
			9000,	11000,	12000,	13000,	14000,	15000,	16000,	16000,	18000,
			10000,	12000,	13000,	14000,	15000,	16000,	17000,	18000,	19000,
			11000,	13000,	14000,	15000,	16000,	18000,	19000,	20000,	21000,
			12000,	14000,	15000,	17000,	18000,	19000,	20000,	21000,	23000,
		},
		nozzle_exit_area =	0.0279, -- площадь выходного сечения сопла
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
	},
	
    {
        Name = AGM_122, --AGM-122
		display_name = _('AGM-122 Sidearm'),
		name = "AGM_122",
        Escort = 0,
        Head_Type = 3,
		sigma = {4, 4, 4},
        M = 88.0,
        H_max = 24400.0,
        H_min = -1,
        Diam = 127.0,
        Cx_pil = 1,
        D_max = 16500.0,
        D_min = 3000.0,
        Head_Form = 0,
        Life_Time = 200.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.3,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 2.0,
        Range_max = 16500.0,
        H_min_t = 0.0,
        Fi_start = 0.3,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_122"],
        exhaust = tail_solid,
        X_back = -1.455,
        Y_back = -0.062,
        Z_back = 0.0,
        Reflection = 0.02,
        KillDistance = 7.0,
			
		LaunchDistData =
		{
			18,		9,
			

					0,		50,		100,	150,	200,	250,	300,	350,	400,		

			50,		9000,	11000,	10000,	10000,	11000,	11000,	12000,	12000,	12000,	
			75,		11000,	13000,	12000,	12000,	13000,	13000,	14000,	14000,	14000,	
			100,	12000,	14000,	13000,	14000,	14000,	14000,	15000,	15000,	16000,	
			125,	13000,	15000,	14000,	15000,	15000,	15000,	16000,	16000,	17000,	
			150,	14000,	15000,	15000,	15000,	16000,	16000,	17000,	17000,	18000,	
			200,	15000,	16000,	16000,	17000,	17000,	18000,	18000,	19000,	19000,	
			300,	17000,	18000,	19000,	19000,	20000,	20000,	20000,	20000,	20000,	
			400,	19000,	19000,	19000,	19000,	19000,	20000,	20000,	20000,	20000,	
			500,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	20000,	20000,	
			600,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	
			700,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	
			800,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	
			900,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,			
			6000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,	19000,
			7000,	18000,	18000,	18000,	18000,	18000,	18000,	18000,	18000,	18000,
			8000,	18000,	18000,	18000,	18000,	18000,	18000,	18000,	18000,	18000,
			9000,	17000,	17000,	17000,	17000,	17000,	17000,	17000,	17000,	17000,
			10000,	17000,	17000,	17000,	17000,	17000,	17000,	17000,	17000,	17000,
		},
		
		Damage_correction_coeff = 0.3,
    },
    {
        Name = AGM_123, --AGM-123
		display_name = _('AGM-122 Skipper 2'),
		name = "AGM_123",
        Escort = 1,
        Head_Type = 4,
		sigma = {10, 10, 10},
        M = 582.0,
        H_max = 24000.0,
        H_min = -1,
        Diam = 356.0,
        Cx_pil = 6,
        D_max = 10000.0,
        D_min = 1000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 10,
        v_min = 170.0,
        v_mid = 250.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 10.0,
        Range_max = 25000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.75,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_123"],
        exhaust = tail_solid,
        X_back = -1.832,
        Y_back = -0.201,
        Z_back = 0.0,
        Reflection = 0.075,
        KillDistance = 0.0,
    },
    {
        Name = AGM_130, --AGM-130
		display_name = _('AGM-130'),
		name = "AGM_130",
        Escort = 0,
        Head_Type = 5,
		sigma = {10, 10, 10},
        M = 1312.65,
        H_max = 24000.0,
        H_min = -1,
        Diam = 460.0,
        Cx_pil = 4,
        D_max = 40000.0,
        D_min = 2000.0,
        Head_Form = 0,
        Life_Time = 250.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 272.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 245.0,
        Range_max = 65000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_130"],
        -- No exhaust (turbo)
        X_back = -1.697,
        Y_back = -0.56,
        Z_back = 0.0,
        Reflection = 0.11,
        KillDistance = 0.0,
    },
    {
        Name = Sea_Eagle, --Sea Eagle
		display_name = _('BAe Sea Eagle'),
		name = "Sea_Eagle",
        Escort = 0,
        Head_Type = 5,
		sigma = {10, 10, 10},
        M = 600.0,
        H_max = 28000.0,
        H_min = -1,
        Diam = 400.0,
        Cx_pil = 6,
        D_max = 100000.0,
        D_min = 4000.0,
        Head_Form = 0,
        Life_Time = 1800.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 465.0,
        Range_max = 110000.0,
        H_min_t = 0.0,
        Fi_start = 0.25,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["Sea_Eagle"],
        -- No exhaust (turbo)
        X_back = -1.97,
        Y_back = -0.199,
        Z_back = 0.0,
        Reflection = 0.121,
        KillDistance = 0.0,
    },
    {
        Name = ALARM, --ALARM
		display_name = _('ALARM'),
		name = "ALARM",
        Escort = 0,
        Head_Type = 3,
		sigma = {10, 10, 10},
        M = 268.0,
        H_max = 24400.0,
        H_min = -1,
        Diam = 224.0,
        Cx_pil = 4,
        D_max = 30000.0,
        D_min = 2000.0,
        Head_Form = 1,
        Life_Time = 400.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 2.1,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 10.0,
        Range_max = 45000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["ALARM"],
        exhaust = tail_solid,
        X_back = -1.97,
        Y_back = -0.109,
        Z_back = 0.0,
        Reflection = 0.0741,
        KillDistance = 5.0,
		
		LaunchDistData =
		{
			16,		8,

					100,	150,	200,	250,	300,	350,	400,	450,
		
			100,	15000,	16000,	17000,	19000,	20000,	22000,	24000,	26000,
			400,	15000,	17000,	18000,	20000,	21000,	23000,	25000,	26000,
			700,	16000,	18000,	19000,	21000,	22000,	24000,	26000,	27000,
			1000,	17000,	18000,	20000,	21000,	23000,	25000,	26000,	28000,
			2000,	20000,	22000,	23000,	25000,	26000,	28000,	29000,	31000,
			3000,	24000,	26000,	26000,	28000,	29000,	31000,	33000,	35000,
			4000,	28000,	29000,	30000,	32000,	34000,	36000,	38000,	39000,
			5000,	32000,	34000,	35000,	36000,	38000,	39000,	41000,	44000,
			6000,	37000,	39000,	39000,	40000,	42000,	44000,	46000,	49000,
			7000,	43000,	44000,	44000,	45000,	48000,	50000,	53000,	53000,
			8000,	48000,	49000,	50000,	51000,	53000,	54000,	57000,	60000,
			9000,	53000,	54000,	55000,	56000,	58000,	61000,	63000,	66000,
			10000,	59000,	60000,	61000,	62000,	65000,	66000,	69000,	72000,
			11000,	65000,	66000,	67000,	68000,	71000,	74000,	77000,	79000,
			12000,	69000,	70000,	71000,	75000,	78000,	81000,	84000,	88000,
			13000,	73000,	76000,	78000,	81000,	85000,	89000,	93000,	93000,
		},
		
		MinLaunchDistData =
		{
			15,		8,

					100,	150,	200,	250,	300,	350,	400,	450,	
		
			1000,	6000,	6000,	7000,	7000,	8000,	8000,	9000,	10000,
			1250,	6000,	6000,	7000,	7000,	8000,	9000,	9000,	10000,	
			2000,	5000,	5000,	5000,	5000,	5000,	5000,	6000,	6000,
			2500,	5000,	5000,	5000,	5000,	5000,	6000,	6000,	7000,
			3000,	5000,	5000,	5000,	6000,	6000,	6000,	7000,	7000,
			4000,	5000,	5000,	6000,	6000,	7000,	8000,	8000,	8000,
			5000,	6000,	6000,	7000,	7000,	8000,	9000,	9000,	10000,
			6000,	6000,	7000,	8000,	8000,	9000,	10000,	10000,	11000,
			7000,	7000,	8000,	9000,	9000,	10000,	11000,	11000,	12000,
			8000,	8000,	9000,	9000,	10000,	11000,	12000,	13000,	13000,
			9000,	9000,	9000,	10000,	11000,	12000,	13000,	14000,	15000,
			10000,	9000,	10000,	11000,	12000,	13000,	14000,	15000,	16000,
			11000,	10000,	11000,	13000,	14000,	15000,	16000,	17000,	17000,
			12000,	11000,	12000,	14000,	15000,	16000,	17000,	18000,	19000,
			13000,	12000,	14000,	15000,	16000,	17000,	19000,	20000,	21000,
		},
    },
    {
        Name = Kormoran, --Kormoran
		display_name = _('AS.34 Kormoran'),
		name = "Kormoran",
        Escort = 0,
        Head_Type = 5,
		sigma = {10, 10, 10},
        M = 600.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 345.0,
        Cx_pil = 6,
        D_max = 20000.0,
        D_min = 1000.0,
        Head_Form = 1,
        Life_Time = 400.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 272.0,
        Mach_max = 0.9,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 400.0,
        Range_max = 40000.0,
        H_min_t = 0.0,
        Fi_start = 0.5,
        Fi_rak = 3.14152,
        Fi_excort = 1.05,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["Kormoran"],
        exhaust = tail_solid,
        X_back = -1.562,
        Y_back = -0.151,
        Z_back = 0.0,
        Reflection = 0.1758,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = AGM_119, --AGM-119
		display_name = _('AGM-119 Penguin'),
		name = "AGM_119",		
        Escort = 0,
        Head_Type = 5,
		sigma = {8, 8, 8},
        M = 370.0,
        H_max = 10000.0,
        H_min = -1,
        Diam = 300.0,
        Cx_pil = 3,
        D_max = 30000.0,
        D_min = 4000.0,
        Head_Form = 0,
        Life_Time = 450.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 1.2,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 400.0,
        Range_max = 35000.0,
        H_min_t = 0.0,
        Fi_start = 0.35,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_119"],
        exhaust = tail_solid,
        X_back = -1.046,
        Y_back = -0.159,
        Z_back = 0.0,
        Reflection = 0.1741,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = SM_2, --SM-2 Standard
		display_name = _('RIM-66M-2 (SM-2MR)'),	
		name = "SM_2",
        Escort = 3,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 612.0,
        H_max = 24400.0,
        H_min = 1.0,
        Diam = 340.0,
        Cx_pil = 5,
        D_max = 40000.0,
        D_min = 4800.0,
        Head_Form = 1,
        Life_Time = 95.0,
        Nr_max = 25,
        v_min = 170.0,
        v_mid = 900.0,
        Mach_max = 4.5,
        t_b = 0.0,
        t_acc = 16.0,
        t_marsh = 0.0,
        Range_max = 72420.0,
        H_min_t = 10.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SM_2"],
        exhaust = {0.8, 0.8, 0.8, 0.85 };
        X_back = -1.65,
        Y_back = -0.0,
        Z_back = 0.0,
        Reflection = 0.1766,
        KillDistance = 15.0,
		hoj = 1,
		ccm_k0 = 0.1,
		--loft_factor = 4.5,
			
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0452478, -- площадь выходного сечения сопла

		PN_coeffs = {6, 				-- Number of Entries
					8000.0 ,1.0,		-- Less 5 km to target Pn = 1
					12000.0, 0.80,
					18000.0, 0.70,
					25000.0, 0.50,		-- Between 10 and 5 km  to target, Pn smoothly changes from 0.5 to 1.0. 
					35000.0, 0.30,
					45000.0, 0.10};		-- Above 30km target, Pn is 0.10.
		

		ModelData = {   58 ,  -- model params count
						
						0.4 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.046 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.069 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.023, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.2 , -- Cy_k0 Clmax at low mach ( M << 1)
						2.4,   -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  ,  -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.3142 , -- alpha max
						2.0 , --Set really high as without this the missile refuses to turn for some reason.  It'll just go straight up
						
					-- Engine data. Time, fuel flow, thrust.  Need to find more sources on	360kg
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	4.0,  		16.0,			0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	45.86,		11.05,			0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	115640.0,	21680.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 90.0, -- Selfdestruction time in sec.
						 90.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 999999, -- Lofting logic Rmin 
						 999999, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
						 
					},
					
			proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 12,
								arm_delay	= 1.6,
							},
    },
     {
        Name = SA5B55, --5V55, S-300P, SA-10B
		display_name = _('5V55R (S-300P)'),
		name = "SA5B55",
        Escort = 3,
        Head_Type = 6,
		sigma = {50, 50, 50},
        M = 1660.0,
        H_max = 30000.0,
        H_min = 25.0,
        Diam = 508.0,
        Cx_pil = 8,
        D_max = 65000.0,
        D_min = 1000.0,
        Head_Form = 1,
        Life_Time = 1080.0,
        Nr_max = 18,
        v_min = 150.0,
        v_mid = 1300.0,
        Mach_max = 5.0,
        t_b = 0.0,
        t_acc = 11.0, -- Corrected by https://youtu.be/7i-UMYNKwcU video
        t_marsh = 0.0,
        Range_max = 75000.0,
        H_min_t = 10.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA5B55"],
        exhaust = tail_solid,
        X_back = -3.681,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.178,
        KillDistance = 20.0,
		hoj = 1,
		loft =1,
		ccm_k0 = 0.2,		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.1358556, -- площадь выходного сечения сопла
		loft_factor = 4.5, --testing
		
		PN_coeffs = {6, 				-- Number of Entries
					1000.0 ,1.0,		-- Less 5 km to target Pn = 1
					5000.0, 0.80,
					15000.0, 0.60,
					20000.0, 0.40,		-- Between 10 and 5 km  to target, Pn smoothly changes from 0.5 to 1.0. 
					25000.0, 0.30,
					30000.0, 0.10};		-- Above 30km target, Pn is 0.10.
			--testing
		
		ModelData = {   58 ,  -- model params count
						1.4 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.042 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.063 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.035, -- Cx_k3 Cd0 at high mach (M>>1)
						1.0 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						2.0 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.5,   -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  ,  -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.35 , -- alpha max
						3.0 , -- угловая скорость создаваймая моментом газовых рулей --testing to see if this helps the pulling to much g's (also check to see if 5v55R has thrust vectoring looks like it does in 3D model???_)
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	0.01,  		11.0,			0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	0.0,		110.0,			0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	0.0,		265000.0,		0.0,			0.0,		0.0,                -- thrust, newtons
					
						 100.0, -- Selfdestruction time in sec.
						 100.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 25000, -- Lofting logic Rmin 
						 100000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 1.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 1.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.

					}, 
			
			proximity_fuze = {
							ignore_inp_armed	= 1,
							radius		= 12,
							arm_delay	= 1.6,
							},
		 
    },
    {
        Name = SA48H6E2, --48N6
		display_name = _('48N6'),
		name = "SA48H6E2",
        Escort = 3,
        Head_Type = 6,
		sigma = {50, 50, 50},
        M = 1900.0,
        H_max = 30000.0,
        H_min = 10.0,
        Diam = 519.0,
        Cx_pil = 9,
        D_max = 55000.0,
        D_min = 6000.0,
        Head_Form = 1,
        Life_Time = 200.0,
        Nr_max = 25,
        v_min = 170.0,
        v_mid = 1000.0,
        Mach_max = 8.6,
        t_b = 0.0,
        t_acc = 12.0,
        t_marsh = 0.0,
        Range_max = 150000.0, 
        H_min_t = 10.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA48H6E2"],
        exhaust = tail_solid,
        X_back = -3.681,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.178,
        KillDistance = 25.0,
		ccm_k0 = 0.005,		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.1358556, -- площадь выходного сечения сопла
		loft_factor = 4.5,
		
		ModelData = {   58 ,  -- model params count
						1.4 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.042 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.063 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.035, -- Cx_k3 Cd0 at high mach (M>>1)
						1.0 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						2.5 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.5,   -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  ,  -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.35 , -- alpha max 
						3.0 , -- угловая скорость создаваймая моментом газовых рулей - experemental to see if it helps missile loose less speed at launch due to poor guidance
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	0.01,  		14.0,			0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	0.0,		40.0,			0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	0.0,		265000.0,		0.0,			0.0,		0.0,                -- thrust, newtons
					
						 100.0, -- Selfdestruction time in sec.
						 100.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 25000, -- Lofting logic Rmin 
						 100000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 1.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 1.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 1.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.

					}, 
		
    },
    {
        Name = SA9M82, --9M82
		display_name = _('9M82'),
		name = "SA9M82",
        Escort = 3,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 4600.0,
        H_max = 30000.0,
        H_min = 1000.0,
        Diam = 810.0,
        Cx_pil = 8,
        D_max = 60000.0,
        D_min = 13000.0,
        Head_Form = 1,
        Life_Time = 100.0,
        Nr_max = 27,
        v_min = 170.0,
        v_mid = 1000.0,
        Mach_max = 5.0,
        t_b = 0.0,
        t_acc = 10.0,
        t_marsh = 0.0,
        Range_max = 100000.0,
        H_min_t = 10.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA9M82"],
        exhaust = tail_solid,
        X_back = 0.138,
        Y_back = 0.0,
        Z_back = 0.0,
		X_back_acc = -3.978,
        Reflection = 0.5099,
        KillDistance = 30.0,
		ccm_k0 = 0,
    },
    {
        Name = SA9M83, --9M83
		display_name = _('9M83'),
		name = "SA9M83",
        Escort = 3,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 2400.0,
        H_max = 25000.0,
        H_min = 25.0,
        Diam = 810.0,
        Cx_pil = 8,
        D_max = 45000.0,
        D_min = 6000.0,
        Head_Form = 1,
        Life_Time = 75.0,
        Nr_max = 27,
        v_min = 170.0,
        v_mid = 1000.0,
        Mach_max = 5.3,
        t_b = 0.0,
        t_acc = 10.0,
        t_marsh = 0.0,
        Range_max = 75000.0,
        H_min_t = 250.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA9M83"],
        exhaust = tail_solid,
        X_back = 0.138,
        Y_back = 0.0,
        Z_back = 0.0,
		X_back_acc = -2.103,
        Reflection = 0.4212,
        KillDistance = 30.0,
    },
    {
        Name = SAV611, --V-611
		display_name = _('V-611'),
		name = "SAV611",
        Escort = 3,
        Head_Type = 6,
		sigma = {15, 15, 15},
        M = 1844.0,
        H_max = 25000.0,
        H_min = -1,
        Diam = 600.0,
        Cx_pil = 8,
        D_max = 10000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 100.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 2.7,
        t_b = 0.0,
        t_acc = 10.0,
        t_marsh = 0.0,
        Range_max = 30000.0,
        H_min_t = 100.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SAV611"],
        exhaust = tail_solid,
        X_back = -1.917,
        Y_back = 0.121,
        Z_back = 0.0,
        Reflection = 0.178,
        KillDistance = 15.0,
    },
    {
        Name = SA3M9M, --3M9M Kub
		display_name = _('3M9M3 Kub'),
		name = "SA3M9M",
        Escort = 3,
        Head_Type = 6,
		sigma = {50, 50, 50},
        M = 600.0,
        H_max = 8000.0,
        H_min = 30.0,
        Diam = 330.0,
        Cx_pil = 4,
        D_max = 25000.0,
        D_min = 4000.0,
        Head_Form = 1,
        Life_Time = 60.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 450.0,
        Mach_max = 2.2,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 22.0,
        Range_max = 25000.0,
        H_min_t = 30.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA3M9M"],
		exhaust1 = { .80, .80, .80, .3 },
		X_back = -2.8,
		Y_back = 0.0,
		Z_back = 0.0,
		exhaust2 = { .86, .86, .86, .8 },
		X_back_acc = -2.8,
		Y_back_acc = 0.0,
		Z_back_acc = 0.0,
        Reflection = 0.106,
        KillDistance = 18.0,
		ccm_k0 = 0.1;
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.0314, -- площадь выходного сечения сопла

		
   			ModelData = {   58 ,  -- model params count
							
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.029 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.049 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.027, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						1.5 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.2	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.175 , -- alpha max
						0.0, --угловая скорость создаваймая моментом газогенератора ориентации
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	4.0,  		20.0,			0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	16.76,		8.6,			0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	123480.0,	13500.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 50.0, -- Selfdestruction time in sec.
						 45.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 1e9, -- Lofting logic Rmin 
						 1e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
					},
			proximity_fuze = {
							ignore_inp_armed	= 1,
							radius		= 18,
							arm_delay	= 1.6,
								},

    },
    {
        Name = SA9M33, --9M33 Osa
		display_name = _('9M33M3 Osa'),
		name = "SA9M33",
        Escort = 3,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 126.3,
        H_max = 5000.0,
        H_min = 25.0,
        Diam = 206.0,
        Cx_pil = 2,
        D_max = 10300.0,
        D_min = 1500.0,
        Head_Form = 1,
        Life_Time = 27.0,
        Nr_max = 14,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 2.1,
        t_b = 0.0,
        t_acc = 2.0,
        t_marsh = 10.0,
        Range_max = 12000.0,
        H_min_t = 10.0,
        Fi_start = 3.14152,  --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 0.9599, --Gimbal Angle Limit
        Fi_search = 0.1396, --Field of View
        OmViz_max = 0.3840, --Gimbl Ang Rate Lim 35°/s
        warhead = warheads["SA9M33"],
		exhaust = { 1.0, 1.0, 1.0, .5 },
        X_back = -1.7,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0404,
        KillDistance = 5.0,
		ccm_k0 = 1.0,
								
		PN_coeffs = {2, 				-- Number of Entries	
					5000.0 ,1.0,		-- Less 5 km to target Pn = 1
					12000.0, 0.8};		-- Between 15 and 5 km  to target, Pn smoothly changes from 0.4 to 1.0. Longer then 15 km Pn = 0.4.
			

		nozzle_exit_area =	0.07339, -- площадь выходного сечения сопла
	

   			ModelData = {   58 ,  -- model params count
							
						0.18 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.043 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.073 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.055, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						2.07 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.86 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.3142 , -- alpha max
						0.0, --угловая скорость создаваймая моментом газогенератора ориентации
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	2.0,  		13.2,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	13.05,		3.7,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	26207.0,	3700.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 27.0, -- Selfdestruction time in sec.
						 27.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 2.0, -- Autopilot delay, sec
						 1e9, -- Lofting logic Rmin 
						 1e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
					},
								proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 4.0,
								arm_delay	= 1.6,
													},
    },
    {
        Name = SA9M31, --9M31 Strela-1
		display_name = _('9M31 Strela-1'),
		name = "SA9M31",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 32.0,
        H_max = 3500.0,
        H_min = 30.0,
        Diam = 120.0,
        Cx_pil = 2,
        D_max = 4200.0,
        D_min = 800.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 250.0,
        Mach_max = 1.3,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 7.0,
        Range_max = 4200.0,
        H_min_t = 30.0,
        Fi_start = math.rad(1),
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA9M31"],
        exhaust = tail_solid,
        X_back = -0.95,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.04,
        KillDistance = 2.5,
		--seeker sensivity params
		SeekerSensivityDistance = 10000, -- The range of target with IR value = 1. In meters.
		SeekerCooled			= false, -- True is cooled seeker and false is not cooled seeker.
		ccm_k0 = 0.1,
    },
    {
        Name = SA9M38M1, --9M38M1 Buk
		display_name = _('9M38M1 Buk-M1'),
        name = "SA9M38M1",
		Escort = 3,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 685.0,
        H_max = 24000.0,
        H_min = 15.0,
        Diam = 400.0,
        Cx_pil = 4,
        D_max = 35000.0,  -- по нижней границе
        D_min = 3320.0,
        Head_Form = 1,
        Life_Time = 60.0,
        Nr_max = 19,
        v_min = 170.0,
        v_mid = 430.0,
        Mach_max = 3.4,
        t_b = 0.0,
        t_acc = 8.0,
        t_marsh = 12.0,
        Range_max = 35000.0, -- по верхней границе
        H_min_t = 15.0,
        Fi_start = 3.14152, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 0.9599, --Gimbal Angle Limit
        Fi_search = 0.1396, --Field of View
        OmViz_max = 0.3491, --Gimbl Ang Rate Lim 20°/s
        warhead = warheads["SA9M38M1"],
        exhaust = tail_solid,
        X_back = -2.89,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0918,
        KillDistance = 13.0,
		
		nozzle_exit_area =	0.07339, -- площадь выходного сечения сопла
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
				
		PN_coeffs = {4, 				-- Number of Entries	
					1800.0 ,1.0,		-- Less 5 km to target Pn = 1
					4000.0, 1.1,
					10000.0, 0.4,		-- Between 10 and 5 km  to target, Pn smoothly changes from 0.5 to 1.0. 
					20000.0, 0.2};		-- Longer then 20 km Pn = 0.2.
			
   			ModelData = {   58 ,  -- model params count
							
						0.40 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.029 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.053 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.027, -- Cx_k3 Cd0 at high mach (M>>1)
						1.5 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.5 , --“polar dump factor"
						
						-- Lift (Cy) 
						2.027 , -- Cy_k0 Clmax at low mach ( M << 1)
						3.5	 , -- Cy_k1 (rerun  at higher res mesh latter) планка Cy0 на сверхзвуке ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.314 , -- alpha max
						0.0, --угловая скорость создаваймая моментом газогенератора ориентации
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	8.0,  		9.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	12.03,		5.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	60754.0,	16200.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1e9, -- Selfdestruction time in sec.
						 60.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.5, -- Autopilot delay, sec
						 1e9, -- Lofting logic Rmin 
						 1e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
					},
			proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 8,
								arm_delay	= 2.0,
													},
   },
    {
        Name = SA9M333, --9M333 Strela-10
		display_name = _('9M333 Strela-10'),
		name = "SA9M333",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 39.0,
        H_max = 3500.0,
        H_min = 25.0,
        Diam = 120.0,
        Cx_pil = 2,
        D_max = 5000.0,
        D_min = 800.0,
        Head_Form = 0,
        Life_Time = 30.0,
        Nr_max = 16,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 5.0,
        Range_max = 5000.0,
        H_min_t = 10.0,
        Fi_start = math.rad(1),
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA9M333"],
        exhaust = tail_solid,
        X_back = -1.25,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.05,
        KillDistance = 3.0,
		--seeker sensivity params
		SeekerSensivityDistance = 15000, -- The range of target with IR value = 1. In meters.
		ccm_k0 = 0.1,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.
    },
    {
        Name = SA9M330, --9M330 Tor, SA-15
		display_name = _('9M330 Tor'),
		name = "SA9M330",
        Escort = 3,
        Head_Type = 6,
		sigma = {15, 15, 15},
        M = 165.0,
        H_max = 6000.0,
        H_min = 10.0,
        Diam = 220.0,
        Cx_pil = 2,
        D_max = 12000.0,
        D_min = 1500.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 30,
        v_min = 170.0,
        v_mid = 430.0,
        Mach_max = 3.0,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 6.0,
        Range_max = 12000.0,
        H_min_t = 10.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 2.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA9M330"],
        exhaust = tail_solid,
        X_back = -1.77,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0307,
        KillDistance = 7.0,
		ccm_k0 = 0.2,
		--[[	ModelData = {   58 ,  -- model params count
						0.18 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.06 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.1 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.06, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.8 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.4, -- alpha max
						5.0, --угловая скорость создаваймая моментом газогенератора ориентации
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		1.8,	5.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	12.03,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	15000.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 60.0, -- Selfdestruction time in sec.
						 60.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.5, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 25.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 5.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
						 0.0,
						 0.0,
						0.0,
						0.0,
					}, --]]
		
    },
    {
        Name = SA9M311, --9M311 Tunguska
		display_name = _('9M311 Tunguska'),
		name = "SA9M311",
        Escort = 3,
        Head_Type = 8,
		sigma = {10, 10, 10},
        M = 42.0,
        H_max = 3500.0,
        H_min = 14.5,
        Diam = 100.0,
        Cx_pil = 6,
        D_max = 8000.0,
        D_min = 2000.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 18,
        v_min = 170.0,
        v_mid = 600.0,
        Mach_max = 2.82,
        t_b = 0.0,
        t_acc = 2.0,
        t_marsh = 0.0,
        Range_max = 8000.0,
        H_min_t = 14.5, -- cause ASM fly on 14.9992 m above the sea level. NOT ON 15
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SA9M311"],
        exhaust = {1.0, 1.0, 1.0, 0.4 };					 
		X_back = 0.0,
        Y_back = 0.0,
        Z_back = 0.0,
		X_back_acc = -1.7,
        Reflection = 0.0102,
        KillDistance = 5.0,
		
		ModelData = {   58 ,  -- model params count
						0.06 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.037 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.063 , -- Cx_k1 Peak Cd0 value 
						0.05 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0086, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						0.8 , -- Cy_k0 Clmax at low mach ( M << 1)
						0.8	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.4 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	2.6,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	5.39,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	14970.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 30.0, -- Selfdestruction time in sec.
						 60.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 0.5, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 30.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 3.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
						 0.0,
						 0.0,
						0.0,
						0.0,
					},
    },
    {
        Name = Igla_1E, --9M39 Igla
		display_name = _('Igla S'),
		name = "Igla_1E",
        Escort = 0,
        Head_Type = 1,
		sigma = {5, 5, 5},
        M = 10.8,
        H_max = 3500.0,
        H_min = 1.0,
        Diam = 72.0,
        Cx_pil = 1,
        D_max = 4500.0,
        D_min = 500.0,
        Head_Form = 1,
        Life_Time = 17.0,
        Nr_max = 16,
        v_min = 70.0,
        v_mid = 570.0,
        Mach_max = 2.2,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 3.0,
        Range_max = 4500.0,
        H_min_t = 10.0,
        Fi_start = math.rad(1),
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["Igla_1E"],
        exhaust = {1.0, 1.0, 1.0, 0.6 };
        X_back = -0.781,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0044,
        KillDistance = 1.0,
		--seeker sensivity params
		SeekerSensivityDistance = 10000, -- The range of target with IR value = 1. In meters.
		ccm_k0 = 0.35,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.		
    },
    {
        Name = MIM_104, --MIM-104 Patriot
		display_name = _('MIM-104C Pac 2'),
		name = "MIM_104_Pac2",
        Escort = 3,
        Head_Type = 6,
		sigma = {10, 10, 10},
        M = 910.0,
        H_max = 24240.0,
        H_min = 45.0,
        Diam = 410.0,
        Cx_pil = 4,
        D_max = 30000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 300.0,
        Nr_max = 25,
        v_min = 170.0,
        v_mid = 600.0,
        Mach_max = 5.0,
        t_b = 0.0,
        t_acc = 13.0,
        t_marsh = 5.0,
        Range_max = 120000.0,
        H_min_t = 25.0,
        Fi_start = 3.14152, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 1.0472, --Gimbal Angle Limit
        Fi_search = 0.1396, --Field of View
        OmViz_max = 0.6981, --Gimbl Ang Rate Lim 40°/s
        warhead = warheads["MIM_104"],
        exhaust = {1.0, 1.0, 1.0, 0.8 };
        X_back = -2.68,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.1066,
        KillDistance = 13.0,
		loft = 1,
		hoj = 1,
		ccm_k0 = 0.01,
		loft_factor = 4.5,
		
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке (doesn't seem to do much if anything?)
		nozzle_exit_area =	0.09290304, -- площадь выходного сечения сопла
		
		
		PN_coeffs = {4, 				-- Number of Entries
					9260.0 , 1.0,		-- Less 5 Nmi to target Pn = 1.0
					27780.0, 0.5,		-- Between 15 and 5 Nmi  to target, Pn smoothly changes from 0.5 to 1.0.
					46300.0, 0.2,		-- Between 25 and 15 Nmi  to target, Pn smoothly changes from 0.2 to 0.5. 
					83340.0, 0.05};		-- Between 45 and 25 NMi  to target, Pn smoothly changes from 0.05 to 0.2. Longer then 45 Nmi Pn = 0.05.
				
				ModelData = {   58 ,  -- model params count
						
						1.1 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.0141 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.083 , -- Cx_k1 Peak Cd0 value 
						0.02 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.0192, -- Cx_k3 Cd0 at high mach (M>>1)
						1.5 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.5 , --“polar dump factor"
						
						-- Lift (Cy) 
						2.0 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.5,   -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  ,  -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.31 , -- alpha max
						0.0 , -- угловая скорость создаваймая моментом газовых рулей
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	13.0,   	0.0,			0.0,			0.0,		1.0e9,         -- time of stage, sec https://youtu.be/OKQlFpMaatA?t=202
						0.0,		0.0,	38.5,		0.0,			0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						0.0,		0.0,    92533.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
								
						 170.0, -- Selfdestruction time in sec.
						 170.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 27780, -- Lofting logic Rmin 
						 83340, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.7854,  -- Angle in radians of loft 
						 50.0, -- продольное ускорения взведения взрывателя
						 30.0,-- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						  -- DLZ settings. 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0, 
						 
					}, 	
			
			proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 15,
								arm_delay	= 2.0,
							},
		
	},
    {
        Name = MIM_72G, --MIM-72G Chaparrel
		display_name = _('MIM-72G Chaparrel'),
		name = "MIM_72G",
        Escort = 0,
        Head_Type = 1,
		sigma = {10, 10, 10},
        M = 84.0,
        H_max = 3000.0,
        H_min = -1,
        Diam = 127.0,
        Cx_pil = 1,
        D_max = 8500.0,
        D_min = 300.0,
        Head_Form = 0,
        Life_Time = 80.0,
        Nr_max = 30,
        v_min = 140.0,
        v_mid = 350.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 0.0,
        Range_max = 8500.0,
        H_min_t = 5.0,
        Fi_start = math.rad(1),
        Fi_rak = 3.14152,
        Fi_excort = 0.79,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["MIM_72G"],
        exhaust = tail_solid,
        X_back = -1.7,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0182,
        KillDistance = 5.0,
		--seeker sensivity params
		SeekerSensivityDistance = 15000, -- The range of target with IR value = 1. In meters.
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				
		ccm_k0	= 0.3,
		
		ModelData = {   58,  -- model params count
						0.35 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.049, -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.082, -- Cx_k1 Peak Cd0 value 
						0.010, -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.001, -- Cx_k3 Cd0 at high mach (M>>1)
						0.550, -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						0.8, --“polar dump factor"
						
						-- Lift (Cy) 
						2.5, -- Cy_k0 Clmax at low mach ( M << 1)
						0.8, -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2, -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.13, -- alpha max
						0.00, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,	   -1.0,	5.20,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	5.27,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	11890.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- Selfdestruction time in sec.
						 60.0, -- lifetime in seconds
						 0, -- Minimum height in M
						 1.0, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 8.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},
    },
    {
        Name = FIM_92C, --FIM-92C
		display_name = _('FIM-92C'),
		name = "FIM_92C",
        Escort = 0,
        Head_Type = 1,
		sigma = {3, 3, 3},
        M = 10.1,
        H_max = 3500.0,
        H_min = 1.0,
        Diam = 70.0,
        Cx_pil = 1,
        D_max = 5000.0,
        D_min = 200.0,
        Head_Form = 1,
        Life_Time = 30.0, -- temporary бортовая батарея работает 19 сек
        Nr_max = 18,
        v_min = 70.0,
        v_mid = 300.0,
        Mach_max = 2.2,
        t_b = 0.0,
        t_acc = 3.0,
        t_marsh = 3.0,
        Range_max = 6000.0,
        H_min_t = 10.0,
        Fi_start = math.rad(1),
        Fi_rak = 3.14152,
        Fi_excort = 0.7,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["FIM_92C"],
        exhaust = tail_solid,
        X_back = -0.781,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0044,
        KillDistance = 0.0,
		--seeker sensivity params
		SeekerSensivityDistance = 10000, -- The range of target with IR value = 1. In meters.
		ccm_k0 = 0.25,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolute resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.		
    },
    {
        Name = SA5B27, --5V27 S-125
		display_name = _("5V27 GOA Mod.1 (SA-3B)"),
		name = "SA5B27",
        Escort = 3,
        Head_Type = 6,
		sigma = {80, 80, 80},
        M = 802.0,
        H_max = 18000.0,
        H_min = 20,
        Diam = 340.0,
        Cx_pil = 8,
        D_max = 29000.0,
        D_min = 6000.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 430.0,
        Mach_max = 3.0,
        t_b = 0.0,
        t_acc = 4.0,
        t_marsh = 22.0,
        Range_max = 29000.0,
        H_min_t = 200.0,
        Fi_start = 3.14152, 	--Angle off boresight
        Fi_rak = 3.14152,   	--Allowable Target engagment angles
        Fi_excort = 0.9599, 	--Gimbal Angle Limit
        Fi_search = 1.5708, 	--Field of View
        OmViz_max = 0.2618, 	--Gimbl Ang Rate Lim 15°/s
        warhead = warheads["SA5B27"],
		exhaust1 = { 1.0, 1.0, 1.0, .5 }, --First stage is nearly smokless (for first gen SAM's)
		X_back = -0.65,
		Y_back = 0.0,
		Z_back = 0.0,
		exhaust2 = { 1.0, 1.0, 1.0, 1.0 }, --More smoke for second stage
		X_back_acc = -2.87,
		Y_back_acc = 0.0,
		Z_back_acc = 0.0,
        Reflection = 0.1676,
        KillDistance = 12.5,
		hoj = 1; --SA3 has "HOJ" even if it is super limited and not very effective IRL
		
		ccm_k0 = 1.5,
		supersonic_A_coef_skew = .25,
		nozzle_exit_area =	0.08043, 
			
		ModelData = {   58,  -- model params count
					
						0.34 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.238 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.65 , -- Cx_k1 Peak Cd0 value 
						0.009 ,  -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.08, -- Cx_k3 Cd0 at high mach (M>>1)
						3.8 ,   -- Cx_k4 steepness of the drag curve after the transonic wave crisis  3.5
						1.2 ,   --“polar dump factor"
						
						-- Lift (Cy) 
						1.3 , -- Cy_k0 Clmax at low mach ( M << 1)
						1.3 , -- Cy_k0 Clmax at high mach ( M >> 1)
						1.2  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.20 , -- alpha max
						0.0, --Additional g’s due to thrust vectoring/rocket motors 
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	 4.0,  		22.0,			0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	 70.0,		6.86,			0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,     100000.0,	16100.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
						 30.0, -- Selfdestruction time in sec.
						 30.0, -- Missile lifetime in sec.
						 0.0, -- Minimum height in M
						 0.50, -- Autopilot delay, sec ----lower than irl due to missile not mantaining nose up pitch upon launch.
						 6000, -- Lofting logic Rmin 
						 20000, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.2,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 8.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},		
   		
		proximity_fuze = {
						ignore_inp_armed	= 1,
						radius		= 20,
						arm_delay	= 4.0,
							},
						  
	},
    {
        Name = HAWK_RAKETA, --MIM-23K Hawk http://en.wikipedia.org/wiki/MIM-23_Hawk
		display_name = _('MIM-23K Hawk'),
		name = "HAWK_RAKETA",
        Escort = 3,
        Head_Type = 6,
		sigma = {50, 50, 50},
        M = 638.0,
        H_max = 18000.0,
        H_min = 60.0,
        Diam = 370.0,
        Cx_pil = 4,
        D_max = 22000.0,
        D_min = 1500.0,
        Head_Form = 1,
        Life_Time = 110.0,
        Nr_max = 20,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 2.3,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 21.0,
        Range_max = 46300.0,
        H_min_t = 25.0,
        Fi_start = 3.14152, --Angle off boresight
        Fi_rak = 3.14152, --Allowable Target engagment angles
        Fi_excort = 0.9599311, --Gimbal Angle Limit
        Fi_search = 0.1396, --Field of View
        OmViz_max = 0.4363323, --Gimbl Ang Rate Lim 25°/s 
        warhead = warheads["HAWK_RAKETA"],
        exhaust = {0.4, 0.4, 0.4, 0.6 }; --Reduced smoke motor
        X_back = -2.70,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.1676,
        KillDistance = 15.0,
    
		supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
		nozzle_exit_area =	0.07526075, -- площадь выходного сечения сопла
	
		PN_coeffs = {5, 				-- Number of Entries			   
				1800.0 ,1.0,			-- Less 1.8 km to target Pn = 1
				4630 , 	0.6,			-- Between 2.5 and .97 Nm  to target, Pn smoothly changes from 1.2 to 1.0. Longer then 15 km Pn = 0.4.
				9260 ,	0.9,			-- Between 10 and 5 Nm  to target, Pn smoothly changes from 1.5 to 1.2. Longer then 15 km Pn = 0.4.	
				18520, 0.6,				-- Between 25 and 10 Nm  to target, Pn smoothly changes from .4 to 1.0. Longer then 15 km Pn = 0.4.
				25000, 0.4};		
				
				
				ModelData = {   58,  -- model params count
						
						0.9 ,   -- characteristic square (характеристическая площадь)
						
						-- Drag (Сx)  
						0.037 , -- Cx_k0 Cd0 at low mach ( M << 1) 
						0.063 , -- Cx_k1 Peak Cd0 value 
						0.03 , -- Cx_k2 steepness of the drag curve before the transonic wave crisis 
						0.021, -- Cx_k3 Cd0 at high mach (M>>1)
						1.2 , -- Cx_k4 steepness of the drag curve after the transonic wave crisis  
						1.2 , --“polar dump factor"
						
						-- Lift (Cy) 
						3.33 , -- Cy_k0 Clmax at low mach ( M << 1)
						2.15	 , -- Cy_k0 Clmax at high mach ( M >> 1)
						0.546  , -- Cy_k2 steepness of the lift curve after the transonic wave crisis  
						
						0.297 , -- Alfa_max of 17°
						0.0, 	--No thrust vectoring
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_boost		t_sustain		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	 5.0,  		21.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	 15.0,		8.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,     78625.0,	11570.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 110.0, -- Time to selfdestruct
						 110.0, -- lifetime in seconds
						 0.0, -- Minimum height in M
						 2.0, -- Autopilot delay, sec
						 1.0e9, -- Lofting logic Rmin 
						 1.0e9, -- Range to target at which loft is complete (must be larger than previous value)  
						 0.0,  -- Angle in radians of loft 
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 8.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 
						 0.0,
						 0.0,
						 0.0,
						 0.0,
						 0.2, 
						 0.6,
						 1.4,
						-3.0,
						0.5,
					},
			proximity_fuze = {
								ignore_inp_armed	= 1,
								radius		= 15,
								arm_delay	= 1.6,
													},
	},
    {
        Name = ROLAND_R, --Roland
		display_name = _('XMIM-115 Roland'),
		name = "ROLAND_R",		
        Escort = 3,
        Head_Type = 6,
		sigma = {15, 15, 15},
        M = 75.0,
        H_max = 6000.0,
        H_min = 10,
        Diam = 150.0,
        Cx_pil = 2,
        D_max = 8000.0,
        D_min = 500.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 14,
        v_min = 170.0,
        v_mid = 570.0,
        Mach_max = 2.0,
        t_b = 0.3,
        t_acc = 3.0,
        t_marsh = 13.2,
        Range_max = 8000.0,
        H_min_t = 10.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["ROLAND_R"],
        exhaust = tail_solid,
        X_back = -1.200,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.0196,
        KillDistance = 5.0,
    },
    {
        Name = P_35, --P-35
		display_name = _('P-35'),
		name = "P_35",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 4500.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 830.0,
        Cx_pil = 1,
        D_max = 300000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 442.0,
        Mach_max = 1.3,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 800.0,
        Range_max = 300000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_35"],
        exhaust1 = tail_solid,
        X_back = -4.734,
        Y_back = 0.277,
        Z_back = 0.0,
		X_back_acc = -4.712,
        Y_back_acc = -0.354,
        Z_back_acc = 0.0,
        Reflection = 0.3967,
        KillDistance = 0.0,
    },
    {
        Name = P_500, --P-500 "Базальт"
		display_name = _('P-500 (SS-N-12 Sandbox)'),
		name = "P_500",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 4800.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 950.0,
        Cx_pil = 1,
        D_max = 500000.0,
        D_min = 13000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 986.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 1800.0,
        Range_max = 500000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_500"],
        exhaust1 = tail_solid,
        X_back = -5.814,
        Y_back = 0.30,
        Z_back = 0.0,
        Reflection = 0.75,
        KillDistance = 0.0,
    },
    {
        Name = P_700, --P-700 "Гранит"
		display_name = _('P-700 (SS-N-19 Shipwreck)'),
		name = "P_700",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 6900.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 850.0,
        Cx_pil = 1,
        D_max = 550000.0,
        D_min = 13000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 850.0,
        Mach_max = 2.5,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 1800.0,
        Range_max = 550000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_700"],
        exhaust1 = tail_solid,
        X_back = -4.231,
        Y_back = 0.0,
        Z_back = 0.0,
		X_back_acc = -5.231,
        Y_back_acc = 0.0,
        Z_back_acc = 0.0,
        Reflection = 0.79,
        KillDistance = 0.0,
    },
    {
        Name = P_15U, --P-15U
		display_name = _('P-15U (SS-N-2 Styx)'),
		name = "P_15U",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 2325.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 470.0,
        Cx_pil = 1,
        D_max = 40000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 374.0,
        Mach_max = 1.1,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 800.0,
        Range_max = 40000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_15U"],
        exhaust1 = tail_solid,
        X_back = -3.373,
        Y_back = -0.124,
        Z_back = 0.0,
		X_back_acc = -2.273,
        Y_back_acc = -0.669,
        Z_back_acc = 0.0,
        Reflection = 0.3967,
        KillDistance = 0.0,
    },
    {
        Name = P_120,
		display_name = _('P-120 (SS-N-9 Siren)'),
		name = "P_120",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 2500.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 720.0,
        Cx_pil = 1,
        D_max = 110000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 12,
        v_min = 170.0,
        v_mid = 306.0,
        Mach_max = 0.9,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 250.0,
        Range_max = 110000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_120"],
        exhaust1 = tail_solid,
        X_back = -4.421,
        Y_back = 0.086,
        Z_back = 0.0,
		X_back_acc = -4.078,
        Y_back_acc = -0.277,
        Z_back_acc = 0.0,
        Reflection = 0.21,
        KillDistance = 0.0,
    },
    {
        Name = R_85,
		display_name = _('85R Metel (SS-N-14 Silex)'),
		name = "R_85",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 1900.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 830.0,
        Cx_pil = 3,
        D_max = 350000.0,
        D_min = 5000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 10,
        v_min = 170.0,
        v_mid = 400.0,
        Mach_max = 2.0,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 250.0,
        Range_max = 350000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["R_85"],
        exhaust1 = tail_solid,
        exhaust2 = tail_liquid,
        X_back = -6.521,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.3967,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = R_85U,
		display_name = _('85RU Rastrub (SS-N-14 Silex)'),
		name = "R_85U",
        Escort = 0,
        Head_Type = 5,
		sigma = {15, 15, 15},
        M = 1900.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 830.0,
        Cx_pil = 3,
        D_max = 55000.0,
        D_min = 2000.0,
        Head_Form = 1,
        Life_Time = 1800.0,
        Nr_max = 10,
        v_min = 170.0,
        v_mid = 350.0,
        Mach_max = 1.0,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 250.0,
        Range_max = 55000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["R_85U"],
        exhaust1 = tail_solid,
        exhaust2 = tail_liquid,
        X_back = -3.6,
        Y_back = 0.294,
        Z_back = -0.403,
		X_back_acc = -3.577,
        Y_back_acc = -0.041,
        Z_back_acc = -0.403,
        Reflection = 0.3967,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = BGM_109B, --BGM-109B Tomahawk
		display_name = _('BGM-109B Tomahawk'),
		name = "BGM_109B",
        Escort = 0,
        Head_Type = 5,
		sigma = {20, 20, 20},
        M = 1225.0,
        H_max = 12000.0,
        H_min = -1,
        Diam = 830.0,
        Cx_pil = 1,
        D_max = 460000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 7200.0,
        Nr_max = 17,
        v_min = 170.0,
        v_mid = 238.0,
        Mach_max = 0.85,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 7200.0,
        Range_max = 460000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["BGM_109B"],
        exhaust1 = tail_solid,
        X_back = -3.463,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.3967,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = AGM_84S, --AGM-84S
		display_name = _('AGM-84S Harpoon'),
		name = "AGM_84S",
        Escort = 0,
        Head_Type = 5,
		sigma = {10, 10, 10},
        M = 661.5,
        H_max = 10000.0,
        H_min = -1,
        Diam = 343.0,
        Cx_pil = 6,
        D_max = 95000.0,
        D_min = 3000.0,
        Head_Form = 1,
        Life_Time = 1850.0,
        Nr_max = 18,
        v_min = 170.0,
        v_mid = 306.0,
        Mach_max = 0.9,
        t_b = 0.0,
        t_acc = 5.0,
        t_marsh = 1800.0,
        Range_max = 241401.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["AGM_84S"],
        exhaust1 = tail_solid,
        X_back = -2.235,
        Y_back = -0.173,
        Z_back = 0.0,
        Reflection = 0.1058,
        KillDistance = 0.0,
		add_attributes = {"Cruise missiles"},
    },
    {
        Name = MALUTKA, --9M14 Malutka
		display_name = _('AT-3 Sagger'),
		name = "MALUTKA",
        Escort = 1,
        Head_Type = 7,
		sigma = {5, 5, 5},
        M = 10.9,
        H_max = 2000.0,
        H_min = -1,
        Diam = 100.0,
        Cx_pil = 6,
        D_max = 3000.0,
        D_min = 100.0,
        Head_Form = 1,
        Life_Time = 30.0,
        Nr_max = 1.1,
        v_min = 105.0,
        v_mid = 115.0,
        Mach_max = 0.45*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 2.0,
        t_marsh = 30.0,
        Range_max = 3000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["MALUTKA"],
        exhaust = {0.9, 0.9, 0.9, 0.2};
        X_back = -0.372,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.02,
        KillDistance = 0.0,
    },
    {
        Name = KONKURS, --9M113 Konkurs
		display_name = _('AT-5 Spandrel'),
		name = "KONKURS",
        Escort = 1,
        Head_Type = 7,
		sigma = {4, 4, 4},
        M = 40.0,
        H_max = 2000.0,
        H_min = -1,
        Diam = 100.0,
        Cx_pil = 6,
        D_max = 4000.0,
        D_min = 75.0,
        Head_Form = 1,
        Life_Time = 25.0,
        Nr_max = 1.2,
        v_min = 105.0,
        v_mid = 200.0,
        Mach_max = 0.65*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 0.1,
        t_marsh = 30.0,
        Range_max = 4000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["KONKURS"],
        exhaust = {0.9, 0.9, 0.9, 0.1},
        X_back = -0.2,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.035,
        KillDistance = 0.0,
    },
    {
        Name = P_9M117, --9M117 Bastion
		display_name = _('AT-10 Stabber'),
		name = "P_9M117",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 17.6,
        H_max = 2000.0,
        H_min = -1,
        Diam = 100.0,
        Cx_pil = 6,
        D_max = 4000.0,
        D_min = 100.0,
        Head_Form = 1,
        Life_Time = 20.0,
        Nr_max = 1.1,
        v_min = 105.0,
        v_mid = 200.0,
        Mach_max = 1.11*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 0.3,
        t_marsh = 30.0,
        Range_max = 4000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_9M117"],
        exhaust = {0.9, 0.9, 0.9, 0.1};
        X_back = 0.0,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.032,
        KillDistance = 0.0,
    },
	{
        Name = REFLEX, --ПТУР 9M119 (AT-11 Sniper), КУВ Рефлекс, Т-80УД
		display_name = _('AT-11 Sniper'),
		name = "REFLEX",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 17.2,
        H_max = 3000.0,
        H_min = -1,
        Diam = 125.0,
        Cx_pil = 6,
        D_max = 5000.0,
        D_min = 100.0,
        Head_Form = 1,
        Life_Time = 20.0,
        Nr_max = 1.2,
        v_min = 150.0,
        v_mid = 260.0,
        Mach_max = 0.85*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 0.5,
        t_marsh = 12.0,
        Range_max = 5000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_9M119"],
		exhaust = {0.9, 0.9, 0.9, 0.1};
        X_back = -0.1,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.032,
        KillDistance = 0.0,
    },
	{
        Name = SVIR, --ПТУР 9M119 (AT-11 Sniper), КУВ Свирь, Т-72Б
		display_name = _('AT-11 Sniper'),
		name = "SVIR",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 17.2,
        H_max = 3000.0,
        H_min = -1,
        Diam = 125.0,
        Cx_pil = 6,
        D_max = 4000.0,
        D_min = 100.0,
        Head_Form = 1,
        Life_Time = 17.6,
        Nr_max = 1.2,
        v_min = 150.0,
        v_mid = 300.0,
        Mach_max = 1.15*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 0.5,
        t_marsh = 12.0,
        Range_max = 4000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_9M119"],
		exhaust = {0.9, 0.9, 0.9, 0.1};
        X_back = -0.1,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.032,
        KillDistance = 0.0,
    },
    {
        Name = TOW, --BGM-71D Tow, wsType_AS_Missile, for aerial
		display_name = _('BGM-71 TOW'),
		name = "TOW",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 40.0,
        H_max = 2000.0,
        H_min = -1,
        Diam = 150.0,
        Cx_pil = 6,
        D_max = 3000.0,
        D_min = 400.0, 
        Head_Form = 1,
        Life_Time = 25.0,
        Nr_max = 3.0,
        v_min = 105.0,
        v_mid = 150.0,
        Mach_max = 0.7,
        t_b = 0.0,
        t_acc = 0.5,
        t_marsh = 30.0,
        Range_max = 3750.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["TOW"],
        exhaust = {0.9, 0.9, 0.8, 0.1};
        X_back = -0.25,
        Y_back =  0.0,
        Z_back =  0,
        Reflection = 0.033,
        KillDistance = 0.0,
    },
    {
        Name			= AGM_154, --AGM-154C
		display_name	= _('AGM-154'),
		name			= "AGM_154",
        Escort			= 0,
        Head_Type		= 5,
		sigma			= {10, 10, 10},
        M				= 485.0,
        H_max			= 28000.0,
        H_min			= 100,
        Diam			= 330.0,
        Cx_pil			= 8,
        D_max			= 140000.0,
        D_min			= 10000.0,
        Head_Form		= 1,
        Life_Time		= 6400.0,
        Nr_max			= 10,
        v_min			= 10.0,
        v_mid			= 200.0,
        Mach_max		= 1.7,
        t_b				= 0.0,
        t_acc			= 0.0,
        t_marsh			= 600.0,
        Range_max		= 140000.0,
        H_min_t			= 0.0,
        Fi_start		= 3.14152,
        Fi_rak			= 3.14152,
        Fi_excort		= 3.14152,
        Fi_search		= 99.9,
        OmViz_max		= 99.9,
        warhead			= warheads["BLU-111B"],
        -- No exhaust (turbo)
        X_back			= -2.552,
        Y_back			= -0.252,
        Z_back			= 0.0,
        Reflection		= 0.0618,
        KillDistance	= 0.0,
		
		LaunchDistData =
		{
			19,		10,
   
													  
																 
																	

					100,	150,	200,	250,	300,	350,	400,	450,	500,	550,		
			100,	0,		0,		0,		8875,	9950,	23350,	29225,	34375,	38225,	41150,	
			200,	0,		0,		7775,	15325,	20325,	29425,	35000,	38825,	42000,	44700,	
			400,	0,		0,		9150,	17125,	26625,	34575,	39800,	43375,	46325,	49100,	
			700,	0,		0,		10150,	18525,	29475,	36725,	42000,	45675,	48625,	51300,	
			1000,	0,		0,		12200,	20350,	31700,	38900,	44250,	47950,	50950,	53700,	
			2000,	0,		11700,	17000,	25550,	36100,	45750,	51250,	55200,	58450,	61500,	
			3000,	14400,	16400,	20400,	29100,	38400,	52500,	58300,	62400,	69900,	73700,	
			4000,	19900,	22500,	23400,	31000,	40200,	59100,	65000,	73300,	77700,	81800,	
			5000,	26000,	26200,	27300,	35100,	42200,	65600,	71500,	80700,	85400,	89800,	
			6000,	31300,	31600,	34100,	38300,	45800,	53000,	77500,	87500,	92200,	97000,	
			7000,	39000,	41750,	42250,	42250,	49500,	57000,	88250,	93750,	98750,	104000,	
			8000,	44750,	45000,	45500,	45500,	53250,	60750,	88750,	93500,	105000,	110500,	
			9000,	46000,	46250,	46750,	49500,	57000,	64750,	97500,	98250,	103500,	116500,	
			10000,	61000,	65750,	75500,	84500,	91250,	96250,	99000,	107500,	107500,	113250,	
			11000,	65500,	72500,	81000,	89000,	95500,	100000,	102500,	106000,	111000,	116500,	
			12000,	69500,	77500,	86000,	93500,	100000,	105000,	107000,	108500,	114000,	118500,	
			13000,	73500,	82000,	90000,	98000,	104500,	109000,	111000,	113500,	116500,	121500,	
			14000,	76500,	86500,	94000,	102000,	108500,	113000,	114500,	117000,	118500,	123000,	
			15000,	79000,	90000,	97000,	105500,	112000,	116500,	117500,	120000,	122500,	125000,	
		},

		MinLaunchDistData =
		{
			19,		10,

					100,	150,	200,	250,	300,	350,	400,	450,	500,	550,		
			100,	0,		0,		0,		5900,	4225,	3750,	4150,	15875,	19325,	22175,	
			200,	0,		0,		7725,	5700,	2800,	3950,	4000,	4175,	5275,	5975,	
			400,	0,		0,		8025,	7025,	5000,	4975,	5075,	5225,	5825,	7325,	
			700,	0,		0,		7675,	6275,	4475,	5475,	5600,	5775,	5950,	6250,	
			1000,	0,		0,		7650,	6600,	4800,	6000,	6100,	6300,	6500,	6650,	
			2000,	0,		7800,	6150,	6450,	5300,	5500,	5700,	5900,	6000,	6750,	
			3000,	8800,	5700,	7100,	7300,	7500,	6200,	6200,	6400,	38600,	42200,	
			4000,	4400,	5900,	6600,	6900,	7200,	6000,	6000,	39600,	43800,	47900,	
			5000,	4000,	5700,	5900,	6300,	6500,	5900,	6200,	44400,	48900,	53400,	
			6000,	2000,	4700,	5400,	5700,	5900,	6000,	5600,	49100,	53800,	58600,	
			7000,	250,	4500,	6000,	6250,	6750,	6750,	48000,	53500,	58500,	63500,	
			8000,	750,	2750,	6250,	7000,	7500,	7500,	7250,	7250,	63000,	68250,	
			9000,	750,	2750,	6500,	7750,	8000,	8500,	44250,	8000,	8250,	73000,	
			10000,	2750,	3500,	6500,	8500,	8750,	9000,	9750,	48000,	9000,	9250,	
			11000,	3000,	3000,	5500,	7500,	8500,	9000,	9500,	10000,	10000,	10000,	
			12000,	5500,	3000,	6500,	8000,	9000,	10000,	10500,	10500,	11000,	11000,	
			13000,	3500,	3000,	6500,	8000,	9500,	10500,	11500,	12000,	11500,	12000,	
			14000,	4000,	5000,	7000,	8500,	10000,	11000,	12000,	12500,	12500,	13500,	
			15000,	4000,	5500,	5500,	9000,	10500,	11500,	12500,	13500,	14500,	14000,	
		},
	
		add_attributes = {"Cruise missiles"},
	},
	
    {
        Name = P_9M133, --9M133 Kornet
		display_name = _('AT-14 Spriggan'),
		name = "P_9M133",
        Escort = 1,
        Head_Type = 4,
		sigma = {4, 4, 4},
        M = 27.0,
        H_max = 5000.0,
        H_min = -1,
        Diam = 152.0,
        Cx_pil = 6,
        D_max = 5000.0,
        D_min = 100.0,
        Head_Form = 1,
        Life_Time = 25.0,
        Nr_max = 1.2,
        v_min = 105.0,
        v_mid = 200.0,
        Mach_max = 0.7*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 0.5,
        t_marsh = 20.0,
        Range_max = 5000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["P_9M133"],
		exhaust = {0.9, 0.9, 0.9, 0.1};
        X_back = -0.5,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.04,
        KillDistance = 0.0,
    },
    {
        Name = S_25L, --S-25L
		display_name = _('S-25L'),
		name = "S_25L",
        Escort = 1,
        Head_Type = 4,
		sigma = {8, 8, 8},
        M = 485.0,
        H_max = 4000.0,
        H_min = -1,
        Diam = 340.0,
        Cx_pil = 6,
        D_max = 4000.0,
        D_min = 1000.0,
        Head_Form = 0,
        Life_Time = 60.0,
        Nr_max = 1,
        v_min = 170.0,
        v_mid = 300.0,
        Mach_max = 1.7,
        t_b = 0.0,
        t_acc = 6.0,
        t_marsh = 0.0,
        Range_max = 7000.0,
        H_min_t = 0.0,
        Fi_start = 0.15,
        Fi_rak = 3.14152,
        Fi_excort = 0.35,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["S_25L"],
        exhaust = {0.9, 0.9, 0.9, 0.5},
        X_back = -1.8,
        Y_back =  0,
        Z_back =  0.0,
        Reflection = 0.0704,
        KillDistance = 0.0,
    },
    {
        Name = TOW2, --BGM-71D Tow for ground units
		display_name = _('BGM-71 TOW'),
		name = "TOW2",
        Escort = 3,
        Head_Type = 7,
		sigma = {4, 4, 4},
        M = 40.0,
        H_max = 2000.0,
        H_min = -1,
        Diam = 230.0, --150.0, -- 230 костыль для увеличения Сх, чтобы ракета теряла скорость близко к референсу
        Cx_pil = 6,
        D_max = 3800.0,
        D_min = 65.0, -- FM 23-34 Tow Weapons System
        Head_Form = 1,
        Life_Time = 23.0,
        Nr_max = 3.0,
        v_min = 105.0,
        v_mid = 150.0,
        Mach_max = 0.87*ATGMMissiles_VelocityAdaptationCoeff,
        t_b = 0.0,
        t_acc = 1.5, -- FM 3-22.1  M2 Bredley
        t_marsh = 0.0, 
        Range_max = 3800.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 1.0,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["TOW"],
        exhaust = {0.9, 0.9, 0.8, 0.1};
        X_back = -0.22,
        Y_back =  0.0,
        Z_back =  0,
        Reflection = 0.0658,
        KillDistance = 0.0,
    },
    {
        Name = SCUD_RAKETA, -- 8K14
		display_name = _('Scud'),
		name = "SCUD_RAKETA",
        Escort = 0,
        Head_Type = 5,
		sigma = {200, 200, 200},
        M = 5862.0,
        H_max = 19000.0,
        H_min = -1,
        Diam = 880.0,
        Cx_pil = 1,
        D_max = 310000.0,
        D_min = 12000.0,
        Head_Form = 1,
        Life_Time = 6000.0,
        Nr_max = 17,
        v_min = 85.0,
        v_mid = 1420.0,
        Mach_max = 4.86,
        t_b = 0.0,
        t_acc = 65.0,
        t_marsh = 0.0,
        Range_max = 350000.0,
        H_min_t = 0.0,
        Fi_start = 3.14152,
        Fi_rak = 3.14152,
        Fi_excort = 3.14152,
        Fi_search = 99.9,
        OmViz_max = 99.9,
        warhead = warheads["SCUD_8F14"],
        exhaust1 = tail_solid,
        X_back = -5.35,
        Y_back = 0.0,
        Z_back = 0.0,
        Reflection = 0.45,
        KillDistance = 0.0,
    },
}

function calcDamage(rocket_data)
    if ( rocket_data.warhead.cumulative_factor and
         rocket_data.warhead.cumulative_factor ~= 0) then
         rocket_data.Damage = rocket_data.warhead.cumulative_factor*rocket_data.warhead.expl_mass*2.5;
    elseif (rocket_data.warhead.expl_mass) then
         rocket_data.Damage = rocket_data.warhead.expl_mass*2.5;
    else
         rocket_data.Damage = 0.0;
    end
	
	if rocket_data.Damage_correction_coeff ~= nil then
		rocket_data.Damage = rocket_data.Damage_correction_coeff * rocket_data.Damage;
	end
	
	rocket_data.warhead.caliber = rocket_data.warhead.caliber or rocket_data.Diam --mm
end 

for _, res in pairs(rockets) do
	calcDamage(res)
	if not res.ws_type then 
		res.ws_type = wstype_missiles[res.Name]
	end
	if res.ws_type then 
		if  res.ws_type[4] == nil then
			res.ws_type[4] = res.Name
		end
	end
	registerResourceName(res,CAT_MISSILES)
end
